package stt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Reza Baktash on 12/12/2015.
 */
public class PrefixTree {
    PTreeNode root = new PTreeNode("root");
    public List<PTreeNode> nodes = new ArrayList<>();
}
class PTreeNode{
    String label;
    Map<String, Double> probNext;

    public PTreeNode(String label){
        this.label = label;
        probNext = new HashMap<>();
    }

    @Override
    public String toString() {
        return label + ":\t" + probNext.toString()+ "\n";
    }
}
