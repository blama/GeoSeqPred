package stt;

import datapreparation.StayPoint;
import hierarchical.Cluster;
import hierarchical.ClusterFunctions;
import hierarchical.Tuple;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Reza Baktash on 2/16/2016.
 */
public class GridClustering {
    public static int unit = 40;
    public static int min_intersects = 1;
    public static int min_stayPoints = 1;

    public static Map<Integer, Cluster> runner(List<StayPoint> initials, String username) {
        Map<String, Integer> cellNameToIdMap  = gridConstruction(ClusterFunctions.makeDailySequences(initials, username), unit);
        Map<Integer, Cluster> clusterMap = new HashMap<>();
        for (String cellName:cellNameToIdMap.keySet()){
            StayPoint cp = new StayPoint();
            cp.east = Integer.parseInt(cellName.substring(0,cellName.indexOf(",")))*unit + unit/2;
            cp.north = Integer.parseInt(cellName.substring(cellName.indexOf(",")+1))*unit + unit/2;
            cp.diameter = unit;
            clusterMap.put(cellNameToIdMap.get(cellName),new Cluster(Collections.singletonList(cp),cellNameToIdMap.get(cellName)));
        }
        return clusterMap;
    }

    public static Map<String, Integer> gridConstruction(List<List<StayPoint>> dailySpList, int unit) {
        Map<String, Tuple<Integer,Set<StayPoint>>> grids = new HashMap<>();
        for (List<StayPoint> spL:dailySpList) {
            for (StayPoint sp:spL) {
                String startGrid = findGridByPoint(sp, unit);
                if (grids.containsKey(startGrid)) {
                    grids.get(startGrid).t2.add(sp);
                } else {
                    grids.put(startGrid, new Tuple<>(0,new HashSet<>(Collections.singleton(sp))));
                }
            }
        }
        //Note: {minEast,maxEast,minNorth,maxNorth}double [] boundaries = getBoundaries(dailySpList.stream().flatMap(List::stream).collect(Collectors.toList()), unit);System.out.println("boundaries" + Arrays.toString(boundaries));
        for (List<StayPoint> spList : dailySpList) {
            for (int j = 0; j < spList.size() - 1; j++) {
                updateGrids(spList.get(j), spList.get(j + 1), unit, grids);
            }
        }
        //Note: filtering grids with these conditions: "min number of intersection" and having at least "one stay point"
        grids = grids.entrySet().stream().filter(e->e.getValue().t1>=min_intersects && e.getValue().t2.size()>=min_stayPoints).collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
        Map<String, Integer> cellNameToIdMap = new HashMap<>();
        int counter = 0;
        for(String key : grids.keySet()){
            cellNameToIdMap.put(key,++counter);
        }
        for (String key:grids.keySet()) {
            for (StayPoint sp:grids.get(key).t2){
                sp.clusterID = cellNameToIdMap.get(key);
                sp.levelIds = Collections.singletonList(sp.clusterID);
            }
        }
        dailySpList.stream().flatMap(List::stream).filter(e->e.clusterID<1).forEach(sp -> {sp.clusterID = -1;sp.levelIds = Collections.singletonList(-1);});
//        System.out.println(grids);
        return cellNameToIdMap;
    }

    /**
     * Given an StayPoint and unit of cells, calculates the cell which given stayPoint falls in.
     */
    private static String findGridByPoint(StayPoint sp, int unit) {
        //Note: if on the border, chooses left and below
        int startE = (sp.east)%unit==0 ? ((int)(sp.east/unit)-1) : (int)sp.east/unit;
        int startN = (sp.north)%unit==0 ? (int)(sp.east/unit)-1 : (int)sp.north/unit;
        return startE+","+startN;
    }

    private static void updateGrids(StayPoint start, StayPoint end, int unit, Map<String, Tuple<Integer, Set<StayPoint>>> grids) {
        if (start.east>end.east){
            StayPoint temp = start;
            start = end;
            end = temp;
        }
        String startGrid = findGridByPoint(start, unit);
        if (grids.containsKey(startGrid)) {
            grids.get(startGrid).t1++;
        } else {
            System.err.println("must be here");
        }
        boolean eAsc = end.east-start.east>0;
        boolean nAsc = end.north-start.north>0;
        List<Integer> eInterSects = eastAxisBetween(Math.min(start.east, end.east), Math.max(start.east, end.east), unit);
        List<Integer> nInterSects = northAxisBetween(Math.min(start.north, end.north), Math.max(start.north, end.north), unit);

        if (start.north==end.north){
            /*infinite slope*/
            for (Integer eIntSct:eInterSects) {
                String cell = getCellByIntersectionAndDirection(eIntSct, start.north, eAsc, nAsc, true, unit);
                if (grids.containsKey(cell)) {
                    grids.get(cell).t1++;
                } else {
                    grids.put(cell, new Tuple<>(1, new HashSet<>()));
                }
            }
        }
        else if(start.east == end.east){
            /*zero slope*/
            for (Integer nIntSct:nInterSects) {
                String cell = getCellByIntersectionAndDirection(start.east, nIntSct, eAsc, nAsc, false, unit);
                if (grids.containsKey(cell)) {
                    grids.get(cell).t1++;
                } else {
                    grids.put(cell, new Tuple<>(1, new HashSet<>()));
                }
            }
        }
        else{
            //equation of the line: y=ax+b => a=(y2-y1)/(x2-x1)  :::  b=y1-ax1
            double a = (end.north-start.north)/(end.east-start.east);
            double b = start.north - start.east*a;
            for (Integer eIntSct:eInterSects){
                String cell = getCellByIntersectionAndDirection(eIntSct,a*eIntSct+b,eAsc,nAsc,true, unit);
                if (grids.containsKey(cell)){
                    grids.get(cell).t1++;
                }
                else{
                    grids.put(cell,new Tuple<>(1,new HashSet<>()));
                }
            }
            for (Integer nIntSct:nInterSects){
                String cell = getCellByIntersectionAndDirection((nIntSct-b)/a,nIntSct,eAsc,nAsc,false, unit);
                if (grids.containsKey(cell)){
                    grids.get(cell).t1++;
                }
                else{
                    grids.put(cell,new Tuple<>(1,new HashSet<>()));
                }
            }
        }
    }

    /**
     * given a list of StayPoints, return bounds of points in two axises east and north.
     * Extends bounds so that points fall inside.
     * @param values
     * @param unit
     * @return a double [4] in the order {minEast,maxEast,minNorth,maxNorth}
     */
    private static double[] getBoundaries(List<StayPoint> values, int unit) {
        double minE=values.get(0).east; double maxE=values.get(0).east; double minN=values.get(0).north; double maxN=values.get(0).north;
        for(StayPoint sp:values){
            if (sp.east <= minE){
                minE = sp.east;
            }else if(sp.east >= maxE){
                maxE = sp.east;
            }
            if (sp.north <= minN){
                minN = sp.north;
            }else if(sp.north >= maxN){
                maxN = sp.north;
            }
        }
        return new double[]{minE-0.1*unit,maxE+0.1*unit,minN-0.1*unit,maxN+0.1*unit};
    }

    private static List<Integer> eastAxisBetween(double smaller, double bigger, int unit){
        int start = (smaller)%unit==0 ? (int)(smaller/unit)*unit : (((int)smaller/unit)+1)*unit;
        List<Integer> ret = new ArrayList<>();
        while (start<=bigger){
            ret.add(start);
            start += unit;
        }
        return ret;
    }

    private static List<Integer> northAxisBetween(double smaller, double bigger, int unit){
        int start = (smaller)%unit==0 ? (int)((smaller)/unit)*unit : ((int)((smaller)/unit)+1)*unit;
        List<Integer> ret = new ArrayList<>();
        while (start<=bigger){
            ret.add(start);
            start += unit;
        }
        return ret;
    }

    /**
     * a point is given like (east,north). Direction is mentioned by eAsc(ascending in east axis) and nAsc(ascending in north axis)
     * and also isEastAxis to mention if it is an intersect with east or north.
     * @param east easting (X) of given point
     * @param north northing (Y) of a given point
     * @param eAsc ascending in east (x) axis
     * @param nAsc ascending in north (x) axis
     * @param isEastAxis shows that given intersection is in the east(X) or north(Y) axis
     * @return the Cell which has been intersected
     */
    public static String getCellByIntersectionAndDirection(double east, double north, boolean eAsc, boolean nAsc, boolean isEastAxis, int unit){
        int eastId;
        int northId;
        if (isEastAxis){
            eastId = eAsc?(int)((east)/unit):(int)((east)/unit)-1;
            northId = (north)%unit!=0?(int)((north)/unit):nAsc?(int)((north)/unit):(int)((north)/unit)-1;
        }else{
            eastId = (east)%unit!=0?(int)((east)/unit):eAsc?(int)((east)/unit):(int)((east)/unit)-1;
            northId = nAsc?(int)((north)/unit):(int)((north)/unit)-1;
        }
        return eastId+","+northId;
    }
    public static double roundDown5(double d) {
        return (long) (d * 1e5) / 1e5;
    }

    public static void main(String[] args) {
//        Map<String, double[]> stats = new HashMap<>();usrSpMap.entrySet().forEach(e -> stats.put(e.getKey(), getBoundaries(e.getValue())));stats.forEach((k,v)->System.out.println("username:"+k+"\teast bounds: ("+v[0]+","+v[1]+") diff:"+(v[1]-v[0])+"\tnorth bounds:("+v[2]+","+v[3]+") diff:"+(v[3]-v[2])));
        List<StayPoint> samples = Arrays.asList(new StayPoint(5.6,2.4),new StayPoint(0.6,5.2),new StayPoint(4.6,4.8),new StayPoint(2.4,3.4));
        gridConstruction(Collections.singletonList(samples), 2);
//        List<StayPoint> samples = Arrays.asList(new StayPoint(1.5,1.5),new StayPoint(1.5,1.5),new StayPoint(1.5,1.5),new StayPoint(1.5,1.5));
    }
}