package stt;

import datapreparation.StayPoint;
import hierarchical.Cluster;
import hierarchical.ClusterFunctions;
import hierarchical.HierarchicalRunner;
import sequence.DailySequence;
import sequence.Function;
import utility.FunctionStore;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Created by Reza Baktash on 12/12/2015.
 */
public class STTRunner {
    public static void main(String[] args) throws IOException {
        /*for (int i = 0; i < 50; i++) {
            System.out.println(new Random().nextInt(5));
        }*/
    }

    public static List<PTreeNode> initializeTree(PTreeNode tNode, String[] input, double minSup){
        List<PTreeNode> sK = new ArrayList<>();
        // Note: single item count phase,
        Map<String, Integer> singleCounts = new HashMap<>();
        for (int i = 0; i < input.length; i++) {
            Arrays.stream(input[i].split(",")).distinct().forEach(s->{
                if (singleCounts.containsKey(s)){
                    singleCounts.put(s,singleCounts.get(s)+1);
                }else{
                    singleCounts.put(s,1);
                }
            });
        }
        //Note: make conditional probability table for root.
//        int sum = singleCounts.values().stream().mapToInt(Integer::intValue).sum();
        if (input.length==0) {System.err.println("division by zero");return sK;}
        singleCounts.forEach((k, v) -> tNode.probNext.put(k, v / (double) input.length));
        makeAndUpdateRoot(tNode, input, sK, minSup);
        return sK;
    }

    public static void makeAndUpdateRoot(PTreeNode fatherNode, String[] input, List<PTreeNode> sK, double minSup){
        fatherNode.probNext.forEach(
                (k, v) -> {
                    if (v >= minSup) {
                        PTreeNode node = new PTreeNode(k);
                        Map<String, Integer> suffix = findSuffix(k, input, minSup);
                        int summ = suffix.values().stream().mapToInt(Integer::intValue).sum();
                        if (!suffix.isEmpty()) {
                            suffix.forEach((key, val) ->
                            {
                                node.probNext.put(key, val / (double) summ);
                            });
                        }
                        sK.add(node);
                    }
                }
        );
    }

    public static void makeAndUpdateCandidates(PTreeNode fatherNode, String[] input, List<PTreeNode> sKPlus, double minSup){
        List<String> prefixes = findPrefix(fatherNode.label,input, minSup);
        if (!prefixes.isEmpty()){
            prefixes.forEach(s -> {
                        PTreeNode node = new PTreeNode(s);
                        Map<String, Integer> suffix = findSuffix(s, input, minSup);
                        int summ = suffix.values().stream().mapToInt(Integer::intValue).sum();
                        if (!suffix.isEmpty()) {
                            suffix.forEach((key, val) ->
                            {
                                node.probNext.put(key, val / (double) summ);
                            });
                        }
                        sKPlus.add(node);
                    }
            );
        }
    }

    public static List<String> findPrefix(String current, String[] input, double minSup){
        List<String> res = new ArrayList<>();
        Map<String, Integer> itemCount = new HashMap<>();
        for (String in:input){
            Set<String> prevs = new HashSet<>();
            while (in.contains(","+current)){
                String temp = in.substring(0,in.indexOf(","+current));
                if (temp.contains(",")){prevs.add(temp.substring(temp.lastIndexOf(",")+1));}
                else{prevs.add(temp);}
                if ((in.indexOf(current) + current.length() + 1)<=in.length()){
                    in = in.substring(in.indexOf(current) + current.length() + 1);
                }else {
                    break;
                }
            }
            for (String s:prevs){
                if (itemCount.containsKey(s)){
                    itemCount.put(s,itemCount.get(s)+1);
                }
                else{
                    itemCount.put(s,1);
                }
            }
        }
        if (!itemCount.isEmpty()){
            itemCount.forEach((key, val) ->
            {
                if ((val/(double)input.length) >= minSup) {
                    res.add(key + "," + current);
                }
            });
        }
        return res;
    }
    public static Map<String, Integer> findSuffix(String current, String[] input, double minSup){
        Map<String, Integer> itemCount = new HashMap<>();
        for (String in:input){
            Set<String> nexts = new HashSet<>();
            while (in.contains(current+",")){
                in = in.substring(in.indexOf(current)+current.length()+1);
                if (in.contains(",")){nexts.add(in.substring(0,in.indexOf(",")));}
                else {nexts.add(in);}
            }
            for (String s:nexts){
                if (itemCount.containsKey(s)){
                    itemCount.put(s,itemCount.get(s)+1);
                }
                else{
                    itemCount.put(s,1);
                }
            }
        }
        return itemCount;
    }
    public static double movementSimilarity(String sq, String nk){
        if (!sq.contains(nk)){
            return 0;
        }
        String[] sQ = sq.split(","); String[] nK = nk.split(",");
        double similarity = 0.0;
        int sqInd = 0;
        int nkInd = 0;
        while (sqInd<sQ.length && nkInd<nK.length){
            if (sQ[sqInd].equals(nK[nkInd])){
                similarity+=(sqInd+1)*(sqInd+1);
                sqInd++;nkInd++;
            }else{
                sqInd++;
            }
        }
        int jSum = 0;
        for (int i = 0; i < sQ.length; i++) {
            jSum += (i+1)*(i+1);
        }
        return similarity/jSum;
    }

    public static PrefixTree makeTree(String[] input, double minSup){
        PrefixTree tree = new PrefixTree();
        List<PTreeNode> sK = initializeTree(tree.root, input, minSup);
        tree.nodes.addAll(sK);
        while (!sK.isEmpty()){
            List<PTreeNode> sKPlus = new ArrayList<>();
            sK.forEach(node -> makeAndUpdateCandidates(node,input,sKPlus, minSup));
            tree.nodes.addAll(sKPlus);
            sK = sKPlus;
        }
        tree.nodes = tree.nodes.stream().filter(a->!a.probNext.isEmpty()).collect(Collectors.toList());
//        System.out.println(tree.root+""+tree.nodes);
        return tree;
    }

    public static double[] pTreePrediction(List<DailySequence> testData, PrefixTree pTree, Map<Integer, Cluster> clusterMap, String username) {
        // Note: 0-testSamples, 1-predictions, 2-matched, 3-true, 4-errorAverage
        double[] stats = new double[8];
        stats[7]=pTree.nodes.size();
        List<Double> distanceError = new ArrayList<>();
        List<String> exploitedSamples = new ArrayList<>();
        List<String> matchWrite = new ArrayList<>();
        List<String> randWrite = new ArrayList<>();
        int true_label = 0;
        int matched = 0;
        int allTests = 0;
        int randCount = 0;
        int errorCount = 0;

        List<String> other = new ArrayList<>();
        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = Function.readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
//            other = Function.readTestIndicesFromFile(username, "C:\\Users\\Reza\\Desktop\\indices-read1.txt");
        }
        for (DailySequence dailySequence: testData){
            DailySequence testSample = ClusterFunctions.filterSingleSequence(dailySequence,true);
            for (int i = 1; i < testSample.list.size(); i++) {
                int idx = dailySequence.list.indexOf(testSample.list.get(i));
                if (HierarchicalRunner.readTestIndices){
                    if (!exploitedSamples.contains(testSample.id+" "+idx)){
//                    if (exploitedSamples.contains(testSample.id+" "+idx) || other.contains(testSample.id+" "+idx)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
                while(ClusterFunctions.hasDuplicateItem(head)){
                    head = ClusterFunctions.filterDuplicate(head);
                }
                Integer lastId = head.get(head.size()-1).levelIds.get(0);
                StayPoint next = null;
                for (int j = i; j < testSample.list.size(); j++) {
                    next = testSample.list.get(j);
                    if (!next.levelIds.get(0).equals(lastId)) {
                        break;
                    }
                }
//                if (next!=null){
//                    if(next.levelIds.get(0).equals(lastId)) {
//                        continue;
//                    }
//                }
//                System.out.println("test sequence: " + testSample);
//                System.out.print("head sequence: " + head + "\treal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                String pStr = whatNext(head, pTree, useRand, clusterMap);
                Integer pValue = 0;
                if (!pStr.equals("")){
                    pValue = Integer.parseInt(pStr);
                }
//                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                    }
                    else {
                        errorCount++;
                        distanceError.add(predicted.distanceTo(next));
//                        System.out.println("\terror:"+predicted.distanceTo(next));
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                    if (HierarchicalRunner.writeTestIndices) {
                        if (useRand.get()){
                            randWrite.add(testSample.id + " " + idx);
                        }else{
                            matchWrite.add(testSample.id+" "+idx);
                        }
                    }
                }
            }
        }
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum();stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                matchWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesRandPath, true)));
                writer.append("u:").append(username).append("\n");
                randWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ///temp
        }
        return stats;
    }
    public static double[] pTreePredictionHarsh(List<DailySequence> testData, PrefixTree pTree, Map<Integer, Cluster> clusterMap, String username) {
        // Note: 0-testSamples, 1-predictions, 2-matched, 3-true, 4-errorAverage
        double[] stats = new double[8];
        stats[7]=pTree.nodes.size();
        List<Double> distanceError = new ArrayList<>();
        List<String> exploitedSamples = new ArrayList<>();
        List<String> other = new ArrayList<>();
        List<String> matchWrite = new ArrayList<>();
        List<String> randWrite = new ArrayList<>();
        int true_label = 0;
        int matched = 0;
        int allTests = 0;
        int randCount = 0;
        int errorCount = 0;

        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = Function.readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
//            other = Function.readTestIndicesFromFile(username, "C:\\Users\\Reza\\Desktop\\indices-read1.txt");
        }
        for (DailySequence testSample: testData){
            for (int i = 1; i < testSample.list.size(); i++) {
                int idx = i;
                if (HierarchicalRunner.readTestIndices){
                    if (!exploitedSamples.contains(testSample.id+" "+idx)){
//                    if (exploitedSamples.contains(testSample.id+" "+idx) || other.contains(testSample.id+" "+idx)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
                //Clean Head
                head = head.get(head.size()-1).clusterID>0? head.stream().filter(a->a.clusterID>0).collect(Collectors.toList()):head;
                while(ClusterFunctions.hasDuplicateItem(head)){
                    head = ClusterFunctions.filterDuplicate(head);
                }
                Integer lastId = head.get(head.size()-1).levelIds.get(0);
                StayPoint next = null;
                for (int j = i; j < testSample.list.size(); j++) {
                    next = testSample.list.get(j);
                    if (!next.levelIds.get(0).equals(lastId)) {
                        break;
                    }
                }
//                System.out.println("test sequence: " + testSample);
//                System.out.print("head sequence: " + head + "\treal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                String pStr = whatNext(head, pTree, useRand, clusterMap);
                Integer pValue = 0;
                if (!pStr.equals("")){
                    pValue = Integer.parseInt(pStr);
                }
//                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                    }
                    else {
                        errorCount++;
                        distanceError.add(predicted.distanceTo(next));
//                        System.out.println("\terror:"+predicted.distanceTo(next));
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                    if (HierarchicalRunner.writeTestIndices) {
                        if (useRand.get()){
                            randWrite.add(testSample.id + " " + idx);
                        }else{
                            matchWrite.add(testSample.id+" "+idx);
                        }
                    }
                }
            }
        }
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum();stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                matchWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesRandPath, true)));
                writer.append("u:").append(username).append("\n");
                randWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ///temp
        }
        return stats;
    }

    private static String whatNext(List<StayPoint> head, PrefixTree prefixTree, AtomicBoolean useRand, Map<Integer, Cluster> clusterMap) {
        Map<String, Double> itemScoreMap = new HashMap<>();
        StringBuilder temp = new StringBuilder();
        head.forEach(sp -> temp.append(sp.levelIds.get(0)).append(","));
        String headString = temp.substring(0, temp.length() - 1);
        prefixTree.nodes.stream().filter(node -> movementSimilarity(headString, node.label) > 0).forEach(nod ->{
            String bestAnswer = "";
            double bestProb = 0.0;
            for (Map.Entry<String,Double> entry:nod.probNext.entrySet()){
                if (entry.getValue()>bestProb){
                    bestAnswer = entry.getKey();
                    bestProb = entry.getValue();
                }
            }
            if (!bestAnswer.equals("")) {
                if (itemScoreMap.containsKey(bestAnswer)) {
                    itemScoreMap.put(bestAnswer,itemScoreMap.get(bestAnswer)+bestProb);
                }else {
                    itemScoreMap.put(bestAnswer,bestProb);
                }
            }
        });
        double bestScore = 0.0;
        String bestItem = "";
        for (Map.Entry<String, Double> entry:itemScoreMap.entrySet()){
            if (entry.getValue() > bestScore){
                bestScore = entry.getValue();
                bestItem = entry.getKey();
            }else if (entry.getValue() == bestScore){
                System.err.println("Shit happened again!");
            }
        }
        if (HierarchicalRunner.randomSelection && bestItem.equals("")){
            useRand.set(true);
            if (HierarchicalRunner.firstRand) {
                return head.get(head.size() - 1).levelIds.get(0).toString();
            }else if(HierarchicalRunner.nearestRand){
                return FunctionStore.getNearestLocation(HierarchicalRunner.firstLevelClusterMap, head.get(head.size() - 1)).toString();
//                return FunctionStore.getNearestLocation(HierarchicalRunner.firstLevelClusterMap, clusterMap.get(head.get(head.size() - 1).levelIds.get(0))).toString();
            }
        }
        return bestItem;
    }
}