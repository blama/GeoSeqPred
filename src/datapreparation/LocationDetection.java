package datapreparation;

import java.io.*;
import java.util.*;

/**
 * reads the stay points from file, turn them into objects and clusters them resulting a set of locations
 * to run : gp to Processor.java
 */
public class LocationDetection {
    final static String PATH = "staypoints";
    //mapping from users to corresponding stay points
    public Map<String, List<StayPoint>> map = new TreeMap<>();
    public Map<String, Integer> countMap = new TreeMap<>();
    public List<StayPoint> allStayPoints = new LinkedList<>();

    /**
     * Reads Stay Point Strings From File and Returns {@link datapreparation.StayPoint} Objects
     * Adds them all to the list:allStayPoints. also adds them group by username to the map.
     *
     * @return some logging information
     * @throws IOException
     */
    public void makePoints(String stayPointPath) throws IOException {
        File root = new File(stayPointPath);
        if (root.isDirectory()) {
            for (File child : root.listFiles((dir, name) -> (name.matches("([0-9])+.*")))) {
                ArrayList<StayPoint> stayPoints = new ArrayList<>();
                BufferedReader reader = new BufferedReader(new FileReader(child));
                String line;
                String username = child.getName().substring(0, child.getName().lastIndexOf("."));
                while ((line = reader.readLine()) != null) {
                    stayPoints.add(new StayPoint(line, username, false));
                }
                map.put(username, stayPoints);
                countMap.put(username, stayPoints.size());
            }
        }
        for (List<StayPoint> list : map.values()) {
            for (StayPoint sp : list) {
                allStayPoints.add(sp);
            }
        }
        /*File f = new File(PATH+"ttt.txt");
        f.createNewFile();
        PrintWriter writer = new PrintWriter(new FileWriter(f));
        for (StayPoint p: allStayPoints){
            writer.println(p.east+"\t"+p.north);
        }*/

    }

    /**
     * gets all stay points and index them with an id
     *
     * @param allPoints list of all stay points
     */
    public static void makeIndex(List<StayPoint> allPoints) {
        for (int i = 0; i < allPoints.size(); i++) {
            allPoints.get(i).index = i;
        }
    }
    public List<Location> DBSCAN(List<StayPoint> allPoints, double radius, int minpts) throws Exception{
        makeIndex(allPoints);
        List<Location> locs = makeCluster(allPoints, radius, minpts);
        locs.stream().forEach(Location::evaluateParameters);
        return locs;
    }
        //all methods in this area is for dbscan clustering from here to ...
    public List<Location> DBSCAN(List<StayPoint> allPoints, double radius, int minpts, String username) throws Exception {
        if (username.length() < 1) {
            username = "allUsers";
        }
        makeIndex(allPoints);
        StringBuilder b = new StringBuilder();
        b.append(allPoints.size());
        List<Location> locations = makeCluster(allPoints, radius, minpts);
        b.append("\t").append("'").append(username).append("'");
        b.append("\t").append(locations.size() - 1);
        System.out.println(b.toString());
        File file = new File("locations/" + username + "-locations" + radius + "," + minpts + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        PrintWriter pw = new PrintWriter(new FileWriter(file), true);
        for (int i = 0; i < locations.size(); i++) {
            Location l = locations.get(i);
            l.evaluateParameters();
            String str = "Location:" + l.id + ";" + l.toString() + "; PointsCount:" + l.pts.size() +
                    "; UsersCount:" + l.numberOfUsers + "; Diameter:" + l.diameter + "; Radius:" + l.radius;
            pw.println(str);
//            System.out.println(str);
        }
        return locations;
    }

    /**
     * Clusters given stay points using D.B.S.C.A.N algorithm with given parameters
     *
     * @param allPoints list of all stay points
     * @param radius    is a parameter in D.B.S.C.A.N
     * @param minpts    is a parameter in D.B.S.C.A.N
     * @return list of clusters which are named Locations
     * @throws Exception
     */
    public static List<Location> makeCluster(List<StayPoint> allPoints, double radius, int minpts) throws Exception {
        int clusterId = Stamper.nextClusterId();
        List<StayPoint> clustered = new ArrayList<>();
        List<StayPoint> seeds = new ArrayList<>();
        while (allPoints.size() > 0) {
            StayPoint sp = allPoints.get(0);
            if (sp.clusterID != Stamper.unclassified) {
                throw new Exception("classified data found");
            }
            allPoints.remove(sp);
            List<StayPoint> tempNbrs = findNeighbors(sp, allPoints, radius);
            if (tempNbrs.size() < minpts) {
                sp.clusterID = Stamper.noise;
                clustered.add(sp);
            } else {
                seeds.add(sp);
                seeds.addAll(tempNbrs);
                for (StayPoint stayPoint : seeds) {
                    stayPoint.clusterID = clusterId;
                    clustered.add(stayPoint);
                    allPoints.remove(stayPoint);
                }
                expandCluster(allPoints, seeds, clustered, radius, minpts, clusterId);
                clusterId = Stamper.nextClusterId();
            }
        }
        Collections.sort(clustered);
        List<Location> locations = new ArrayList<>();
        //could be implemented better
        int locationId = 0;
        if (clustered.size() > 0) {
            Location tempLoc = new Location();
            for (int i = 0; i < clustered.size(); i++) {
                if (clustered.get(i).clusterID == locationId) {
                    tempLoc.id = locationId;
                    tempLoc.pts.add(clustered.get(i));
                } else {
                    locations.add(tempLoc);
                    tempLoc = new Location();
                    locationId = clustered.get(i).clusterID;
                    tempLoc.pts.add(clustered.get(i));
                    tempLoc.id = locationId;
                }
            }
            locations.add(tempLoc);
        }
        return locations;
    }

    /**
     * expands the first given seed points and then goes through points
     * until no new neighbor is added and seed list is empty
     *
     * @param allPoints
     * @param seeds
     * @param clustered
     * @param radius
     * @param minpts
     * @param clusterId
     */
    public static void expandCluster(List<StayPoint> allPoints, List<StayPoint> seeds, List<StayPoint> clustered, double radius, int minpts, int clusterId) {
        while (!seeds.isEmpty()) {
            StayPoint current = seeds.get(0);
            List<StayPoint> tempNbrs = findNeighbors(current, allPoints, radius);
            if (tempNbrs.size() < minpts) {
                // if surrounding points are less than minpts, then expanding is over for this point and it is removed from seeds list
                seeds.remove(current);
            } else {
                // in the case surrounding points are more than minpts, this happens
                for (StayPoint sp : tempNbrs) {
                    sp.clusterID = clusterId;
                    clustered.add(sp);
                    allPoints.remove(sp);
                    seeds.add(sp);
                }
                seeds.remove(current);
            }
        }
    }

    /**
     * finds the points in the neighboring of current point within given radius
     *
     * @param current
     * @param allPoints
     * @param radius
     * @return found neighbors
     */
    public static List<StayPoint> findNeighbors(StayPoint current, List<StayPoint> allPoints, double radius) {
        List<StayPoint> neighbors = new ArrayList<StayPoint>();
        for (StayPoint sp : allPoints) {
            if (StayPoint.distance(sp, current) < radius) {
                neighbors.add(sp);
            }
        }
        return neighbors;
    }
    //to here

    public void writeSPsWithLocations(List<StayPoint> stayPoints, String desc, String rootPath) {
        try {
            File dest = new File(rootPath);
            if (!dest.exists()) {
                System.out.println(dest.mkdir());
            }
            File f = new File(rootPath + "\\" + desc + ".txt");
            System.out.println(f.createNewFile());
            PrintWriter writer = new PrintWriter(new FileWriter(f), true);
            if (stayPoints.size() > 0) {
                for (int i = 0; i < stayPoints.size() - 1; i++) {
                    StayPoint sp = stayPoints.get(i);
                    writer.println(sp.east + "," + sp.north + "," + sp.enterTime + "," + sp.leaveTime + "," + sp.diameter + "," + sp.timeString + "," + sp.clusterID);
                    writer.flush();
                }
                StayPoint sp = stayPoints.get(stayPoints.size() - 1);
                writer.print(sp.east + "," + sp.north + "," + sp.enterTime + "," + sp.leaveTime + "," + sp.diameter + "," + sp.timeString + "," + sp.clusterID);
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeLocationsOnFile(Map<Integer, Location> lMap, String rootDestPath) throws IOException {
        File f = new File(rootDestPath + "\\locations" + ".txt");
        if (!f.exists()) {
            if (f.createNewFile()) {
                StringBuilder sb = new StringBuilder("");
                for (Integer i : lMap.keySet()) {
                    sb.append(lMap.get(i).toString() + "\n");
                }
                PrintWriter writer = new PrintWriter(new FileWriter(f));
                writer.print(sb.toString().trim());
                writer.flush();
            } else {
                System.err.print("Couldn't Make File");
            }
        }
    }
}