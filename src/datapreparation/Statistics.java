package datapreparation;

import java.io.*;

public class Statistics {
    final static String PATH = "/home/harati/Desktop/GeoLife/converted-single-files";

    public static void main(String[] args) throws IOException {

    }

    void count() throws IOException {
        File root = new File(PATH);
        if (root.isDirectory()) {
            for (File child : root.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.contains("-")) {
                        return false;
                    }
                    return true;
                }
            })) {
                LineNumberReader reader = new LineNumberReader(new FileReader(child));
                long lineNumber = 0;
                while (reader.readLine() != null) {
                    lineNumber++;
                }
                System.out.println(child.getName() + ";" + lineNumber);
            }
            for (File child : root.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.contains("-")) {
                        return true;
                    }
                    return false;
                }
            })) {
                LineNumberReader reader = new LineNumberReader(new FileReader(child));
                long lineNumber = 0;
                while (reader.readLine() != null) {
                    lineNumber++;
                }
                System.out.println(child.getName().substring(0, child.getName().lastIndexOf(".")) + ";" + lineNumber);
            }
        }
    }
}
