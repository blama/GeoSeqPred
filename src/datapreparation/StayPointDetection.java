package datapreparation;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class StayPointDetection {
    static final int DAY_TO_SEC = 86400;
//    static final double Distance_Threshold = 50;
    static final double Distance_Threshold = 200;
//    static final double Time_Threshold = 3000;
    static final double Time_Threshold = 1800;

    static final String PATH= "F:\\reza\\courses\\Msc\\Thesis\\Strorage\\backups\\converted-single-files+stayPoints+TimeString\\converted-single-files\\GPS Points";
    static final String DEST= "F:\\reza\\courses\\Msc\\Thesis\\Strorage\\backups\\converted-single-files+stayPoints+TimeString\\converted-single-files\\staypoints";

    public static void main(String[] args) throws IOException {
        stopPoints();
    }

    /**
     * retrieves stay points from gps logs (effective parameters are Time_Threshold and Distance_Threshold)
     * and writes them on a file with same name
     */
    public static void stopPoints() throws IOException {
        File root = new File(PATH);
        if (!root.isDirectory()) {
            return;
        }
        for (File src : root.listFiles()) {
            File dstn = new File(DEST + "\\"+src.getName());
            if (!dstn.exists()) {
                dstn.createNewFile();
            }
            PrintWriter writer = new PrintWriter(new FileWriter(dstn));
            BufferedReader reader = new BufferedReader(new FileReader(src));
            ArrayList<String> lines = new ArrayList<String>();
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                lines.add(nextLine);
            }
            boolean token = false;

            System.out.println("FileName:" + src.getName() + "-size:" + lines.size());
            int i = 0;
            int j;
            Point first, beforeLast, last;
            while (i < lines.size() - 1) {
                double sumNorth = 0;
                double sumEast = 0;
                first = getPoint(lines.get(i));
                j = i + 1;
                while (j < lines.size()) {
                    last = getPoint(lines.get(j));
                    beforeLast = getPoint(lines.get(j - 1));
                    token = false;
                    if (lines.get(i) == null || lines.get(j) == null) {
                        System.err.println("null pointer exception");
                    }
                    double distance = distance(first, last);
                    sumEast += beforeLast.mlat;
                    sumNorth += beforeLast.mlong;
                    if (distance > Distance_Threshold) {
                        double time_diff = Math.abs((first.time - last.time) * DAY_TO_SEC);
                        if (time_diff > Time_Threshold) {
//                                String string = "stay Point: easting:"+sumEast/(j-i)+",northing:"+sumNorth/(j-i)+",enterTime:"+first.time+",leaveTime:"+beforeLast.time+",Diameter:"+getDistance(first,beforeLast);
//                                System.out.println(string);
                            writer.println("" + sumEast / (j - i) + "," + sumNorth / (j - i) + "," + first.time + "," + beforeLast.time + "," + distance(first, beforeLast) + "," + first.time_format + "," + beforeLast.time_format);
                            i = j;
                            token = true;
                        }
                        break;
                    }
                    j++;
                }
                if (!token) {
                    i++;
                }
                writer.flush();
            }
        }
    }

    //makes the object Point from string format
    public static Point getPoint(String line) {
        StringTokenizer st = new StringTokenizer(line, ",");
        return new Point(
                Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()),
                Double.parseDouble(st.nextToken()), st.nextToken() + "," + st.nextToken());
    }

    //euclidean getDistance between two Points
    public static double distance(Point first, Point second) {
        return Math.sqrt(Math.pow(first.mlat - second.mlat, 2) + Math.pow(first.mlong - second.mlong, 2));
    }

    /**
     * Gets each entry line and returns expected properties in and array:
     * easting, northing and time in meters, meters and days respectively.
     */
    public static double[] getProperties(String line) {
        StringTokenizer st = new StringTokenizer(line, ",");
        return new double[]{Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken())};
    }
}