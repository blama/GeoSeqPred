package datapreparation;

import java.util.StringTokenizer;

/**
 * Class MyDate Converts the String in format 'yyyy-mm-dd,hh:mm:ss' to our predefined format
 * It also Adds 8 hours to it, So that GMT time be converted to beijing time.
 */
public class MyDate {
    public int year;
    public int month;
    public int day;
    public int hour;
    public int min;
    public int second;

    public MyDate(int year, int month, int day, int hour, int min, int second) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.min = min;
        this.second = second;
    }

    public MyDate() {
    }

    public static MyDate getStartDate(String date) {
        MyDate md = new MyDate();
        StringTokenizer tok1 = new StringTokenizer(date, ",");
        StringTokenizer tok2 = new StringTokenizer(tok1.nextToken(), "-");
        StringTokenizer tok3 = new StringTokenizer(tok1.nextToken(), ":");
        md.year = Integer.parseInt(tok2.nextToken());
        md.month = Integer.parseInt(tok2.nextToken());
        md.day = Integer.parseInt(tok2.nextToken());
        md.hour = Integer.parseInt(tok3.nextToken());
        md.min = Integer.parseInt(tok3.nextToken());
        md.second = Integer.parseInt(tok3.nextToken());
        if (md.hour < 16) {
            md.hour = md.hour + 8;
        } else {
            int remain = 24 - md.hour;
            md.hour = 8 - remain;
            md.day = md.day + 1;
        }
        return md;
    }

    public static MyDate getEndDate(String date) {
        MyDate md = new MyDate();
        StringTokenizer tok1 = new StringTokenizer(date, ",");
        tok1.nextToken();
        tok1.nextToken();
        StringTokenizer tok2 = new StringTokenizer(tok1.nextToken(), "-");
        StringTokenizer tok3 = new StringTokenizer(tok1.nextToken(), ":");
        md.year = Integer.parseInt(tok2.nextToken());
        md.month = Integer.parseInt(tok2.nextToken());
        md.day = Integer.parseInt(tok2.nextToken());
        md.hour = Integer.parseInt(tok3.nextToken());
        md.min = Integer.parseInt(tok3.nextToken());
        md.second = Integer.parseInt(tok3.nextToken());
        if (md.hour < 16) {
            md.hour = md.hour + 8;
        } else {
            int remain = 24 - md.hour;
            md.hour = 8 - remain;
            md.day = md.day + 1;
        }
        return md;
    }
}