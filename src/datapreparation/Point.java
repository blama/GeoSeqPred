package datapreparation;

/**
 * Simple Object for each point
 */
public class Point {
    public double mlat;
    double mlong;
    double time;
    String time_format;

    public Point(double mlat, double mlong, double time, String time_format) {
        this.mlat = mlat;
        this.mlong = mlong;
        this.time = time;
        this.time_format = time_format;
    }

    @Override
    public String toString() {
        return mlat + "";
    }
}
