package datapreparation;

public class Stamper {
    public static final int noise = -1;
    public static final int unclassified = 0;
    public static int cluster_id = 0;

    public static int nextClusterId() {
        return ++cluster_id;
    }
}
