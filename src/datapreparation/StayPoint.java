package datapreparation;

import clustering.Euclidean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Just The Model For Stay Point Objects
 */

public class StayPoint implements Comparable<StayPoint>, Serializable, Euclidean<StayPoint> {

    public double east;
    public double north;
    public double enterTime;
    public double leaveTime;
    public double diameter;
    public String timeString;
    public int clusterID = 0;
    public String username;
    public List<Integer> levelIds;

    int index = 0;
    public StayPoint() {}
    public StayPoint(double east, double north, double enterTime, double leaveTime, double diameter, String timeString) {
        this.east = east;
        this.north = north;
        this.enterTime = enterTime;
        this.leaveTime = leaveTime;
        this.diameter = diameter;
        this.timeString = timeString;
    }

    public StayPoint(double east, double north) {
        this.east = east;
        this.north = north;
    }

    public StayPoint(String line, String username, boolean hasClusterId) {
        try {
            StringTokenizer tokenizer = new StringTokenizer(line, ",");
            this.east = Double.parseDouble(tokenizer.nextToken());
            this.north = Double.parseDouble(tokenizer.nextToken());
            this.enterTime = Double.parseDouble(tokenizer.nextToken());
            this.leaveTime = Double.parseDouble(tokenizer.nextToken());
            this.diameter = Double.parseDouble(tokenizer.nextToken());
            String timeString = tokenizer.nextToken() + "," + tokenizer.nextToken() + "," + tokenizer.nextToken() + "," + tokenizer.nextToken();
            this.timeString = timeString;
            if (hasClusterId) {
                this.clusterID = Integer.parseInt(tokenizer.nextToken());
            }
            this.username = username;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public StayPoint(StayPoint sp) {
        this.east = sp.east;
        this.north = sp.north;
        this.enterTime = sp.enterTime;
        this.leaveTime = sp.leaveTime;
        this.diameter = sp.diameter;
        this.timeString = sp.timeString;
        this.clusterID = sp.clusterID;
        this.levelIds = sp.levelIds;
        this.username = sp.username;
    }

    public static List<StayPoint> getAllPoints(String[] list) {
        List<StayPoint> res = new ArrayList<StayPoint>();
        for (String s : list) {
            res.add(new StayPoint(s, "", false));
        }
        return res;
    }

    @Override
    public StayPoint add(StayPoint t1, StayPoint t2) {
        return new StayPoint(t1.east + t2.east, t1.north + t2.north);
    }

    @Override
    public StayPoint addSqr(StayPoint sumSqr, StayPoint entry) {
        return new StayPoint(sumSqr.east + (entry.east * entry.east), sumSqr.north + (entry.north * entry.north));
    }

    @Override
    public double getDistance(StayPoint e1, StayPoint e2) {
        return StayPoint.distance(e1, e2);
    }

    //getDistance between two stay points
    public static double distance(StayPoint sam, StayPoint in) {
        return Math.sqrt(Math.pow((sam.east - in.east), 2) + Math.pow((sam.north - in.north), 2));
    }

    public static StayPoint getAverageSP(List<StayPoint> list){
        StayPoint reduce = list.stream().reduce(new StayPoint(0, 0), (s1, s2) -> new StayPoint(s1.east + s2.east, s1.north + s2.north));
        int size = list==null?1:list.size();
        reduce.north/=size;
        reduce.east/=size;
        return reduce;
    }

    @Override
    public StayPoint divide(StayPoint sum, int size) {
        return new StayPoint(east / size, north / size);
    }

    //getDistance between this and another stay point
    public double distance(StayPoint in) {
        return Math.sqrt(Math.pow((east - in.east), 2) + Math.pow((north - in.north), 2));
    }

    @Override
    public int compareTo(StayPoint o) {
        return this.clusterID - o.clusterID;
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof StayPoint) {
            StayPoint that = (StayPoint) other;
            result = (this.east == that.east && this.north == that.north && this.timeString.equals(that.timeString));
        }
        return result;
    }

    @Override
    public String toString() {
        return ""+levelIds; // +"easting:"+new BigDecimal(east).intValue() + ", northing:"+north + ", diameter:"+ diameter;
    }

}
