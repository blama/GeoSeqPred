package datapreparation;

public class SPDate {
    public MyDate start;
    public MyDate end;

    public SPDate(String time) {
        start = MyDate.getStartDate(time);
        end = MyDate.getEndDate(time);
    }
}

