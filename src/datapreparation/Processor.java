package datapreparation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Processor {
    public static void main(String[] args) {
//        generateGeneralPoint("stayPoints(with timeString)", 30, 3);
//        generateAllocatedPoint("stayPoints(with timeString)", 30, 3);
        generateAllocatedPoint("stayPoints(30,200)", 30, 3);
    }

    /**
     * Clusters StayPoints  for Each Specific User with D.B.S.C.A.N and Writes Results in the File
     *
     * @param stayPointPath
     * @param radius
     * @param minPts
     */
    public static void generateAllocatedPoint(String stayPointPath, int radius, int minPts) {
        String rootDestPath = "stayPoints(30,200)+loc(DBSCAN,30,3)";
        try {
            Map<Integer, Location> lMap = new HashMap<>();
            LocationDetection ld = new LocationDetection();
            ld.makePoints(stayPointPath);
            for (String key : ld.map.keySet()) {
                List<StayPoint> stayPoints = new ArrayList<>();
                stayPoints.addAll(ld.map.get(key));
                List<Location> tl = ld.DBSCAN(stayPoints, radius, minPts, key);
                for (Location l : tl) {
                    if (!lMap.containsKey(l.id)) {
                        lMap.put(l.id, l);
                    }
                }
                ld.writeSPsWithLocations(ld.map.get(key), key, rootDestPath);
            }
            ld.writeLocationsOnFile(lMap, rootDestPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Clusters StayPoints  for All Users with D.B.S.C.A.N and Writes Results in the File
     *
     * @param stayPointPath
     * @param radius
     * @param minPts
     */
    public static void generateGeneralPoint(String stayPointPath, int radius, int minPts) {
        String rootDestPath = "stayPoints+loc+all(timeString)";
        try {
            Map<Integer, Location> lMap = new HashMap<>();
            LocationDetection ld = new LocationDetection();
            ld.makePoints(stayPointPath);
            List<StayPoint> stayPoints = new ArrayList<>();
            for (String key : ld.map.keySet()) {
                stayPoints.addAll(ld.map.get(key));
            }
            List<Location> tl = ld.DBSCAN(stayPoints, radius, minPts, "");
            for (String key : ld.map.keySet()) {
                for (Location l : tl) {
                    if (!lMap.containsKey(l.id)) {
                        lMap.put(l.id, l);
                    }
                }
                ld.writeSPsWithLocations(ld.map.get(key), radius + "," + minPts + "," + key, rootDestPath);
            }
            ld.writeLocationsOnFile(lMap, rootDestPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
