package datapreparation;

import prediction.location.UserLocation;

import java.io.Serializable;
import java.util.*;

/**
 * The Model For City Locations &
 * Required Functions on Locations
 */
public class Location implements Serializable {
    public int getId() {
        return id;
    }

    //unique id for each place
    int id;
    //mean coordinates of points
    public double easting;
    public double northing;
    //list of stay points in this place
    public List<StayPoint> pts;
    //a map showing each user's stay points
    Map<String, List<StayPoint>> groupBy = new HashMap<String, List<StayPoint>>();
    int numberOfUsers = 0;
    public double diameter = 0;
    public double radius = 0;

    public Location() {
        pts = new ArrayList<>();
    }

    public Location(List<StayPoint> stayPoints) {
        pts = stayPoints;
        evaluateParameters();
    }

    public Location(String line) {
        StringTokenizer tokenizer = new StringTokenizer(line);
        this.id = Integer.parseInt(tokenizer.nextToken());
        this.easting = Double.parseDouble(tokenizer.nextToken());
        this.northing = Double.parseDouble(tokenizer.nextToken());
        this.radius = Double.parseDouble(tokenizer.nextToken());
    }

    //calculates coordinates, diameter, radius and makes the user-point map
    public void evaluateParameters() {
        double eastSum = 0;
        double northSum = 0;
        for (StayPoint sp : pts) {
            eastSum += sp.east;
            northSum += sp.north;
        }
        easting = eastSum / pts.size();
        northing = northSum / pts.size();
        List<StayPoint> tempPts = new ArrayList<StayPoint>();
        tempPts.addAll(pts);
        List<StayPoint> group = new ArrayList<StayPoint>();
        while (!tempPts.isEmpty()) {
            StayPoint current = tempPts.get(0);
            String username = current.username;
            group.add(current);
            tempPts.remove(current);
            for (StayPoint point : tempPts) {
                if (point.username.equals(username)) {
                    group.add(point);
                }
            }
            groupBy.put(username, group);
            tempPts.removeAll(group);
            group.clear();
        }
        numberOfUsers = groupBy.size();
        diameter();
        radius();
    }

    //biggest getDistance between the points
    public void diameter() {
        double diam = 0;
        for (int i = 0; i < pts.size() - 1; i++) {
            for (int j = i + 1; j < pts.size(); j++) {
                if (StayPoint.distance(pts.get(i), pts.get(j)) > diam) {
                    diam = StayPoint.distance(pts.get(i), pts.get(j));
                }
            }
        }
        this.diameter = diam;
    }

    //biggest getDistance from the center
    public void radius() {
        StayPoint center = new StayPoint(this.easting, this.northing, 0, 0, 0, "");
        double radius = 0;
        for (StayPoint sp : pts) {
            if (StayPoint.distance(sp, center) > radius) {
                radius = StayPoint.distance(sp, center);
            }
        }
        this.radius = radius;
    }

    //getDistance between center of two separate locations
    public static double centerDistance(Location l1, Location l2) {
        return Math.sqrt(Math.pow(l1.easting - l2.easting, 2) + Math.pow(l1.northing - l2.northing, 2));
    }

    public static Double stayPointDistance(Location l1, UserLocation s1) {
        return Math.sqrt(Math.pow(l1.easting - s1.east, 2) + Math.pow(l1.northing - s1.north, 2));
    }

    @Override
    public String toString() {
        return id + "\t" + easting + "\t" + northing + "\t" + radius;
    }

    public boolean isEmpty() {
        return pts.isEmpty();
    }

    public void addPoint(StayPoint p) {
        pts.add(p);
    }

}
