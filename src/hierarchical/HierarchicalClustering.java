package hierarchical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HierarchicalClustering {
    public static final int numberOfLevels = 6;
    public static Map<Integer, Integer> levelCounters = new HashMap<>();
    public static Map<Integer, List<HLocation>> levelLocationMap = new HashMap<>();
//    public static Map<Integer,HLocation> locationMap = new HashMap<>();

    public static Map<Integer, List<HLocation>> start(List<HLocation> firstLevel, Map<Integer, HLocation> locationMap) {
        //initialize level counter <1,1000> , <2,2000> , ...
        for (int i = 0; i < numberOfLevels; i++) {
            levelCounters.put(i + 1, (i + 1) * 1000);
        }
        int currentLevel = 1;
        levelLocationMap.put(currentLevel, firstLevel);
        for (int i = 2; i <= numberOfLevels; i++) {
            List<HLocation> niu = levelUpClusters(i - 1, locationMap);
            if (niu.size() == levelLocationMap.get(currentLevel).size()) {
                break;
            }
            levelLocationMap.put(i, niu);
        }
        return levelLocationMap;
    }

    private static List<HLocation> levelUpClusters(Integer previousLevel, Map<Integer, HLocation> locationMap) {
        List<HLocation> previousLocs = levelLocationMap.get(previousLevel);
        List<HLocation> temp = new ArrayList<>();
        temp.addAll(previousLocs);
//        System.out.println("size:" + previousLocs.size());
        if (temp.size() < 2) {
            return temp;
        }
        long limit = Math.max(1, Math.round(0.1 * previousLocs.size()));
        for (int i = 0; i < limit; i++) {
            temp = mergeNearest(temp, previousLevel + 1, locationMap);
        }
        return temp;
    }

    public static List<HLocation> mergeNearest(List<HLocation> temp, Integer currentLevel, Map<Integer, HLocation> locationMap) {
        int x = 0, y = 0;
        double minDist = Double.MAX_VALUE;
        for (int i = 0; i < temp.size(); i++) {
            for (int j = i + 1; j < temp.size(); j++) {
                if (i != j) {
                    double dist;
                    if ((dist = HLocation.distance(temp.get(i), temp.get(j))) < minDist) {
                        minDist = dist;
                        x = i;
                        y = j;
                    }
                }
            }
        }
        List<HLocation> ret = new ArrayList<>();
        for (int i = 0; i < temp.size(); i++) {
            if (i != x && i != y) {
                ret.add(temp.get(i));
            }
        }
        HLocation merged = HLocation.merge(temp.get(x), temp.get(y), levelCounters.get(currentLevel) + 1);
        ret.add(merged);
        locationMap.put(merged.id, merged);
        levelCounters.put(currentLevel, levelCounters.get(currentLevel) + 1);
        return ret;
    }


    public static Map<Integer, List<Integer>> retrieveLevels(List<Integer> integers, List<HLocation> lastLevLocs) {
        Map<Integer, List<Integer>> listMap = new HashMap<>();
        List<List<Integer>> allLists = new ArrayList<>();
        for (HLocation hl : lastLevLocs) {
            topDownLists(hl, new ArrayList<>(), allLists);
        }
        for (List<Integer> il : allLists) {
            if (integers.contains(il.get(il.size() - 1))) {
                listMap.put(il.get(il.size() - 1), il);
            }
        }
        return listMap;
    }


    public static void topDownLists(HLocation hl, List<Integer> head, List<List<Integer>> allLists) {
        List<Integer> niu = new ArrayList<>();
        niu.addAll(head);
        niu.add(hl.id);
        if (hl.subLocations.isEmpty()) {
            allLists.add(niu);
        } else {
            for (HLocation hLoc : hl.subLocations) {
                topDownLists(hLoc, niu, allLists);
            }
        }
    }
}
