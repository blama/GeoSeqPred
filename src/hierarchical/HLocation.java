package hierarchical;

import datapreparation.Location;
import prediction.location.UserLocation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HLocation {
    public List<HLocation> subLocations;

    public double east;
    public double north;
    public double radius;
    public int id;
    public int numberOfChilds = 1;

    public HLocation() {
    }

    public HLocation(Location l) {
        this.east = l.easting;
        this.north = l.northing;
        this.radius = l.radius;
        this.id = l.getId();
        this.subLocations = new ArrayList<>();
    }

    public static double distance(HLocation a, HLocation b) {
        return Math.sqrt(Math.pow((a.east - b.east), 2) + Math.pow((a.north - b.north), 2));
    }

    public static Double distance(HLocation a, UserLocation b) {
        return Math.sqrt(Math.pow((a.east - b.east), 2) + Math.pow((a.north - b.north), 2));
    }

    public static HLocation merge(HLocation a, HLocation b, int id) {
        int childSum = a.numberOfChilds + b.numberOfChilds;
        HLocation hl = new HLocation();
        hl.numberOfChilds = childSum;
        hl.north = (a.numberOfChilds * a.north + b.numberOfChilds * b.north) / childSum;
        hl.east = (a.numberOfChilds * a.east + b.numberOfChilds * b.east) / childSum;
        hl.radius = Math.max(a.radius, b.radius) + Math.max(HLocation.distance(hl, a), HLocation.distance(hl, b));
        hl.subLocations = Arrays.asList(a, b);
        hl.id = id;
        return hl;
    }

    public boolean isInRadius(UserLocation noise) {
        return (distance(this, noise) < radius);
    }
}
