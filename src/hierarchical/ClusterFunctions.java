package hierarchical;

import datapreparation.Location;
import datapreparation.SPDate;
import datapreparation.StayPoint;
import prediction.util.AprioriRunner;
import sequence.DailySequence;
import sequence.PredictionTest;
import sequence.Sequence;
import utility.MapUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Reza Baktash on 8/30/2015.
 */
public class ClusterFunctions {
    /**
     * starts hierarchical clustering with a list of small seed points
     * @param initials
     * @param seed
     * @return A Tuple2C, which is a map of level_ids to cluster lists as first parameter
     * and a list of cutoff levels.
     */
    public static Map<Integer, List<Cluster>> startClustering(List<StayPoint> initials, List<Cluster> seed, List<Integer> cutOffs) {
        initials.forEach(s -> s.clusterID = HierarchicalRunner.globalClusterIndexer++);
        for (StayPoint stayPoint:initials){
            seed.add(new Cluster(Collections.singletonList(stayPoint), stayPoint.clusterID));
        }
        int n = seed.size();
        Map<Integer, List<Cluster>> resultMap = new TreeMap<>();
        int cutOffIndex = 0;
        Tuple2C<List<Cluster>, Double> newCL;
        List<Double> minz = new ArrayList<>();
        minz.add(0.0);
        while(seed.size()>1 && cutOffIndex<cutOffs.size()){
            newCL = levelUp(seed,minz);
            if (newCL.getT2() > cutOffs.get(cutOffIndex)){
                resultMap.put(n-seed.size()+1, seed);
                cutOffIndex++;
                // Note: this is the case where two consecutive cutoff thresholds are so close so that a new levelUp will move two steps higher
                while(cutOffIndex<cutOffs.size()){
                    if (newCL.getT2() > cutOffs.get(cutOffIndex)) {
                        System.err.println("consider another set set of thresholds" + cutOffs.get(cutOffIndex-1) + "," + cutOffs.get(cutOffIndex));
                        cutOffIndex++;
                    }
                    else{
                        break;
                    }
                }
                //
            }
            seed = newCL.getT1();
        }
        System.out.print("levelMapSize:" + resultMap.size() + "\t");
        resultMap.forEach((k, v) -> System.out.print("level:" + k + ", size:" + resultMap.get(k).size() + ", dist:" + (minz.get(k - 1))));
        System.out.println("minz:"+minz.toString());
        return resultMap;
    }

    public static Map<Integer, List<Cluster>> startClustering(List<StayPoint> initials, List<Cluster> seed) {
        initials.forEach(s -> s.clusterID = HierarchicalRunner.globalClusterIndexer++);
        for (StayPoint stayPoint:initials){
            seed.add(new Cluster(Collections.singletonList(stayPoint), stayPoint.clusterID));
        }
        int n = seed.size();
        Map<Integer, List<Cluster>> resultMap = new TreeMap<>();
        int cutOffIndex = 0;
        Tuple2C<List<Cluster>, Double> newCL;
        List<Double> minz = new ArrayList<>();
        minz.add(0.0);
        while(seed.size()>1){
            newCL = levelUp(seed,minz);
            if (newCL.getT2()<HierarchicalRunner.cutOffs.get(0)){
                seed = newCL.getT1();
                continue;
            }
            if (newCL.getT2()>HierarchicalRunner.cutOffs.get(HierarchicalRunner.cutOffs.size()-1)){
                break;
            }
            resultMap.put(n - seed.size() + 1, newCL.getT1());
            seed = newCL.getT1();
        }
//        System.out.print("levelMapSize:" + resultMap.size() + "\t");
//        resultMap.forEach((k, v) -> System.out.print("level:" + k + ", size:" + resultMap.get(k).size() + ", dist:" + (minz.get(k - 1))));
//        System.out.println("minz:"+minz.toString());
        return resultMap;
    }

    private static Tuple2C<List<Cluster>,Double> levelUp(List<Cluster> seed, List<Double> minz){
        double [][] distances = new double[seed.size()][seed.size()];
        int minI = 0;
        int minJ = 0;
        double minDist = Double.MAX_VALUE;
        for (int i = 0; i < seed.size(); i++) {
            for (int j = i; j < seed.size(); j++) {
                distances[i][j] = avgClusterDistance(seed.get(i),seed.get(j));
                distances[j][i] = distances[i][j];
                if (i!=j && distances[i][j]<minDist){
                    minDist = distances[i][j];
                    minI = i;
                    minJ = j;
                }
            }
        }
        List<Cluster> upgraded = new ArrayList<>();
        for (int i = 0; i < seed.size(); i++) {
            if (i!=minI && i!=minJ){
                upgraded.add(seed.get(i));
            }
        }
        minz.add(minDist);
        upgraded.add(Cluster.merge(seed.get(minI), seed.get(minJ)));
        return new Tuple2C<>(upgraded,minDist);
    }

    private static double avgClusterDistance(Cluster i, Cluster j) {
        double sum = 0;
        for (int k = 0; k < i.getMembers().size(); k++) {
            for (int l = 0; l < j.getMembers().size(); l++) {
                sum += StayPoint.distance(i.getMembers().get(k), j.getMembers().get(l));
            }
        }
        return sum/ (i.getMembers().size() * j.getMembers().size());
    }

    public static HierarchicalTree makeHierarchyTree(List<StayPoint> initials) {
        HierarchicalTree tree = new HierarchicalTree();
        spLoop:
        for (StayPoint stayPoint:initials){
            List<Integer> levelIds = stayPoint.levelIds;
            if (levelIds!=null && levelIds.size()>0) {
                Iterator<Integer> it = levelIds.iterator();
                Integer lastId = it.next();
                HTreeNode lastNode;
                if (!tree.hasNode(lastId)){
                    lastNode = new HTreeNode(lastId);
                    HTreeNode currentNode;
                    while (it.hasNext()){
                        Integer currentId = it.next();
                        if (currentId.equals(lastId)){
                            continue;
                        }
                        if (tree.hasNode(currentId)){
                            currentNode = tree.getNodeById(currentId);
                            lastNode.parent = currentNode;
                            tree.addNode(lastNode);
                            continue spLoop;
                        }else{
                            currentNode = new HTreeNode(currentId);
                            lastNode.parent = currentNode;
                            tree.addNode(lastNode);
                            lastNode = currentNode;
                            lastId = currentId;
                        }
                    }
                    if (lastNode.parent == null){
                        lastNode.parent = tree.root;
                        tree.addNode(lastNode);
                    }
                }
            }
        }
        return tree;
    }

    /**
     * instantiates levelIds for each stayPoint using the level-cluster map
     * @param initials
     * @param levelMap
     */
    public static void labelPoints(List<StayPoint> initials, Map<Integer, List<Cluster>> levelMap, boolean hasNoise) {
        spLoop:
        for (StayPoint stayPoint:initials){
            List<Integer> levelIds = new ArrayList<>();
            //DONE: removed duplicate level-ids.
            Integer lastLevel = -1;

            for (Integer level:levelMap.keySet()){
                Integer current = getParentId(levelMap.get(level), stayPoint);
                if (current==null){
                    if (hasNoise){
                        stayPoint.clusterID = -1;
                        break;
                    }else {
                        System.err.println("error in tree structure");
                    }
                }else {
                    if (!current.equals(lastLevel)) {
                        levelIds.add(current);
                    }
                    lastLevel = current;
                }
            }
            if (levelIds.isEmpty()){
                levelIds.add(stayPoint.clusterID);
            }
            stayPoint.levelIds = levelIds;
        }
    }

    private static Integer getParentId(List<Cluster> clusters, StayPoint stayPoint) {
        for (Cluster c:clusters){
            if (c.hasThis(stayPoint)){
                return c.getIndex();
            }
        }
        return null;
    }
    public static List<List<StayPoint>> makeDailySequences(List<StayPoint> mainSequence, String username) {
        if (mainSequence.isEmpty()){
            return null;
        }
        List<List<StayPoint>> dailySequenceList = new ArrayList<>();
        List<StayPoint> dailySequence = new ArrayList<>();;
        SPDate last_time = new SPDate(mainSequence.get(0).timeString);
        for (int i = 1; i < mainSequence.size(); i++) {
            StayPoint spz = mainSequence.get(i);
            SPDate current_time = new SPDate(spz.timeString);
            if (current_time.start.day == last_time.end.day) {
                dailySequence.add(spz);
                if (current_time.start.day != current_time.end.day) {
                    dailySequenceList.add(dailySequence);
                    dailySequence = new ArrayList<>();
                    dailySequence.add(spz);
                }
            } else {
                dailySequenceList.add(dailySequence);
                dailySequence = new ArrayList<>();
                dailySequence.add(spz);
                if (current_time.start.day != current_time.end.day) {
                    dailySequenceList.add(dailySequence);
                    dailySequence = new ArrayList<>();
                    dailySequence.add(spz);
                }
            }
            last_time = current_time;
        }
        return dailySequenceList;
    }

    public static List<DailySequence> filterSequences(List<DailySequence> dailySeq, boolean separateSPPrediction) {
        int counter=0;
        List<DailySequence> dailySequences = new ArrayList<>();
        System.out.println("dailySequences count:"+dailySeq.size());
        if (separateSPPrediction) {
            dailySequences = dailySeq;
        }
        //Note: removing duplicates
        else{
            for (DailySequence ds:dailySeq){
                while(ClusterFunctions.hasDuplicateItem(ds.list)){
                    counter++;
                    ds.list = ClusterFunctions.filterDuplicate(ds.list);
                }
                dailySequences.add(ds);
            }
            System.out.println("duplicates consecutive items found:"+counter);
        }
        //note: removing shorter than two.
        dailySequences = dailySequences.stream()
                    .filter(ds->ds.list.size()>1).collect(Collectors.toList());
        System.out.println("dailySequences count after filter:"+dailySequences.size());
        return dailySequences;
    }

    /**
     * The reason for this filter is ignoring effects of noise points (when there exist any)
     * after saving predicted sample indexes.
     * @param ds
     * @param separateSPPrediction
     * @return
     */
    public static DailySequence filterSingleSequence(DailySequence ds, boolean separateSPPrediction) {
        DailySequence dailySequence = new DailySequence(ds.id,new ArrayList<>());
        //Note: removing duplicates
        if (!separateSPPrediction){
            while(ClusterFunctions.hasDuplicateItem(ds.list)){
                ds.list = ClusterFunctions.filterDuplicate(ds.list);
            }
        }
        //note: removing noises
        dailySequence.list = ds.list.stream().filter(a -> a.clusterID > 0).collect(Collectors.toList());

        //note: removing shorter than two.
        if (dailySequence.list.size()<2){
            dailySequence.list.clear();
        }
        return dailySequence;
    }

    public static List<StayPoint> filterDuplicate(List<StayPoint> ds) {
        Integer last = ds.get(0).levelIds.get(0);
        Integer lastInd = 0;
        for (int i = 1; i < ds.size(); i++) {
            if (ds.get(i).levelIds.get(0).intValue() == last){
                List<StayPoint> noo = new ArrayList<>();
                for (int j = 0; j < ds.size(); j++) {
                    if (j==lastInd){
                        StayPoint mixed = new StayPoint(ds.get(lastInd));
                        mixed.leaveTime = ds.get(lastInd+1).leaveTime;
                        noo.add(mixed);
                        j++;
                    }
                    else{
                        noo.add(ds.get(j));
                    }
                }
                return noo;
            }else{
                last = ds.get(i).levelIds.get(0);
                lastInd = i;
            }
        }
        return ds;
    }

    public static Cluster findCluster(Map<Integer, List<Cluster>> clustered, Integer pValue) {
        for (Map.Entry<Integer, List<Cluster>> entry:clustered.entrySet()){
            for (Cluster cluster:entry.getValue()){
                if (cluster.getIndex() == pValue){
                    return cluster;
                }
            }
        }
        return null;
    }

    public static boolean hasDuplicateItem(List<StayPoint> ds) {
        if (ds.size()<2){
            return false;
        }
        Integer last = ds.get(0).levelIds.get(0);
        for (int i = 1; i < ds.size(); i++) {
            if (ds.get(i).levelIds.get(0).intValue() == last){
                return true;
            }else{
                last = ds.get(i).levelIds.get(0);
            }
        }
        return false;
    }

    public static String stayPointListToStringByLevel(List<StayPoint> points, int level, boolean removeDuplicated){
        StringBuilder builder = new StringBuilder();
        try{
            if (!removeDuplicated) {
                points.forEach(a -> builder.append(a.levelIds.get(level) + ","));
            }else{
                Integer last = 0;
                for (StayPoint a:points){
                    if(!last.equals(a.levelIds.get(level))){
                        builder.append(a.levelIds.get(level)+",");
                        last = a.levelIds.get(level);
                    }
                }
            }
            return builder.toString();
        }catch (Exception e){
            return "error in level ids";
        }
    }

    public static void stayPointFrequencyStatistics(List<StayPoint> initials, List<List<StayPoint>> dailySequences) {
        Set<Integer> lOneAll = initials.stream().map(s->s.levelIds.get(s.levelIds.size()-1)).collect(Collectors.toSet());
        System.out.println(lOneAll.size());
        Map map = dailySequences.stream().map(l->l.get(l.size()-2).levelIds.get(0)).collect(Collectors.groupingBy(a->a, Collectors.counting()));
        System.out.println(dailySequences.size());
        System.out.println(map.size());
        System.out.println(map);
    }

    public static Map<Integer, Cluster> readDBScanLocations(){
        Map<Integer, Cluster> cMap = new HashMap<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(AprioriRunner.staypointsSpecURL+"\\locations.txt"));
            String line = "";
            while ((line = reader.readLine())!=null){
                StringTokenizer tokenizer = new StringTokenizer(line);
                Integer id = Integer.parseInt(tokenizer.nextToken());
                StayPoint sp = new StayPoint(Double.parseDouble(tokenizer.nextToken()),Double.parseDouble(tokenizer.nextToken()));
                sp.diameter = Double.parseDouble(tokenizer.nextToken());
                Cluster c = new Cluster(Arrays.asList(sp),id);
                cMap.put(id,c);
//                StayPoint sp = new StayPoint();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return cMap;
    }

    public static Map<String, double[]> predictUsingOtherUsers(Map<String, List<PredictionTest>> unpredictedTests
            , Map<String, Map<Sequence, Integer>> lengthTwoSeqs, Map<String, Map<Integer, Cluster>> userLocationsMap) {
        Map<String, double[]> resultMap = new HashMap<>();
        overUserNames:
        for (Map.Entry<String,List<PredictionTest>> unpredicted:unpredictedTests.entrySet()){
            String username = unpredicted.getKey();
            //0:allCount, 1:matched, 2:truePrediction, 3:errorSum, 4:randErrorSum
            double[] stat = new double[5];
            stat[0] = unpredicted.getValue().size();
            int matched = 0; int truePrediction = 0; double errorSum = 0.0; double randErrorSum = 0.0;
            overPredictionTests:
            for (PredictionTest test:unpredicted.getValue()){
                Map<Sequence, Tuple2C<Tuple2C<Integer,String>,Cluster>> candidates = new HashMap<>();
                StayPoint lastStayPoint = test.head.get(test.head.size()-1);
                Map<Integer, Cluster> currentUserCluster;
                overOtherUserNames:
                for (String user:lengthTwoSeqs.keySet()){
                    currentUserCluster = userLocationsMap.get(user);
                    Cluster curr;
                    overOtherSequences:
                    for (Sequence twoLenSeq:lengthTwoSeqs.get(user).keySet()){
                        if ((curr = currentUserCluster.get(twoLenSeq.getItemSets().get(0).getItems().get(0).getItemId()))!=null
                                && curr.distanceTo(lastStayPoint)<curr.getRadius()){
                            candidates.put(twoLenSeq,new Tuple2C<>(new Tuple2C<>(lengthTwoSeqs.get(user).get(twoLenSeq),user),curr));
                        }
                    }
                }
                if (candidates.isEmpty()){
                    continue overPredictionTests;
                }
                candidates = MapUtil.sortByValue(candidates);
                Iterator<Sequence> it = candidates.keySet().iterator();
                Map<Sequence,Integer> itemScoreMap = new HashMap<>();
                Sequence seq;
                for (int i = 0; i < Math.max(Math.min(candidates.size() / 5, 10),1); i++) {
                    seq = it.next();
                    itemScoreMap.put(seq, candidates.get(seq).getT1().getT1());
                }
                itemScoreMap = MapUtil.sortByValue(itemScoreMap,Comparator.reverseOrder());
                if (!itemScoreMap.isEmpty()){
                    matched++;
                    seq = itemScoreMap.keySet().iterator().next();
                    Cluster next = userLocationsMap.get(candidates.get(seq).getT1().getT2())
                            .get(seq.getItemSets().get(1).getItems().get(0).getItemId());
                    if (lastStayPoint.levelIds.contains(next.getIndex())){
                        truePrediction++;
                    }
                    randErrorSum += next.distanceTo(lastStayPoint);
                    errorSum += next.distanceTo(test.next);
                }
            }
            stat[1]=matched; stat[2]=truePrediction; stat[3]=errorSum; stat[4]=randErrorSum;
            resultMap.put(unpredicted.getKey(),stat);
        }
        return resultMap;
    }

    public static Map<Integer, Cluster> readDBScanLocations(List<Location> dbLocation) {
        Map<Integer,Cluster> clusterMap = new HashMap<>();
        for (Location l:dbLocation){
            if (l.getId()>0) {
                StayPoint sp = new StayPoint(l.easting, l.northing);
                sp.diameter = l.diameter;
                clusterMap.put(l.getId(), new Cluster(Arrays.asList(sp), l.getId()));
            }
        }
        return clusterMap;
    }
}