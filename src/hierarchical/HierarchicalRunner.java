package hierarchical;

import datapreparation.Location;
import datapreparation.LocationDetection;
import datapreparation.StayPoint;
import prediction.util.AprioriRunner;
import sequence.DailySequence;
import sequence.Function;
import sequence.PredictionTest;
import sequence.Sequence;
import stt.GridClustering;
import utility.FunctionStore;
import utility.MapUtil;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class HierarchicalRunner {
    public static int globalClusterIndexer = 1;
    public static Map<String, List<StayPoint>> usrSpMap = new TreeMap<>();
    public static Map<String, Integer> countMap = new TreeMap<>();
    public static Map<Integer, Location> tempLocMap = new HashMap<>();
    public static Map<String,Map<Sequence,Integer>> lengthTwoSeqs = new HashMap<>();
    public static Map<String,List<PredictionTest>> unpredicted = new HashMap<>();
    public static Map<String,Map<Integer, Cluster>> locations = new HashMap<>();
    public static Map<Integer, Cluster> firstLevelClusterMap = new HashMap<>();
    public static List<String> errorMap = new ArrayList<>();

    public static boolean matchLowDepth = true;//this is our way now
    public static boolean pruneParentChild = false;//removes case when {a->a' && a'->a}
    public static boolean pruneSameConsecutive = false;//removes cases when {a->a}
    public static boolean keepShorterSequences = false;//Note: during matching low depth, keep shorter than max length sequences
    public static boolean matchLowTargetFirst = false;    //Note: true: from final candidates, chooses lowest targets and chooses best score(sup).// false: removes parents from final candidates,
    public static boolean separateSPPrediction = true;//Note: use stay points to predict locations

    public static String staypointsURL = "staypoints(30,200)";
    public static String testIndicesReadPath = System.getProperty("user.home")+"\\Desktop\\indices-read.txt";
    public static String testIndicesReadPath1 = System.getProperty("user.home")+"\\Desktop\\indices-read1.txt";
    public static String testIndicesWritePath = System.getProperty("user.home")+"\\Desktop\\indices-write.txt";
    public static String testIndicesRandPath = System.getProperty("user.home")+"\\Desktop\\indices-rand.txt";
    public static final int MATCHING_MAX_GAP = 0;
    public static final int MAKING_MAX_GAP = 0;
    public static boolean removeDBScanNoises = false; //@- ChangeId:1.1.1

    public static final String LONGEST_COMMON_SUFFIX = "longest_common_suffix";
    public static final String LAST_ITEM = "last_item";
    public static final String ALL_MATCH = "all_match";
    public static int DbMinPts= 1;
    public static int DbRadius = 80;

    public static double trainRatio = 0.8;
    public static double validationRatio = 0.2;
    public static boolean validation = false;
    public static double minSup = 0.01;

    public static double minConf = 0.03;

    public static boolean randomSelection = true;
    public static boolean nearestRand = true; // MUST be != firstRand
    public static boolean firstRand = false;//when no match and randomSelection, picks {a,b->?}:{b}

    public static boolean readTestIndices = false;
    public static boolean writeTestIndices = false;

    //Note: Model Selection
    public static boolean useDBScan = false;
    public static boolean gridClustering = false;
    public static boolean PTreeModel = false;
    public static boolean morzyModel = true;
    public static boolean useCutOffs = true;

    public static boolean harshPrediction = true;

//        public static List<Integer> cutOffs = new ArrayList<>();
    public static List<Integer> cutOffs = Arrays.asList(20, 80, 150, 240);
//    public static List<Integer> cutOffs = Arrays.asList(20, 50, 60, 70, 80, 100, 200, 300, 500, 800, 1500, 3000, 5000, 7000);
//    public static List<Integer> cutOffList = Arrays.asList(20, 30, 40, 50, 60, 70, 80,100, 150, 200, 250, 300, 400, 500, 800, 1500, 2000, 2500, 3000,4000, 5000, 7000);
//    public static List<Integer> cutOffs = Arrays.asList(100, 200, 300, 500, 1000, 3000, 5000, 7000);

    public static void main(String[] args) {
//        for (int JJ = 1; JJ < 11; JJ++) {
        for (int JJ = 1; JJ < 2; JJ++) {
//            GridClustering.unit = 20*JJ;
//            DbMinPts = JJ;
            for (int KK = 1; KK < 2; KK++) {
//            for (int KK = 1; KK < 10; KK++) {
//                GridClustering.min_stayPoints = KK;
//                GridClustering.min_intersects = KK;
//                DbRadius = 10+KK*5;
                for (int LL = 1; LL < 2 ; LL++) {
                    for (int MM = 3; MM < 4; MM++) {
                        //minPts units
//                        String tempStr = ""+ DbMinPts+"\t"+DbRadius;
                        String tempStr = ""+ GridClustering.min_stayPoints+"\t"+GridClustering.unit;
//                      testIndicesWritePath = System.getProperty("user.home")+"\\Desktop\\indices-write"+LL+".txt";
                        try {
                            if (useDBScan) {
                                removeDBScanNoises = true;
                            }
                            // <&><&><&><&><&><&><&><&><&><&><&>
                            long time = System.currentTimeMillis();
                            //Note: choosing users
                            List<String> validUsers = FunctionStore.getValidUsers("result\\stayPointCount.csv", 60);
//                validUsers.remove("153");
//                        validUsers = Arrays.asList("174");
                            // <&><&><&><&><&><&><&><&><&><&><&>
                            // Reading and Preparing StayPoints
                            if (removeDBScanNoises) {
//                            FunctionStore.readSequences(usrSpMap, countMap, AprioriRunner.staypointsSpecURL, tempLocMap, true);
                                FunctionStore.readSequences(usrSpMap, countMap, staypointsURL, tempLocMap, false);
                            } else {
                                FunctionStore.readSequences(usrSpMap, countMap, staypointsURL, tempLocMap, false);
                            }


                            Map<String, double[]> stats = new HashMap<>();
                            for (String username : validUsers) {
                                System.out.print("username:'" + username + "'\n");
                                Map<Integer, Cluster> clusterMap = null;
                                //Note: cutOff
//                            if (username.equals("030")||username.equals("035")||username.equals("036")||username.equals("038")){
//                                cutOffs = Arrays.asList(20, 50, 80, 120, 200, 300, 500, 800);
//                            }else{
//                                cutOffs = Arrays.asList(20,30, 40, 50, 60, 70, 80,100, 150, 200, 250, 300, 400, 500, 800,1100, 1500);
//                            }
                                // Note: getting ready for new set
                                globalClusterIndexer = 1;
                                System.gc();

                                // <&><&><&><&><&><&><&><&><&><&><&>
                                // Labeling StayPoints with Location Information
                                List<StayPoint> initials = usrSpMap.get(username);
                                List<Cluster> seed = new ArrayList<>();
                                Map<Integer, List<Cluster>> clustered = null;

                                if (removeDBScanNoises) {
//                        initials = initials.stream().filter(a -> a.clusterID > 0).collect(Collectors.toList());
                                }
                                if (useDBScan) {
                                    List<Location> dbLocation = new LocationDetection().DBSCAN(new ArrayList<>(initials), DbRadius, DbMinPts);
                                    initials.forEach(s -> s.levelIds = Arrays.asList(s.clusterID));
                                    if (clusterMap == null) {
                                        clusterMap = ClusterFunctions.readDBScanLocations(dbLocation);
                                        //if no location found, make a sample
                                        if (clusterMap.isEmpty()) {
                                            StayPoint sp = StayPoint.getAverageSP(initials);
                                            clusterMap.put(Integer.MAX_VALUE, new Cluster(Arrays.asList(sp), Integer.MAX_VALUE));
                                        }
                                        //
                                        firstLevelClusterMap = clusterMap;
                                    }
                                } else if (gridClustering) {
                                    clusterMap = GridClustering.runner(initials, username);
                                    //if no location found, make a sample
                                    if (clusterMap.isEmpty()) {
                                        StayPoint sp = StayPoint.getAverageSP(initials);
                                        clusterMap.put(Integer.MAX_VALUE, new Cluster(Arrays.asList(sp), Integer.MAX_VALUE));
                                    }
                                    //
                                    firstLevelClusterMap = clusterMap;
//                    initials = initials.stream().filter(a -> a.clusterID > 0).collect(Collectors.toList());
                                } else {
                                    System.out.println("clustering...");
                                    if (useCutOffs) {
                                        clustered = ClusterFunctions.startClustering(initials, seed, cutOffs);
                                    } else {
                                        clustered = ClusterFunctions.startClustering(initials, seed);
                                    }
                                    if (clustered.isEmpty()) {
                                        continue;
                                    }
                                    System.out.println("point labeling...");
                                    ClusterFunctions.labelPoints(initials, clustered, false);
                                    clusterMap = FunctionStore.getPointedClusters(clustered);
                                    firstLevelClusterMap = new HashMap<>();
                                    //Note: adding only first level clusters to corresponding list
                                    for (StayPoint sp : initials) {
                                        if (clusterMap.containsKey(sp.levelIds.get(0))) {
                                            firstLevelClusterMap.put(sp.levelIds.get(0), clusterMap.get(sp.levelIds.get(0)));
                                        } else {
                                            throw new Exception();
                                        }
                                    }
                                    locations.put(username, clusterMap);
                                }
                                System.out.println("making tree...");
                                HierarchicalTree tree = ClusterFunctions.makeHierarchyTree(initials);

                                // <&><&><&><&><&><&><&><&><&><&><&>
                                // Partitioning main sequence into daily sequences and filtering them
//                List<List<StayPoint>> dailySequences = ClusterFunctions.makeDailySequences(initials, username);
                                List<List<StayPoint>> dailySPList = ClusterFunctions.makeDailySequences(initials, username);
                                int cnt = 0;
                                List<DailySequence> dailySequences = new ArrayList<>();
                                if (dailySPList != null) {
                                    for (List<StayPoint> ls : dailySPList) {
                                        dailySequences.add(new DailySequence(++cnt, ls));
                                    }
                                } else {
                                    System.err.println("dailySequences are empty");
                                    continue;
                                }
                                System.out.println("average daily sequence size: " + dailySequences.stream().collect(Collectors.averagingInt(a -> a.list.size())));
                                //Note: length<2 && duplicate
                                dailySequences = ClusterFunctions.filterSequences(dailySequences, separateSPPrediction);
                                System.out.println("average daily sequence size after: " + dailySequences.stream().collect(Collectors.averagingInt(a -> a.list.size())));

                                // <&><&><&><&><&><&><&><&><&><&><&>
                                // Separating train and test data preparing for prediction
                                if (dailySequences.size() < 2) {
                                    System.err.println("dailySequences.size()<2");
                                    continue;
                                }
                                int trainEdge = 0;
                                int testEdge = 0;
                                if (validation) {
                                    trainEdge = Math.max((int) Math.round(dailySequences.size() * (trainRatio - validationRatio)), 1);
                                    testEdge = Math.max((int) Math.round(dailySequences.size() * (trainRatio)), trainEdge);
                                } else {
                                    trainEdge = Math.max((int) Math.round(dailySequences.size() * (trainRatio)), 1);
                                    testEdge = dailySequences.size();
                                }
                                List<DailySequence> trainData = dailySequences.subList(0, trainEdge);
                                List<DailySequence> testData = dailySequences.subList(trainEdge, testEdge);
//                            List<DailySequence> testData = dailySequences.subList(trainEdge, validationEdge);
                                if (gridClustering || useDBScan) {
                                    for (DailySequence dSeq : trainData) {
                                        dSeq.list = dSeq.list.stream().filter(a -> a.clusterID > 0).collect(Collectors.toList());
                                    }
                                }
                                if (separateSPPrediction) {
                                    trainData = ClusterFunctions.filterSequences(trainData, false);
                                }

                                // <&><&><&><&><&><&><&><&><&><&><&>
                                // Model creation and prediction
                                Function f = new Function();
                                //changeId 2.2.2: use f.performPrefixTree(Lei's Algorithm) instead of f.performSequential
                                double[] da;
                                if (PTreeModel) {
                                    da = f.performPrefixTree(clusterMap, trainData, testData, minSup, username);
                                } else if (morzyModel) {
                                    da = f.performApriori(trainData, testData, minSup, minConf, username, clusterMap, LAST_ITEM);
                                } else {
                                    da = f.performSequential(trainData, testData, tree, minSup, username, matchLowDepth, clusterMap, separateSPPrediction, lengthTwoSeqs, unpredicted);
                                }
                                if (da != null) {
                                    stats.put(username, da);
                                }
                            }
                            //general
//            Map<String, double[]> unPredictedResults = predictUsingOtherUsers(unpredicted, lengthTwoSeqs, locations);

                            // <&><&><&><&><&><&><&><&><&><&><&>
                            // Preparing the results generated in prediction section
                            StringBuilder logger = new StringBuilder();
                            //result header
                            logger.append("parameters:\nminimum support = ").append(minSup).append("\nrandom_selection:").append(randomSelection)
                                    .append("\ttype:").append(nearestRand ? "nearest" : "first").append("\ntrain_ratio = ").append(trainRatio)
                                    .append("\tvalidation:").append(validation).append("\nCut off values = ").append(cutOffs).append("\nsequence-making max_gap = ")
                                    .append(MAKING_MAX_GAP).append("\nmatching max gap = 0\n");
                            // Note: 0-testSamples, 1-predictions, 2-matched, 3-true, 4-errorAverage
                            stats.keySet().forEach(k ->
                                    logger.append(k).append("\t").append(Arrays.toString(stats.get(k)))
                                            .append("\t#test:").append(stats.get(k)[0])
                                            .append("\t#samples:").append(stats.get(k)[1]).append("\t#match:").append(stats.get(k)[2])
                                            .append("\t%match:").append(roundDown4(stats.get(k)[2] / stats.get(k)[1]))
                                            .append("\tacc_match:").append(randomSelection ? "--" : roundDown4(stats.get(k)[3] / stats.get(k)[2]))
                                            .append("\tacc_all:").append(roundDown4(stats.get(k)[3] / stats.get(k)[1]))
                                            .append("\terr_avg:").append(roundDown4(stats.get(k)[4] / stats.get(k)[5]))
                                            .append("\t#err:").append(stats.get(k)[5])
                                            .append("\t#rand:").append(stats.get(k)[6])
                                            .append("\t#patterns:").append(roundDown4(stats.get(k)[7])).append("\n"));

                            double[] res = stats.keySet().stream().map(q -> stats.get(q))
                                    .reduce(new double[8], (a1, a2) -> {
                                        for (int i = 0; i < a1.length; i++) {
                                            a1[i] = a1[i] + a2[i];
                                        }
                                        return a1;
                                    });
                            logger.append("All Users Average Result::\n")
                                    .append("#test:").append(res[0]).append("\t#samples:").append(res[1])
                                    .append("\t#match:").append(res[2]).append("\t%match:").append(roundDown4(res[2] / res[1]))
                                    .append("\tacc_match:").append(randomSelection ? "--" : roundDown4(res[3] / res[2]))
                                    .append("\tacc_all:").append(roundDown4(res[3] / res[1]))
                                    .append("\terr_avg:").append(roundDown4(res[4] / res[5]))
                                    .append("\t#err:").append(res[5]).append("\t#rand:").append(res[6])
                                    .append("\tavg#patterns:").append(roundDown4(res[7] / stats.size())).append("\n")
                                    .append("#users:").append(stats.size())
                                    .append("\n").append(Arrays.toString(res))
                                    .append("\n" + (System.currentTimeMillis() - time) / 1000 + " seconds");
                            System.out.println(logger.toString());
                            PrintWriter writer = new PrintWriter(new FileWriter(System.getProperty("user.home") + "\\Desktop\\res-" + JJ + "-" + KK + "-" + LL + "-" + MM+ ".txt"));
//                    PrintWriter writer = new PrintWriter(new FileWriter(System.getProperty("user.home") + "\\Desktop\\res" + LL + ".txt"));
                            writer.print(logger.toString());
                            writer.flush();
                            // minSup   error   samples match   accuracy    pattern time
                            errorMap.add(tempStr + "\t" + minSup + "\t" + roundDown4(res[4] / res[5]) + "\t" + res[1] + "\t" + roundDown4(res[2] / res[1]) + "\t" + roundDown4(res[3] / res[1]) + "\t" + roundDown4(res[7] / stats.size()) + "\t" + (System.currentTimeMillis() - time) / 1000);
//                        errorMap.add("pts:" + GridClustering.min_intersects + "\tcel:" + GridClustering.unit + "\tminSp" + minSup + "\terr:" + roundDown4(res[4] / res[5]) + "\tsamples:" + res[1] + "\tmatch:" + roundDown4(res[2] / res[1]) + "\tacc:"+ roundDown4(res[3] / res[1]) + "\t#pattern:" + roundDown4(res[7] / stats.size()) + "\ttime:" + (System.currentTimeMillis() - time) / 1000 + "s");
                            //general
            /*unPredictedResults.forEach( (k , v) -> System.out.println(k+": "+Arrays.toString(v)));
            double[] reduction = unPredictedResults.keySet().stream().map(q -> unPredictedResults.get(q))
                    .reduce(new double[5], (a1, a2) -> {
                        for (int i = 0; i < a1.length; i++) {
                            a1[i] = a1[i] + a2[i];
                        }
                        return a1;
                    });
            System.out.println("reduction:" + Arrays.toString(reduction));*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        errorMap.forEach(en -> sb.append(en).append("\n"));
        System.out.print(sb.toString());
    }

    public static double roundDown4(double d) {
        return (long) (d * 1e4) / 1e4;
    }
}