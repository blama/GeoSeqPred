package hierarchical;

/**
 * Created by sce on 8/31/2015.
 */
public class Tuple2C<T1,T2 extends Comparable<T2>> implements Comparable<Tuple2C<T1,T2>>{
    private T1 t1;
    private T2 t2;

    public Tuple2C(T1 t1, T2 t2) {
        this.t1 = t1; this.t2 = t2;
    }

    public T1 getT1() {return t1;}
    public T2 getT2() {return t2;}

    @Override
    public int compareTo(Tuple2C<T1,T2> o) {
        return t2.compareTo(o.getT2());
    }

    @Override
    public String toString() {
        return t1.toString()+":"+t2.toString();
    }
}