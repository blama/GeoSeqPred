package hierarchical;

/**
 * Created by Reza Baktash on 11/10/2015.
 */
public class HTreeNode{
    public Integer nodeValue;
    public HTreeNode parent;
    public HTreeNode(Integer nodeValue, HTreeNode parent){
        this.nodeValue = nodeValue;
        this.parent = parent;
    }
    public HTreeNode(Integer nodeValue){
        this.nodeValue = nodeValue;
    }

    @Override
    public int hashCode() {
        return nodeValue.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HTreeNode hTreeNode = (HTreeNode) o;

        return !(nodeValue != null ? !nodeValue.equals(hTreeNode.nodeValue) : hTreeNode.nodeValue != null);

    }
}
