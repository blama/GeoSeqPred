package hierarchical;

import datapreparation.StayPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

/**
 * Created by Reza Baktash on 8/30/2015.
 */
public class Cluster implements Comparable<Cluster>{
    private List<StayPoint> members;
    private int index;

    public Cluster(List<StayPoint> members, int index){
        this.members = members;
        this.index = index;
    }

    public List<StayPoint> getMembers() {return members;}

    public int getIndex() {return index;}

    /**
     * Gets two Clusters and merges them into one "new" Cluster with a new id, by adding all members of each to the new one.
     * @param i first Cluster
     * @param j second Cluster
     * @return the new Cluster created
     */
    public static Cluster merge(Cluster i, Cluster j) {
        List<StayPoint> newList = new ArrayList<>();
        newList.addAll(i.getMembers());
        newList.addAll(j.getMembers());
        return new Cluster(newList, HierarchicalRunner.globalClusterIndexer++);
    }

    public boolean hasThis(StayPoint stayPoint){
        for (StayPoint sp:members){
            if (sp.clusterID == stayPoint.clusterID){
                return true;
            }
        }
        return false;
    }

    public Double distanceTo(StayPoint last) {
        return StayPoint.distance(last, getEuclideanAverage());
    }

    public static Double getDistance(Cluster first, Cluster last){
        return StayPoint.distance(first.getEuclideanAverage(), last.getEuclideanAverage());
    }

    /**
     * @return a sample StayPoint representing average of members for euclidean parameters (east,north)
     */
    public StayPoint getEuclideanAverage(){
        OptionalDouble east = members.stream().mapToDouble(a -> a.east).average();
        OptionalDouble north = members.stream().mapToDouble(a -> a.north).average();
        double eastAvg = east.isPresent()?east.getAsDouble():0.0;
        double northAvg = north.isPresent()?north.getAsDouble():0.0;
        return new StayPoint(eastAvg, northAvg);
    }

    public double getRadius(){
        if (members.size()==1){
            return members.get(0).diameter!=0?members.get(0).diameter:1;
        }
        StayPoint center = getEuclideanAverage();
        OptionalDouble od =  members.stream().mapToDouble(m -> StayPoint.distance(center,m)).max();
        if (!od.isPresent()){
            System.err.println("Cluster Must Have Radius\nMust Have Radius");
            return 1;
        }else{
            if (od.getAsDouble()==0){
                return 1;
            }
            return od.getAsDouble();

        }
    }

    @Override
    public String toString() {
        return "cid:"+index+", cnt:"+members.size();
    }


    @Override
    public int compareTo(Cluster o) {
        return Integer.compare((int)getRadius(),(int)o.getRadius());
    }
}
