package hierarchical;

import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Reza Baktash on 11/10/2015.
 */
public class HierarchicalTree {
    private Set<HTreeNode> nodes = new HashSet<>();
    public HTreeNode root = new HTreeNode(null,null);

    public boolean crashOnRoute(@NotNull Integer start, @NotNull Integer sample) throws NodeNotFoundException{
        HTreeNode startNode = getNodeById(start);
        if (startNode==null){throw new NodeNotFoundException();}
        while (!startNode.parent.equals(root)){
            if (startNode.parent.nodeValue.equals(sample)){
                return true;
            }
            startNode = startNode.parent;
        }
        return false;
    }

    public HTreeNode getNodeById(Integer id){
        HTreeNode node = null;
        for (HTreeNode n:nodes){
            if (n.nodeValue.equals(id)){
                node = n;
                break;
            }
        }
        return node;
    }

    public boolean hasNode(Integer id){
        for (HTreeNode n:nodes){
            if (n.nodeValue.equals(id)){
                return true;
            }
        }
        return false;
    }

    /**
     * Gets a TreeNode and returns the depth of it.
     * @param node
     * @return
     */
    public int getDepth(HTreeNode node){
        int depth = 1;
        HTreeNode current = node;
        while (!current.parent.equals(root)){
            depth++;
            current = current.parent;
        }
        return depth;
    }

    public void addNode(HTreeNode toBeAdded) {
        nodes.add(toBeAdded);
    }

    public List<Integer> getAllParentIds(HTreeNode child) {
        List<Integer> result = new ArrayList<>();
        result.add(child.nodeValue);
        while (!child.parent.equals(root)){
            child = child.parent;
            result.add(child.nodeValue);
        }
        return result;
    }
}
