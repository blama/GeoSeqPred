package sequence;

import datapreparation.StayPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reza Baktash on 2/28/2016.
 */
public class DailySequence {
    public int id;
    public List<StayPoint> list;
    public DailySequence(int id, List<StayPoint> list){
        this.id = id;
        this.list = list;
    }

    @Override
    public String toString() {
        return list.toString();
    }
}
