package sequence;

import java.util.Comparator;

/**
 * Created by Reza on 10/31/2015.
 */
public class Item implements Comparable<Item>{
    public Integer itemId;
    public Item(int itemId){
        this.itemId = itemId;
    }
    public Integer getItemId(){
        return itemId;
    }

    @Override
    public int compareTo(Item o) {
        return itemId.compareTo(o.itemId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Item other = (Item)obj;
        return itemId.equals(other.itemId);
    }

    @Override
    public int hashCode() {
        return ((itemId == null) ? 0 : itemId.hashCode());
    }

    @Override
    public String toString() {
        return "("+itemId+")";
    }
}
