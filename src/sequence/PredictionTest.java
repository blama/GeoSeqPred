package sequence;

import datapreparation.StayPoint;

import java.util.List;

/**
 * Created by Reza Baktash on 1/20/2016.
 */
public class PredictionTest{
    public List<StayPoint> head;
    public StayPoint next;
    public PredictionTest(List<StayPoint> head , StayPoint next){
        this.head = head;
        this.next = next;
    }
}
