package sequence;

import datapreparation.StayPoint;
import hierarchical.HierarchicalTree;
import hierarchical.NodeNotFoundException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Reza on 10/31/2015.
 */
public class Sequence implements Comparable<Sequence>{
    private List<ItemSet> itemSets;

    public void addItemSet(ItemSet is) {
        itemSets.add(is);
    }

    public Set<Integer> getAllItemIds() {
        Set<Integer> allItemIds = new HashSet<>();
        for (ItemSet is:itemSets){
            for (Item i:is.getItems()){
                allItemIds.add(i.getItemId());
            }
        }
        return allItemIds;
    }

    public Sequence(){itemSets = new ArrayList<>();}
    public Sequence(List<ItemSet> itemSets){this.itemSets = itemSets;}

    public Integer size(){
        return itemSets.size();
    }

    public List<ItemSet> getItemSets(){
        return itemSets;
    }

    public void addAll(Sequence toBeAdded){
        for (ItemSet is:toBeAdded.itemSets){
            itemSets.add(is);
        }
    }

    /**
     *
     * @param current
     * @param maxGap number of items allowed in between
     * @return
     */
    public boolean containsSequence(Sequence current, int maxGap, boolean mustRemain) {
        //check if incoming sequence contain only single-item itemsets
        if (current.itemSets.stream().anyMatch(a->a.size()!=1)){
            System.err.println("invalid itemSets");
            return false;
        }
        if (current.size()>itemSets.size() || current.size()==0 || itemSets.size()==0){
            return false;
        }
        Iterator<ItemSet> mainItr = itemSets.iterator();
        Iterator<ItemSet> currItr = current.itemSets.iterator();
        int gap = 0;
        ItemSet mainIs = mainItr.next();
        ItemSet currIs = currItr.next();
        boolean eqHappened = false;
        while (true){
            if (eqHappened){
                gap++;
            }
            if (mainIs.hasItem(currIs.getItems().iterator().next())){
                if (!currItr.hasNext()){
                    if (mustRemain){
                        return mainItr.hasNext();
                    }
                    else{return true;}
                }
                else if (!mainItr.hasNext()){
                    return false;
                }
                else{
                    mainIs = mainItr.next();
                    currIs = currItr.next();
                    eqHappened = true;
                    gap=0;
                }
            }else if (gap<=maxGap){
                if (!mainItr.hasNext()){
                    return false;
                }
                mainIs = mainItr.next();
            }
            else{
                return false;
            }
        }
    }

    public Integer containsAndNext(Sequence current, int maxGap) {
        Integer no_result = null;
        //check if incoming sequence contain only single-item itemsets
        if (current.itemSets.stream().anyMatch(a->a.size()!=1)){
            System.err.println("invalid itemSets");
            return no_result;
        }
        if (current.size()>itemSets.size() || current.size()==0 || itemSets.size()==0){
            return no_result;
        }
        Iterator<ItemSet> mainItr = itemSets.iterator();
        Iterator<ItemSet> currItr = current.itemSets.iterator();
        int gap = 0;
        ItemSet mainIs = mainItr.next();
        ItemSet currIs = currItr.next();
        boolean eqHappened = false;
        while (true){
            if (mainIs.hasItem(currIs.getItems().iterator().next())){
                if (!currItr.hasNext()){
                    if (mainItr.hasNext()){
                        return mainItr.next().getItems().get(0).getItemId();
                    }
                    else{return no_result;}
                }
                else if (!mainItr.hasNext()){
                    return no_result;
                }
                else{
                    mainIs = mainItr.next();
                    currIs = currItr.next();
                    eqHappened = true;
                    gap=0;
                }
            }else if (gap<=maxGap){
                if (!mainItr.hasNext()){
                    return no_result;
                }
                mainIs = mainItr.next();
                if (eqHappened){
                    gap++;
                }
            }
            else{
                return no_result;
            }
        }
    }
    /*
    @Override
    public int compareTo(Sequence o) {
        Iterator<ItemSet> thisIter = itemSets.iterator();
        Iterator<ItemSet> anotherIter = o.itemSets.iterator();
        int diffSum = 0;
        while (thisIter.hasNext()) {
            if (!anotherIter.hasNext()) {
                return 1;
            }
            ItemSet i1 = thisIter.next();
            ItemSet i2 = anotherIter.next();

            int diff = i1.compareTo(i2);
            if (diff > 0) {
                diffSum++;
            }else if (diff < 0){
                diffSum--;
            }
        }

        if (anotherIter.hasNext()) {
            return -1;
        } else {
            return diffSum;
        }
    }*/

    @Override
    public int compareTo(Sequence o) {
        Iterator<ItemSet> thisIter = itemSets.iterator();
        Iterator<ItemSet> anotherIter = o.itemSets.iterator();

        while (thisIter.hasNext()) {
            if (!anotherIter.hasNext()) {
                return 1;
            }

            ItemSet i1 = thisIter.next();
            ItemSet i2 = anotherIter.next();

            int diff = i1.compareTo(i2);
            if (diff != 0) {
                return diff;
            }
        }

        if (anotherIter.hasNext()) {
            return -1;
        } else {
            return 0;
        }
    }


    public boolean hasItem(int itemId){
        for (ItemSet is:itemSets){
            for (Item i : is.getItems()){
                if (i.getItemId()==itemId){
                    return true;
                }
            }
        }
        return false;
    }

    public static Sequence makeSequence(List<StayPoint> list, int [] indices){
        List<ItemSet> itemSets = new ArrayList<>();
        Integer lastItem = -1;
        for (int i = 0; i < list.size(); i++) {
            Integer in = list.get(i).levelIds.get(indices[i]);
            if (in.equals(lastItem)){
                continue;
            }else {
                ItemSet is = new ItemSet();
                //DONE: does not check equality of two consequent levels
                is.addItem(new Item(list.get(i).levelIds.get(indices[i])));
                itemSets.add(is);
                lastItem = in;
            }
        }
        return !itemSets.isEmpty()?new Sequence(itemSets):null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sequence other = (Sequence) obj;
		if (this.compareTo(other)==0)
			return true;
        return false;
    }

    @Override
    public int hashCode() {
        return itemSets.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (ItemSet is:itemSets){
            sb.append(is);
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * checks whether the items of this sequence are parents of given sequence
     * @param sequence
     * @param tree
     * @return
     */
    public boolean isParentOf(Sequence sequence, HierarchicalTree tree) {
        int[] thisList = itemSets.stream().mapToInt(itemSet -> itemSet.getItems().get(0).getItemId()).toArray();
        int[] thatList = sequence.itemSets.stream().mapToInt(itemSet -> itemSet.getItems().get(0).getItemId()).toArray();
        for (int i = 0; i < thisList.length; i++) {
            if (thisList[i] > thatList[i]){
                try {
                    if (!tree.crashOnRoute(thatList[i],thisList[i])){
                        return false;
                    }
                } catch (NodeNotFoundException e) {
                    e.printStackTrace();
                }
            }else if (thisList[i] < thatList[i]){
                return false;
            }
        }
        return true;
    }

    public boolean isSingleItem(){
        for (ItemSet is:itemSets){
            if (is.size()>1) return false;
        }
        return true;
    }

    /**
     * makes a copy of this Sequence creating all objects
     * @return
     */
    public Sequence copy() {
        Sequence ret = new Sequence();
        for (ItemSet is:itemSets){
            ret.addItemSet(new ItemSet(Collections.singletonList(new Item(is.getItems().get(0).getItemId()))));
        }
        return ret;
    }
}
