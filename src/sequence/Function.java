package sequence;

import datapreparation.Location;
import datapreparation.SPDate;
import datapreparation.StayPoint;
import hierarchical.*;
import prediction.aprioriall.ApriorAllModelData;
import prediction.aprioriall.ApriorAllRule;
import prediction.aprioriall.ApriorPrecision;
import prediction.aprioriall.AprioriAll;
import prediction.location.UserLocation;
import prediction.location.UserSequence;
import stt.PrefixTree;
import stt.STTRunner;
import utility.FunctionStore;
import utility.MapUtil;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Created by Reza on 10/31/2015.
 */

public class Function {
    public static double length_multiplier = 0.25;public static double match_depth_multiplier = 0.25;public static double support_multiplier = 0.25;public static double target_depth_multiplier = 0.25;//public static double confidence_multiplier = 0.2;

    public double[] performSequential(List<DailySequence> trainList
            , List<DailySequence> testList, HierarchicalTree tree, double minSup, String username
            , boolean matchLowDepth, Map<Integer, Cluster> clusterMap, boolean separateSPPrediction
            , Map<String, Map<Sequence, Integer>> lengthTwoSeqs, Map<String, List<PredictionTest>> unpredicted) throws Exception {
        List<Sequence> trainData = transformStayPoints(trainList);
        System.out.println("frequent sequence mining...");
//        Map<Sequence, Integer> frequentSequencesMap = generateFrequentSeqs(trainData, tree, minSup);
        Map<Sequence, Integer> frequentSequencesMap = generateFrequentSequences(trainData, tree, minSup);
        int patternCount = frequentSequencesMap.size();
        System.out.println("predicting...");
        if (separateSPPrediction){
            if (HierarchicalRunner.harshPrediction) {
                return stayPointPredictionHarsh(testList, frequentSequencesMap, tree, username, matchLowDepth, clusterMap, patternCount);
            }else {
                return stayPointPrediction(testList, frequentSequencesMap, tree, username, matchLowDepth, clusterMap, unpredicted, patternCount);
            }
        }
        else {
            return prediction(testList, frequentSequencesMap, tree, username, matchLowDepth, clusterMap);
        }
    }
    //Note: soft
    private double[] stayPointPrediction(List<DailySequence> testData, Map<Sequence, Integer> frequentSequencesMap
            , HierarchicalTree tree, String username, boolean matchLowDepth, Map<Integer, Cluster> clusterMap
            , Map<String, List<PredictionTest>> unpredicted, int patternCount) throws Exception {
        // Note: 0-testSamples, 1-predictions, 2-matched, 3-true, 4-errorAverage, 5-errorCount, 6-randCount, 7 - patterns count
        List<PredictionTest> unpredictedList = new ArrayList<>();
        List<Double> distanceError = new ArrayList<>();
        List<String> exploitedSamples = new ArrayList<>();
        List<String> matchWrite = new ArrayList<>();
        double[] stats = new double[8];
        stats[7]=patternCount;
        int true_label = 0;
        int matched = 0;
        int allTests = 0;
        int errorCount = 0;
        int randCount = 0;
        //
        OptionalDouble od = clusterMap.values().stream().mapToDouble(Cluster::getRadius).max();
        double maxClusterRadius = od.isPresent()?od.getAsDouble():1;
        OptionalInt optionalMaxSupport = frequentSequencesMap.values().stream().mapToInt(Integer::intValue).max();
        int maxSupport = 0;
        List<String> randWrite = new ArrayList<>();
        if (optionalMaxSupport.isPresent()){
            maxSupport = optionalMaxSupport.getAsInt();
        }
        List<String> other = new ArrayList<>();
        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
            other = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath1);
        }
        for (DailySequence dailySequence: testData){
            DailySequence testSample = ClusterFunctions.filterSingleSequence(dailySequence,true);
            for (int i = 1; i < testSample.list.size(); i++) {
                int idx = dailySequence.list.indexOf(testSample.list.get(i));
                if (HierarchicalRunner.readTestIndices){
//                    if (!exploitedSamples.contains(testSample.id+" "+idx)){
                    if (!exploitedSamples.contains(testSample.id+" "+idx) && !other.contains(testSample.id+" "+idx)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
//                while(ClusterFunctions.hasDuplicateItem(head)){
//                    head = ClusterFunctions.filterDuplicate(head);
//                }s
                Integer lastId = head.get(head.size()-1).levelIds.get(0);
                StayPoint next = null;
                for (int j = i; j < testSample.list.size(); j++) {
                    next = testSample.list.get(j);
                    if (!next.levelIds.get(0).equals(lastId)) {
                        break;
                    }
                }
//                if (next!=null){
//                    if(next.levelIds.get(0).equals(lastId)) {
//                        continue;
//                    }
//                }else{
//                    throw new Exception("");
//                }
//                System.out.println("id:"+testSample.id+ "-"+i+"\ntest sequence: " + testSample);
//                System.out.println("head sequence: " + head + "\treal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                Integer pValue = whatNext(head, frequentSequencesMap, tree, clusterMap, maxClusterRadius, maxSupport, matchLowDepth, useRand);
//                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    //
                    if (HierarchicalRunner.writeTestIndices) {
                        if (useRand.get()){
                            randWrite.add(testSample.id + " " + idx);
                        }else{
                            matchWrite.add(testSample.id+" "+idx);
                        }
                    }
                    //
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                        throw new Exception();
                    }
                    else {
                        distanceError.add(predicted.distanceTo(next));
                        errorCount++;
//                        System.out.println("\terror:" + predicted.distanceTo(next)+"\n");
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                }else{
                    unpredictedList.add(new PredictionTest(head,next));
                }
                //
//                System.out.println("\terror predicting itself:" + Cluster.getDistance(clusterMap.get(next.levelIds.get(0)),clusterMap.get(testSample.get(i-1).levelIds.get(0))));
            }
        }
        unpredicted.put(username,unpredictedList);
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum(); stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                matchWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesRandPath, true)));
                writer.append("u:").append(username).append("\n");
                randWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ///temp
        }
        return stats;
    }
    //Note: harsh
    private double[] stayPointPredictionHarsh(List<DailySequence> testData, Map<Sequence, Integer> frequentSequencesMap
            , HierarchicalTree tree, String username, boolean matchLowDepth, Map<Integer, Cluster> clusterMap
            , int patternCount) throws Exception {
        // Note: 0-testSamples, 1-predictions, 2-matched, 3-true, 4-errorAverage, 5-errorCount, 6-randCount, 7 - patterns count
        List<Double> distanceError = new ArrayList<>();
        List<String> exploitedSamples = new ArrayList<>();
        List<String> matchWrite = new ArrayList<>();
        double[] stats = new double[8];
        stats[7]=patternCount;
        int true_label = 0;
        int matched = 0;
        int allTests = 0;
        int errorCount = 0;
        int randCount = 0;
        //
        OptionalDouble od = clusterMap.values().stream().mapToDouble(Cluster::getRadius).max();
        double maxClusterRadius = od.isPresent()?od.getAsDouble():1;
        OptionalInt optionalMaxSupport = frequentSequencesMap.values().stream().mapToInt(Integer::intValue).max();
        int maxSupport = 0;
        List<String> randWrite = new ArrayList<>();
        if (optionalMaxSupport.isPresent()){
            maxSupport = optionalMaxSupport.getAsInt();
        }
        List<String> other = new ArrayList<>();
        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
            other = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath1);
        }
        for (DailySequence testSample: testData){
//            DailySequence testSample = ClusterFunctions.filterSingleSequence(dailySequence,true);
            for (int i = 1; i < testSample.list.size(); i++) {
                int idx = i;
                if (HierarchicalRunner.readTestIndices){
                    if (!exploitedSamples.contains(testSample.id+" "+idx)){
//                    if (!exploitedSamples.contains(testSample.id+" "+idx) && !other.contains(testSample.id+" "+idx)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
                head = head.get(head.size()-1).clusterID>0? head.stream().filter(a->a.clusterID>0).collect(Collectors.toList()):head;
                Integer lastId = head.get(head.size()-1).levelIds.get(0);
                StayPoint next = null;
                for (int j = i; j < testSample.list.size(); j++) {
                    next = testSample.list.get(j);
                    if (!next.levelIds.get(0).equals(lastId)) {
                        break;
                    }
                }
//                if (next!=null){
//                    if(next.levelIds.get(0).equals(lastId)) {
////                        continue;
//                    }
//                }else{
//                    throw new Exception("");
//                }
//                System.out.println("id:"+testSample.id+ "-"+i+"\ntest sequence: " + testSample);
//                System.out.println("head sequence: " + head + "\treal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                Integer pValue = whatNext(head, frequentSequencesMap, tree, clusterMap, maxClusterRadius, maxSupport, matchLowDepth, useRand);
//                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    //
                    if (HierarchicalRunner.writeTestIndices) {
                        if (useRand.get()){
                            randWrite.add(testSample.id + " " + idx);
                        }else{
                            matchWrite.add(testSample.id+" "+idx);
                        }
                    }
                    //
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                        throw new Exception();
                    }
                    else {
                        distanceError.add(predicted.distanceTo(next));
                        errorCount++;
//                        System.out.println("\terror:" + predicted.distanceTo(next)+"\n");
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                }else{
                }
                //
//                System.out.println("\terror predicting itself:" + Cluster.getDistance(clusterMap.get(next.levelIds.get(0)),clusterMap.get(testSample.get(i-1).levelIds.get(0))));
            }
        }
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum(); stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                matchWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesRandPath, true)));
                writer.append("u:").append(username).append("\n");
                randWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ///temp
        }
        return stats;
    }

    public double[] performPrefixTree(Map<Integer, Cluster> clusterMap, List<DailySequence> trainList, List<DailySequence> testList, double minSup, String username){
        List<String> trainData = new ArrayList<>();
        trainList.forEach(spl -> {
            StringBuilder temp = new StringBuilder();
            spl.list.forEach(sp -> temp.append(sp.levelIds.get(0)).append(","));
            trainData.add(temp.substring(0, temp.length() - 1));
        });
        System.out.println("making prefix tree...");
        String [] array = new String[trainData.size()];
        for (int i = 0; i < trainData.size(); i++) {
            array[i] = trainData.get(i);
        }
        PrefixTree prefixTree = STTRunner.makeTree(array, minSup);
        if (prefixTree.nodes.isEmpty()){
            return null;
        }
        System.out.println("predicting...");
        if (HierarchicalRunner.harshPrediction){
            return STTRunner.pTreePredictionHarsh(testList, prefixTree, clusterMap, username);
        }else {
            return STTRunner.pTreePrediction(testList, prefixTree, clusterMap, username);
        }
    }

    private double[] prediction(List<DailySequence> testData, Map<Sequence, Integer> frequentSequencesMap
            , HierarchicalTree tree, String username, boolean matchLowDepth, Map<Integer, Cluster> clusterMap) throws Exception {
        // Note: 0-testSamples, 1-predictions, 2-matched, 3-true, 4-errorAverage, 5-errorCount, 6-randCount
        double[] stats = new double[7];
        List<Double> distanceError = new ArrayList<>();
        List<String> exploitedSamples = new ArrayList<>();
        int true_label = 0;
        int matched = 0;
        int allTests = 0;
        int errorCount = 0;
        int randCount = 0;
        //
        /*List<Double> distances = new ArrayList<>();
        for (List<StayPoint> spl:testData){
            for (int i = 0; i < spl.size()-1; i++) {
                distances.add(Cluster.getDistance(clusterMap.get(spl.get(i).levelIds.get(0)),clusterMap.get(spl.get(i+1).levelIds.get(0))));
            }
        }
        OptionalDouble odo = distances.stream().mapToDouble(Double::doubleValue).average();
        System.out.println(odo.getAsDouble());*/
        //
        OptionalDouble od = clusterMap.values().stream().mapToDouble(Cluster::getRadius).max();
        double maxClusterRadius = od.getAsDouble();
        int maxSupport = frequentSequencesMap.values().stream().mapToInt(Integer::intValue).max().getAsInt();
        //
        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
        }
        for (DailySequence testSample: testData){
            for (int i = 1; i < testSample.list.size(); i++) {
                if (HierarchicalRunner.readTestIndices){
                    if (!exploitedSamples.contains(testSample.id+" "+i)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
                StayPoint next = (StayPoint)testSample.list.get(i);
                System.out.println("test sequence: " + testSample);
                System.out.println("head sequence: " + head + "\nreal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                Integer pValue = whatNext(head, frequentSequencesMap, tree, clusterMap, maxClusterRadius, maxSupport, matchLowDepth, useRand);
                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    //
                    if (HierarchicalRunner.writeTestIndices) {
                        exploitedSamples.add(testSample.id+" "+i);
                    }
                    //
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                        throw  new Exception();
                    }
                    else {
                        distanceError.add(predicted.distanceTo(next));
                        errorCount++;
                        System.out.println("\terror:" + predicted.distanceTo(next));
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                }
                //error predicting itself instead
//                System.out.println("\terror predicting itself:" + Cluster.getDistance(clusterMap.get(next.levelIds.get(0)),clusterMap.get(testSample.get(i-1).levelIds.get(0))));
            }
        }
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum(); stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                exploitedSamples.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stats;
    }

    public static List<String> readTestIndicesFromFile(String username, String testIndicesPath) {
        List<String> pairs = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(testIndicesPath));
            String line;
            boolean inScope = false;
            while ((line = reader.readLine())!=null){
                if (!inScope){
                    if (line.startsWith("u:" + username)){
                        inScope = true;
                    }
                }else{
                    if (line.startsWith("u:")){
                        break;
                    }
                    pairs.add(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pairs;
    }

    private Integer whatNext(List<StayPoint> head, Map<Sequence, Integer> frequentSequencesMap, HierarchicalTree tree
            ,Map<Integer, Cluster> clusterMap, double maxClusterRadius, int maxSupport, boolean matchLowDepth, AtomicBoolean useRand) throws NodeNotFoundException {
        Map<Integer, Double> itemScoreMap = new HashMap<>();
        List<Integer> bestItem = new ArrayList<>();
        //Note: Lowest first method
        if (matchLowDepth){
            List<Sequence> picked = new ArrayList<>();
            int maxLenMatch = 0;
            List<Sequence> maxLenPicked = new ArrayList<>();
            for (int i = 0; i < head.size(); i++) {
                //Note: first time when i=0, we know we have nothing picked.
                if (i==0) {
                    for (Integer lv : head.get(head.size() - i - 1).levelIds) {
                        for (Sequence s : frequentSequencesMap.keySet()) {
                            if (s.size() > i + 1) {
                                if (s.getItemSets().get(s.size() - i - 2).getItems().get(0).getItemId() == lv.intValue()) {
                                    picked.add(s);
                                }
                            }
                        }
                        if (!picked.isEmpty()){
                            maxLenMatch = i+1;
                            maxLenPicked = picked;
                            break;
                        }
                    }
                    if (picked.isEmpty()){
                        break;
                    }
                }
                //Note: first rePick shorter ones, then search for lowest in longer ones
                else{
                    List<Sequence> rePick = new ArrayList<>();
                    for (Integer lv : head.get(head.size() - i - 1).levelIds) {
                        int ind = i;
                        for (Sequence s : picked.stream().filter(p->p.size()>ind+1).collect(Collectors.toList())) {
                            if (s.getItemSets().get(s.size() - i - 2).getItems().get(0).getItemId() == lv.intValue()) {
                                rePick.add(s);
                            }
                        }
                        if (!rePick.isEmpty()){
                            maxLenMatch = i+1;
                            maxLenPicked = rePick;
                            break;
                        }
                    }
                    if (rePick.isEmpty()){
                        break;
                    }
                }
            }
            /*if (maxLenMatch+1<maxLenPicked.stream().mapToInt(s->s.size()).max().getAsInt()){
                System.out.println("");
            }*/
            int kkkkk = maxLenMatch+1;
            maxLenPicked = maxLenPicked.stream().filter(s -> s.size() <= kkkkk).collect(Collectors.toList());
//            System.out.println("picked: "+picked);
            if (!HierarchicalRunner.keepShorterSequences){
                if (!maxLenPicked.isEmpty()) {
                /*    int max = maxLenMatch+1;
                    int size = picked.size();

                    picked = picked.stream().filter(sequence -> sequence.size()>=max).collect(Collectors.toList());

                    if (picked.size() < size) {
                        System.out.println("longer picked:" + picked);
                    }*/
                    picked = maxLenPicked;
                }
            }
            //Note: tries to pick sequences with lower targets
            List<Sequence> lowest = new ArrayList<>();
            if (HierarchicalRunner.matchLowTargetFirst){
                int size = picked.size();
                for (int i = 0; i < HierarchicalRunner.cutOffs.size(); i++) {
                    for (Sequence s:picked){
                        if(clusterMap.get(s.getItemSets().get(s.size()-1).getItems().get(0).getItemId()).getRadius()
                                < HierarchicalRunner.cutOffs.get(i)*2){
                            lowest.add(s);
                        }
                    }
                    if (!lowest.isEmpty()){
                        break;
                    }
                }
                if (lowest.isEmpty()){
                    lowest = picked;
                }
                picked = lowest;
                if (picked.size()<size){
//                    System.out.println("picked low target:"+picked);
                }
            }

            for (Sequence seq:picked){
                //
                Tuple2C<Integer, Double> current = new Tuple2C<>(seq.getItemSets().get(seq.getItemSets().size()-1).getItems().get(0).getItemId(),frequentSequencesMap.get(seq)/(double)maxSupport);
                //
                /*if(HierarchicalRunner.longestmatch.containsKey(seq.size())){
                    HierarchicalRunner.longestmatch.put(seq.size(),HierarchicalRunner.longestmatch.get(seq.size())+1);
                }else{
                    HierarchicalRunner.longestmatch.put(seq.size(),1);
                }*/
//                List<Integer> seqInts = seq.getItemSets().stream().map(itemSet -> itemSet.getItems().get(0).getItemId()).collect(Collectors.toList());
//                Tuple2C<Integer, Double> current = similarityScore(head.size(), seqInts, frequentSequencesMap.get(seq), maxSupport, clusterMap, maxClusterRadius);
                if (itemScoreMap.containsKey(current.getT1())){
                    itemScoreMap.put(current.getT1(), roundDown4(itemScoreMap.get(current.getT1()) + current.getT2()));
                }else{
                    itemScoreMap.put(current.getT1(), roundDown4(current.getT2()));
                }
            }
        }else{
            //Note: Score Based Method
            for (Map.Entry<Sequence,Integer> seq:frequentSequencesMap.entrySet()) {
                List<Integer> seqInts = seq.getKey().getItemSets().stream().map(itemSet -> itemSet.getItems().get(0).getItemId()).collect(Collectors.toList());
                if (spListContainsSeq(head, seqInts)){
                    Tuple2C<Integer, Double> current = similarityScore(head.size(), seqInts, seq.getValue(), maxSupport, clusterMap, maxClusterRadius);
                    if (itemScoreMap.containsKey(current.getT1())){
                        itemScoreMap.put(current.getT1(), roundDown4(itemScoreMap.get(current.getT1()) + current.getT2()));
                    }else{
                        itemScoreMap.put(current.getT1(), roundDown4(current.getT2()));
                    }
                }
            }
        }
//        System.out.println("result candidates:"+ MapUtil.sortByKey(itemScoreMap));
        Map<Integer,Integer> parentCountMap = new HashMap<>();
        if (!HierarchicalRunner.matchLowTargetFirst){
            int size = itemScoreMap.size();
            Map<Integer, Double> newItemScoreMap = itemScoreMap;
            List<Integer> items = itemScoreMap.keySet().stream().map(Integer::intValue).collect(Collectors.toList());
            int count=0;
            for (int i = 0; i < items.size(); i++) {
                for (int j = i+1; j < items.size(); j++) {
                    if (tree.crashOnRoute(items.get(i),items.get(j))){
                        newItemScoreMap.remove(items.get(j));
                        if (parentCountMap.containsKey(items.get(i))) {parentCountMap.put(items.get(i),parentCountMap.get(items.get(i))+1);}
                        else{{parentCountMap.put(items.get(i),1);}}
                        count++;
                    }else if (tree.crashOnRoute(items.get(j),items.get(i))){
                        newItemScoreMap.remove(items.get(i));
                        if (parentCountMap.containsKey(items.get(j))) {parentCountMap.put(items.get(j),parentCountMap.get(items.get(j))+1);}
                        else{{parentCountMap.put(items.get(j),1);}}
                        count++;
                    }
                }
            }
            itemScoreMap = newItemScoreMap;
            if (itemScoreMap.size()<size){
//                System.out.println("result candidates parents removed:"+itemScoreMap);
            }
        }

        //Note: if more than one item had same scores, randomly chosen.
        double bestScore = 0.0;
        for (Map.Entry<Integer, Double> entry:itemScoreMap.entrySet()){

            if (entry.getValue() > bestScore){
                bestScore = entry.getValue();
                if (bestItem.isEmpty()){
                    bestItem.add(entry.getKey());
                }else {
                    bestItem.clear();
                    bestItem.add(entry.getKey());
                }
            }else if (entry.getValue() == bestScore){
                if (bestScore!=0.0){
                    bestItem.add(entry.getKey());
//                    System.err.println("Shit happened again!");
                }
            }
        }
        if (!bestItem.isEmpty()){
            //TODO: remove parents
        }
//        if (HierarchicalRunner.randomSelection){
        if (HierarchicalRunner.randomSelection && bestItem.isEmpty()){
            useRand.set(true);
            if (HierarchicalRunner.firstRand) {
                return head.get(head.size() - 1).levelIds.get(0);
            }
            else if (HierarchicalRunner.nearestRand) {
                return FunctionStore.getNearestLocation(HierarchicalRunner.firstLevelClusterMap, head.get(head.size() - 1));
//                return FunctionStore.getNearestLocation(HierarchicalRunner.firstLevelClusterMap, clusterMap.get(head.get(head.size() - 1).levelIds.get(0)));
            }
            else {
                Map<Sequence, Integer> lowest = new HashMap<>();
                for (int i = 0; i < HierarchicalRunner.cutOffs.size(); i++) {
                    for (Sequence s : frequentSequencesMap.keySet()) {
                        if (clusterMap.get(s.getItemSets().get(s.size() - 1).getItems().get(0).getItemId()).getRadius()
                                < HierarchicalRunner.cutOffs.get(i)) {
                            lowest.put(s, frequentSequencesMap.get(s));
                        }
                    }
                    if (!lowest.isEmpty()) {
                        break;
                    }
                }
                if (lowest.isEmpty()) {
                    lowest = frequentSequencesMap;
                }
//            return lowest.keySet().stream().map(s->s.getItemSets().get(s.size()-1).getItems().get(0).getItemId()).collect(Collectors.toList()).get(new Random().nextInt(lowest.size()));
                Map<Integer, Long> countMap = lowest.keySet().stream()
                        .map(s -> s.getItemSets().get(s.size() - 1).getItems().get(0).getItemId())
                        .collect(Collectors.groupingBy(a -> a, Collectors.counting()));
                Map.Entry<Integer, Long> maxEntry = null;
                for (Map.Entry<Integer, Long> entry : countMap.entrySet()) {
                    if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                        maxEntry = entry;
                    }
                }
                return maxEntry == null ? 0 : maxEntry.getKey();
            }
        }
        // from candidate items, keeps the one having more parent removed.
        OptionalInt maxRmvParents = parentCountMap.values().stream().mapToInt(a->a).max();
        if (maxRmvParents.isPresent()){
            Map<Integer,Integer> parentMap = parentCountMap.entrySet().stream().filter(e->e.getValue()>=maxRmvParents.getAsInt()).collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));
            if (bestItem.size()>1){
                if (bestItem.stream().anyMatch(parentMap::containsKey)){
                    bestItem = bestItem.stream().filter(parentMap::containsKey).collect(Collectors.toList());
                }
            }
        }else{
//            System.err.println("Why???");
        }
        Integer result = bestItem.isEmpty()?0:bestItem.get(new Random().nextInt(bestItem.size()));
//        System.out.println("result(from "+bestItem.size()+" candidates):"+result);
        return result;
    }

    private boolean spListContainsSeq(List<StayPoint> head, List<Integer> seq) {
        // Note: part of sequence to be compared with test head is seq.subList(0, seq.size-1))
        if (seq.size()-1 > head.size()){
            return false;
        }
        int n = head.size()-1;
        int m = seq.size()-2;
        while (n>-1 && m>-1){
            if (head.get(n).levelIds.contains(seq.get(m))){
                n--; m--;
            }
            //Note: in the case where two consecutive stay points are the same in higher levels
            else if(m+1<seq.size()-1 && head.get(n).levelIds.contains(seq.get(m + 1))){
                n--;
            }
            else return false;
        }
        return true;
    }

    private Tuple2C<Integer, Double> similarityScore(int testSize, List<Integer> seqInts, Integer seqSupport, Integer maxSupport, Map<Integer, Cluster> clustered, double maxClusterRadius) {
        double depthScore = 0.0;
        double alpha = 0.5;
        for (int i = 0; i < seqInts.size()-1; i++) {
            double radius = clustered.get(seqInts.get(i)).getRadius();
            depthScore += (1-(radius/maxClusterRadius))*Math.pow(alpha,seqInts.size()-2-i);
        }
        double lengthScore = (seqInts.size()-1)/(double)testSize;
        double targetScore = 1 - (clustered.get(seqInts.get(seqInts.size()-1)).getRadius()/maxClusterRadius);
        double supportScore = seqSupport/(double)maxSupport;

        return new Tuple2C<>(seqInts.get(seqInts.size()-1),
                Function.match_depth_multiplier*depthScore + Function.length_multiplier*lengthScore +
                        Function.target_depth_multiplier*targetScore + Function.support_multiplier*supportScore);
    }

    private int commonSuffix(List<Integer> subList, List<Integer> test) {
        int n = subList.size()-1;
        int m = test.size()-1;
        int suffix = 0;
        while (n>-1 && m>-1){
            if (subList.get(n).intValue() == test.get(m).intValue()){
                suffix++; n--; m--;
            }
            else return suffix;
        }
        return suffix;
    }

    public Map<Integer,Integer> countSingleItems(List<Sequence> sequences){
        System.out.println("Total Number Of Sequences:::::"+sequences.size());
        Map<Integer,Integer> itemCountMap = new TreeMap<>();

        for (Sequence s:sequences){
            Set<Integer> seqItemIds = s.getAllItemIds();
            for (Integer id:seqItemIds){
                if (itemCountMap.containsKey(id)){
                    itemCountMap.put(id, itemCountMap.get(id) + 1);
                }else{
                    itemCountMap.put(id, 1);
                }
            }
        }
        System.out.println("Total Number Of Items:::::" + itemCountMap.size());
        return itemCountMap;
    }

    public Map<Integer, Integer> filterLowSupItems(Map<Integer,Integer> itemCountMap,Integer minSup){
        return itemCountMap.entrySet()
                .stream()
                .filter(p -> p.getValue() >= minSup)
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
    }
    /**
     * Converts List of List<StayPoints> to List of Sequences.
     * For each StayPoint, levelIds will become items and each StayPoint will be an ItemSet,
     * So each List of daily StayPoints will be List of ItemSets or a Sequence.
     * @param dailySequences
     * @return
     */
    public List<Sequence> transformStayPoints(List<DailySequence> dailySequences){
        List<Sequence> sequenceList = new ArrayList<>();
        for (DailySequence stayPointList:dailySequences){
            Sequence sequence = new Sequence();
            for (StayPoint sp: stayPointList.list){
                ItemSet is = new ItemSet();
                for (Integer id:sp.levelIds){
                    Item item = new Item(id);
                    is.addItem(item);
                }
                sequence.addItemSet(is);
            }
            sequenceList.add(sequence);
        }
        return sequenceList;
    }
    /**
     * Gets daily sequecnes of users and uses Apriori-based candidate generation to extract frequent sequences
     * @param dailySequences
     * @param hierarchyTree
     * @param minSup
     * @return a map of type "<Sequence,Integer>" representing each frequent sequence with it's frequency.
     */
    public Map<Sequence, Integer> generateFrequentSequences(List<Sequence> dailySequences, HierarchicalTree hierarchyTree, double minSup) throws NodeNotFoundException {
        Map<Sequence, Integer> sequenceCountMap = new HashMap<>();
        Set<Sequence> candidateSeed;
        Set<Sequence> candidates = generateC1(dailySequences);
//        System.out.println("candidates:" + candidates.size());
        //Note: large item-sets are created and added to sequence map.
        SortedSet<Sequence> lastRoundsLn = generateLn(dailySequences.size() * minSup, dailySequences, candidates, sequenceCountMap);
        //Note: parents are removed to reduce next round candidate generation seed size.
        SortedSet<Sequence> nextRoundSeed = removeParentSeqs(lastRoundsLn, hierarchyTree);
        // Note: removing length-one frequent sequences
        sequenceCountMap.clear();
//        System.out.println("litemsets:" + lastRoundsLn.size());
//        System.out.println("next round seed:" + nextRoundSeed.size());
        OptionalInt maxLength = dailySequences.stream().mapToInt(Sequence::size).max();
        int maxLengthFreqSeq = maxLength.isPresent()?maxLength.getAsInt():0;
        for (int i = 2; i <= maxLengthFreqSeq; i++) {
//            System.out.println("started"+System.currentTimeMillis());
            candidateSeed = generateCandidates(i, nextRoundSeed, hierarchyTree);
//            System.out.println(i + ":candidateSeed:" + candidateSeed.size());
            candidates.clear();
            lastRoundsLn.clear();
            expandCandidates(i, candidateSeed, hierarchyTree, lastRoundsLn, dailySequences.size() * minSup, dailySequences, sequenceCountMap);
//            System.out.println(i + ":lastRoundsLn:" + lastRoundsLn.size());
            nextRoundSeed = removeParentSeqs(lastRoundsLn, hierarchyTree);
//            System.out.println(i+":nextRoundSeed:"+nextRoundSeed.size());
            if (lastRoundsLn.size() == 0) {
                break;
            }
//            System.out.println("ended"+System.currentTimeMillis());
        }
        return sequenceCountMap;
    }

    public Map<Sequence, Integer> generateFrequentSeqs(List<Sequence> dailySequences, HierarchicalTree hierarchyTree, double minSup) {
        Map<Sequence, Integer> sequenceCountMap = new HashMap<>();
        Set<Sequence> candidates = generateC1(dailySequences);
        System.out.println("candidates:"+candidates.size());
        SortedSet<Sequence> lastRoundFreqItemSet = generateLn(dailySequences.size() * minSup, dailySequences, candidates, sequenceCountMap);
        // Note: removing length-one frequent sequences
        sequenceCountMap.clear();

        System.out.println("litemsets:" + lastRoundFreqItemSet.size());
        OptionalInt maxLength = dailySequences.stream().mapToInt(Sequence::size).max();
        int maxLengthFreqSeq = maxLength.isPresent()?maxLength.getAsInt():0;
        for (int i = 2; i <= maxLengthFreqSeq; i++) {
            //todo
            System.out.println(System.currentTimeMillis());
            candidates = generateCn(i, lastRoundFreqItemSet, hierarchyTree);
            System.out.println(i + ":candidates:" + candidates.size());
            lastRoundFreqItemSet = generateLn(dailySequences.size() * minSup, dailySequences, candidates, sequenceCountMap);
            System.out.println(i+":litemsets:"+lastRoundFreqItemSet.size());
            if (lastRoundFreqItemSet.size() == 0) {
                break;
            }
        }
        return sequenceCountMap;
    }

    private void expandCandidates(int level, Set<Sequence> seed, HierarchicalTree tree, SortedSet<Sequence> lastRoundsLn
            , double minSupCount, List<Sequence> dailySequences, Map<Sequence, Integer> sequenceCountMap) {
        Set<Sequence> ret = new HashSet<>();
        Set<String> expanded = new HashSet<>();
        for (Sequence seq:seed){
            List<List<Integer>> parentLists = seq.getItemSets().stream().map(
                    is -> tree.getAllParentIds(tree.getNodeById(is.getItems().get(0).getItemId()))).collect(Collectors.toList());
            expanded.addAll(recursiveParentSequenceMaker(parentLists));
        }
        System.out.println(level + ":candidates:" + expanded.size());
        List<String> exL = new ArrayList<>(expanded);
        expanded.clear();

        int partitionSize = exL.size()>10?exL.size()/10:exL.size();
        while (!exL.isEmpty()){
            List<String> tempList = exL.subList(0,Math.min(partitionSize,exL.size()));
            exL = exL.subList(Math.min(partitionSize,exL.size()),exL.size());
            tempList.forEach(ex -> ret.add(convertStringToSequence(ex)));
            lastRoundsLn.addAll(generateLn(minSupCount, dailySequences, ret, sequenceCountMap));
            ret.clear();
        }
    }

    private Sequence convertStringToSequence(String ex) {
        return new Sequence(new ArrayList<>((Arrays.asList(ex.split(",")).stream().map(s->new ItemSet(Collections.singletonList(new Item(Integer.parseInt(s))))).collect(Collectors.toList()))));
    }



    /*private Set<Sequence> recursiveParentSequenceMaker(List<List<Integer>> parentLists) {
        Set<Sequence> newSet = new HashSet<>();
        //Note: if parentLists.isEmpty returns newSet
        if (parentLists.size()==1){
            newSet.addAll(parentLists.get(0).stream().map(parentId -> new Sequence(Collections.singletonList(new ItemSet(Collections.singletonList(new Item(parentId)))))).collect(Collectors.toList()));
        }else {
            Set<Sequence> prevSet = recursiveParentSequenceMaker(parentLists.subList(0, parentLists.size() - 1));
            for (Integer parentId : parentLists.get(parentLists.size() - 1)) {
                Sequence addi;
                for (Sequence prev : prevSet) {
                    (addi = prev.copy()).addItemSet(new ItemSet(Collections.singletonList(new Item(parentId))));
                    newSet.add(addi);
                }
            }
        }
        return newSet;
    }*/

    private Set<String> recursiveParentSequenceMaker(List<List<Integer>> parentLists) {
        Set<String> newSet = new HashSet<>();
        //Note: if parentLists.isEmpty returns newSet
        if (parentLists.size()==1){
            newSet.addAll(parentLists.get(0).stream().map(Object::toString).collect(Collectors.toSet()));
        }else {
            Set<String> prevSet = recursiveParentSequenceMaker(parentLists.subList(0, parentLists.size() - 1));
            for (Integer parentId : parentLists.get(parentLists.size() - 1)) {
                for (String prev : prevSet) {
                    newSet.add(prev.concat(",").concat(parentId.toString()));
                }
            }
        }
        return newSet;
    }

    /**
     * by iteration on last rounds' seed twice, generates next round candidates. equality of sequence heads is modified
     * for hierarchical sequence items by adding case where two items on same position can be on the same route on tree,
     * from leaf to root.
     * @param n
     * @param lastFreqSeqs
     * @param tree
     * @return
     * @throws NodeNotFoundException
     */
    private Set<Sequence> generateCandidates(int n, SortedSet<Sequence> lastFreqSeqs, HierarchicalTree tree) throws NodeNotFoundException {
        Set<Sequence> ret = new HashSet<>();
        List<Sequence> lastFreqList = new ArrayList<>(lastFreqSeqs);
        for (int i = 0; i < lastFreqList.size(); i++) {
            label:
            for (int j = i; j < lastFreqList.size(); j++) {
                Iterator<ItemSet> seqIter = lastFreqList.get(i).getItemSets().iterator();
                Iterator<ItemSet> seqIter2 = lastFreqList.get(j).getItemSets().iterator();
                Sequence newSeq1 = new Sequence();
                Sequence newSeq2;
                ItemSet first;
                ItemSet second;
                for (int k = 0; k < n - 2; k++) {
                    if ((first = seqIter.next()).equals((second = seqIter2.next()))) {
                        newSeq1.addItemSet(first);
                    } else if (first.getItems().get(0).getItemId() > second.getItems().get(0).getItemId()
                            && tree.crashOnRoute(second.getItems().get(0).getItemId(), first.getItems().get(0).getItemId())) {
                        newSeq1.addItemSet(first);
                    } else if (first.getItems().get(0).getItemId() < second.getItems().get(0).getItemId()
                            && tree.crashOnRoute(first.getItems().get(0).getItemId(), second.getItems().get(0).getItemId())) {
                        newSeq1.addItemSet(second);
                    } else {
                        continue label;
                    }
                }
                newSeq2 = newSeq1.copy();
                ItemSet firstLast = seqIter.next();
                ItemSet secondLast = seqIter2.next();
                newSeq1.addItemSet(firstLast);
                newSeq1.addItemSet(secondLast);
                newSeq2.addItemSet(secondLast);
                newSeq2.addItemSet(firstLast);
                ret.add(newSeq1);
                ret.add(newSeq2);
            }
        }
        return ret;
    }

    /**
     * In some level, prunes Ln(large ItemSets) to remove parent sequences.
     * @param ln
     * @param tree
     * @return
     */
    private SortedSet<Sequence> removeParentSeqs(SortedSet<Sequence> ln, HierarchicalTree tree) {
        SortedSet<Sequence> lowLevelsSequences = new TreeSet<>();
        Set<Integer> highLevelIndices = new HashSet<>();
        List<Sequence> lnList = new ArrayList<>(ln);
        for (int i = 0; i < lnList.size(); i++) {
            if (!highLevelIndices.contains(i)){
                Sequence seq = lnList.get(i);
                for (int j = i+1; j < lnList.size(); j++) {
                    if (lnList.get(j).isParentOf(seq,tree)){
                        highLevelIndices.add(j);
                    }
                }
            }
        }
        for (int i = 0; i < lnList.size(); i++) {
            if (!highLevelIndices.contains(i)){
                lowLevelsSequences.add(lnList.get(i));
            }
        }
        return lowLevelsSequences;
    }

    private static SortedSet<Sequence> generateLn(double minSupCount, List<Sequence> dailySequences, Set<Sequence> candidates, Map<Sequence, Integer> sequenceCountMap) {
        SortedSet<Sequence> ret = new TreeSet<>();
        for (Sequence seq : candidates) {
            int sup = getSignificance(dailySequences, seq);
            if (sup > minSupCount) {
                ret.add(seq);
                sequenceCountMap.put(seq,sup);
            }
        }
        return ret;
    }

    private static int getSignificance(List<Sequence> data, Sequence current) {
        int count = 0;
        for (Sequence sequence : data) {
            if (sequence.containsSequence(current, HierarchicalRunner.MAKING_MAX_GAP, false)) {
                count++;
            }
        }
        return count;
    }

    private static Set<Sequence> generateCn(int n, SortedSet<Sequence> lastFreqSeqs, HierarchicalTree tree) {
        Set<Sequence> ret = new HashSet<>();
        if (HierarchicalRunner.pruneSameConsecutive) {
            if (HierarchicalRunner.pruneParentChild){
                for (Sequence seq : lastFreqSeqs) {
                    label:
                    for (Sequence seq2 : lastFreqSeqs) {
                        if (seq2 == seq) {
                            continue label;
                        }
                        Iterator<ItemSet> seqIter = seq.getItemSets().iterator();
                        Iterator<ItemSet> seqIter2 = seq2.getItemSets().iterator();
                        for (int j = 0; j < n - 2; j++) {
                            if (!seqIter.next().equals(seqIter2.next())) {
                                continue label;
                            }
                        }
                        Sequence newSeq = new Sequence();
                        newSeq.addAll(seq);
                        // filtering happening of the case <a,b> | b parent of a
                        ItemSet next = seqIter2.next();
                        Integer nextId = next.getItems().get(0).getItemId();
                        Integer lastId = newSeq.getItemSets().get(newSeq.size()- 1).getItems().get(0).getItemId();
                        // checking conditions where new candidate must not be created
                        // Note: filter inconsistent sequences -> 1) a,b,c,... | b parentOf a  2) b=a
                        try {
                            if (tree.crashOnRoute(lastId,nextId) || tree.crashOnRoute(nextId,lastId)){
                                continue;
                            }
                        }catch (NodeNotFoundException ignored){}
                        newSeq.addItemSet(next);
                        ret.add(newSeq);
                    }
                }
            }else {
                for (Sequence seq : lastFreqSeqs) {
                    label:
                    for (Sequence seq2 : lastFreqSeqs) {
                        if (seq2 == seq) {
                            continue label;
                        }
                        Iterator<ItemSet> seqIter = seq.getItemSets().iterator();
                        Iterator<ItemSet> seqIter2 = seq2.getItemSets().iterator();
                        for (int j = 0; j < n - 2; j++) {
                            if (!seqIter.next().equals(seqIter2.next())) {
                                continue label;
                            }
                        }
                        Sequence newSeq = new Sequence();
                        newSeq.addAll(seq);
                        ItemSet next = seqIter2.next();
                        newSeq.addItemSet(next);
                        ret.add(newSeq);
                    }
                }
            }
        }else{
            if (HierarchicalRunner.pruneParentChild) {
                for (Sequence seq : lastFreqSeqs) {
                    label:
                    for (Sequence seq2 : lastFreqSeqs) {
                        Iterator<ItemSet> seqIter = seq.getItemSets().iterator();
                        Iterator<ItemSet> seqIter2 = seq2.getItemSets().iterator();
                        for (int j = 0; j < n - 2; j++) {
                            if (!seqIter.next().equals(seqIter2.next())) {
                                continue label;
                            }
                        }
                        Sequence newSeq = new Sequence();
                        newSeq.addAll(seq);
                        // filtering happening of the case <a,b> | b parent of a
                        ItemSet next = seqIter2.next();
                        Integer nextId = next.getItems().get(0).getItemId();
                        Integer lastId = newSeq.getItemSets().get(newSeq.size() - 1).getItems().get(0).getItemId();
                        // checking conditions where new candidate must not be created
                        // Note: filter inconsistent sequences -> 1) a,b,c,... | b parentOf a  2) b=a
                        try {
                            if (tree.crashOnRoute(lastId, nextId) || tree.crashOnRoute(nextId, lastId)) {
                                continue;
                            }
                        } catch (NodeNotFoundException ignored) {
                        }
                        newSeq.addItemSet(next);
                        ret.add(newSeq);
                    }
                }
            }else{
                //todo:
                int i = 0;
                for (Sequence seq : lastFreqSeqs) {
                    //todo: remove
                    i++;
                    if (i%1000==0){
                        System.out.println("cur ind:"+i+"\ttime:"+System.currentTimeMillis());
                    }
                    //
                    label:
                    for (Sequence seq2 : lastFreqSeqs) {
                        Iterator<ItemSet> seqIter = seq.getItemSets().iterator();
                        Iterator<ItemSet> seqIter2 = seq2.getItemSets().iterator();
                        for (int j = 0; j < n - 2; j++) {
                            if (!seqIter.next().equals(seqIter2.next())) {
                                continue label;
                            }
                        }
                        Sequence newSeq = new Sequence();
                        newSeq.addAll(seq);
                        ItemSet next = seqIter2.next();
                        newSeq.addItemSet(next);
                        ret.add(newSeq);
                    }
                }
            }
        }
        return ret;
    }

    private static Set<Sequence> generateC1(List<Sequence> data) {
        Set<Sequence> ret = new HashSet<>();
        for (Sequence seq : data) {
            for (ItemSet itemset : seq.getItemSets()) {
                for (Item item : itemset.getItems()) {
                    Sequence s = new Sequence();
                    ItemSet is = new ItemSet();
                    is.addItem(item);
                    s.addItemSet(is);
                    ret.add(s);
                }
            }
        }
        return ret;
    }

    public double[] performApriori(List<DailySequence> trainData, List<DailySequence> testData, double minSup, double minConf, String username, Map<Integer, Cluster> clusterMap, String matchMethod) throws Exception {
        List<ApriorAllModelData> data = new ArrayList<>();
        List<UserSequence> trainSequences = transformData(trainData, username);
        for (UserSequence ds : trainSequences) {
            data.add(new ApriorAllModelData("", ds));
        }
        List<ApriorAllRule> rules = AprioriAll.findAllFreqAssoRule(data, new ApriorPrecision(minConf, minSup));
        if (HierarchicalRunner.harshPrediction){
            return aprioriPredictionHarsh(rules,testData,username,clusterMap,matchMethod);
        }else {
            return aprioriPrediction(rules, testData, username, clusterMap, matchMethod);
        }
    }

    private static double[] aprioriPredictionHarsh(List<ApriorAllRule> rules, List<DailySequence> testData, String username, Map<Integer, Cluster> clusterMap, String matchMethod) throws Exception {
        List<String> exploitedSamples = new ArrayList<>();
        List<String> matchWrite = new ArrayList<>();
        List<String> randWrite = new ArrayList<>();
        List<Double> distanceError = new ArrayList<>();
        List<String> other = new ArrayList<>();
        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
//            other = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath1);
        }
        double[] stats = new double[8];
        int allTests = 0;int matched = 0;int true_label = 0;int errorCount = 0;int randCount = 0;stats[7]=rules.size();
        for (DailySequence testSample: testData){

            for (int i = 1; i < testSample.list.size(); i++) {
                int idx = i;
                if (HierarchicalRunner.readTestIndices){
                    if (!exploitedSamples.contains(testSample.id+" "+idx)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
                //Note: Clean Head
                head = head.get(head.size()-1).clusterID>0? head.stream().filter(a->a.clusterID>0).collect(Collectors.toList()):head;
                Integer lastId = head.get(head.size()-1).levelIds.get(0);
                StayPoint next = null;
                for (int j = i; j < testSample.list.size(); j++) {
                    next = testSample.list.get(j);
                    if (!next.levelIds.get(0).equals(lastId)) {
                        break;
                    }
                }
//                System.out.println("id:"+testSample.id+ "-"+i+"\ntest sequence: " + testSample);
//                System.out.println("head sequence: " + head + "\treal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                Integer pValue = aprioriWhatNext(head, rules, clusterMap, useRand, matchMethod);
//                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    //
                    if (HierarchicalRunner.writeTestIndices) {
                        if (useRand.get()){
                            randWrite.add(testSample.id + " " + idx);
                        }else{
                            matchWrite.add(testSample.id+" "+idx);
                        }
                    }
                    //
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                        throw new Exception();
                    }
                    else {
                        distanceError.add(predicted.distanceTo(next));
                        errorCount++;
//                        System.out.println("\terror:" + predicted.distanceTo(next)+"\n");
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                }
            }
        }
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum(); stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                matchWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesRandPath, true)));
                writer.append("u:").append(username).append("\n");
                randWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stats;
    }
    public static double[] aprioriPrediction(List<ApriorAllRule> rules, List<DailySequence> testData, String username, Map<Integer, Cluster> clusterMap, String matchMethod) throws Exception {
        List<String> exploitedSamples = new ArrayList<>();
        List<String> matchWrite = new ArrayList<>();
        List<String> randWrite = new ArrayList<>();
        List<Double> distanceError = new ArrayList<>();
        List<String> other = new ArrayList<>();
        if (HierarchicalRunner.readTestIndices){
            exploitedSamples = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath);
//            other = readTestIndicesFromFile(username, HierarchicalRunner.testIndicesReadPath1);
        }
        double[] stats = new double[8];
        stats[7]=rules.size();
        int allTests = 0;int matched = 0;int true_label = 0;int errorCount = 0;int randCount = 0;
        for (DailySequence dailySequence: testData){
            DailySequence testSample = ClusterFunctions.filterSingleSequence(dailySequence,true);

            for (int i = 1; i < testSample.list.size(); i++) {
                int idx = dailySequence.list.indexOf(testSample.list.get(i));
                if (HierarchicalRunner.readTestIndices){
                    if (!exploitedSamples.contains(testSample.id+" "+idx)){
                        continue;
                    }
                }
                List<StayPoint> head = testSample.list.subList(0, i);
                Integer lastId = head.get(head.size()-1).levelIds.get(0);
                StayPoint next = null;
                for (int j = i; j < testSample.list.size(); j++) {
                    next = testSample.list.get(j);
                    if (!next.levelIds.get(0).equals(lastId)) {
                        break;
                    }
                }
//                System.out.println("id:"+testSample.id+ "-"+i+"\ntest sequence: " + testSample);
//                System.out.println("head sequence: " + head + "\treal next: " + next.levelIds);
                AtomicBoolean useRand = new AtomicBoolean(false);
                Integer pValue = aprioriWhatNext(head, rules, clusterMap, useRand, matchMethod);
//                System.out.println("\tprediction: "+pValue);
                allTests++;
                if (pValue!=0){
                    //
                    if (HierarchicalRunner.writeTestIndices) {
                        if (useRand.get()){
                            randWrite.add(testSample.id + " " + idx);
                        }else{
                            matchWrite.add(testSample.id+" "+idx);
                        }
                    }
                    //
                    matched++;
                    if (next.levelIds.contains(pValue)){
                        true_label++;
                    }
                    Cluster predicted = clusterMap.get(pValue);
                    if (predicted==null) {
                        System.err.println("\tCluster Not Found");
                        throw new Exception();
                    }
                    else {
                        distanceError.add(predicted.distanceTo(next));
                        errorCount++;
//                        System.out.println("\terror:" + predicted.distanceTo(next)+"\n");
                    }
                    if (useRand.get()){
                        randCount++;
                    }
                }
            }
        }
        stats[0]=testData.size(); stats[1]=allTests; stats[2]=matched-randCount; stats[3]=true_label; stats[4]=distanceError.stream().mapToDouble(Double::doubleValue).sum(); stats[5]=errorCount; stats[6]=randCount;
        if (HierarchicalRunner.writeTestIndices) {
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
                writer.append("u:").append(username).append("\n");
                matchWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesRandPath, true)));
                writer.append("u:").append(username).append("\n");
                randWrite.forEach(a -> writer.append(a).append("\n"));
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return stats;
    }

    public static Integer aprioriWhatNext(List<StayPoint> head, List<ApriorAllRule> rules, Map<Integer, Cluster> clusterMap, AtomicBoolean useRand, String matchMethod) {
        List<String> heads = new ArrayList<>();
        head.stream().forEach(sp -> heads.add(sp.levelIds.get(0) + ""));
        Map<String, Double> scores = new HashMap<>();

        FunctionStore.findCandidates(heads, rules, scores, matchMethod);

        double max = 0.0;
        String best = "";
        for (String s : scores.keySet()) {
            if (scores.get(s) > max) {
                best = s;
                max = scores.get(s);
            }
        }
//        if (HierarchicalRunner.randomSelection){
        if (scores.isEmpty() && HierarchicalRunner.randomSelection){
            useRand.set(true);
            return FunctionStore.getNearestLocation(clusterMap,head.get(head.size()-1));
        }
        return best.length()>0 ? Integer.parseInt(best):0;
    }

    private List<UserSequence> transformData(List<DailySequence> trainData, String username) {
        List<UserSequence> ret = new ArrayList<>();
        for (DailySequence ds:trainData) {
            UserSequence us = new UserSequence(username,0);
            prediction.aprioriall.ItemSet itemSet;
            UserLocation userLocation;
            for (int i = 1; i < ds.list.size(); i++) {
                StayPoint spz = ds.list.get(i);
                userLocation = new UserLocation(spz, username);
                itemSet = new prediction.aprioriall.ItemSet();
                itemSet.add(userLocation);
                us.add(itemSet);
            }
            ret.add(us);
        }
        return ret;
    }

    public static double roundDown4(double d) {
        return (long) (d * 1e4) / 1e4;
    }
}
