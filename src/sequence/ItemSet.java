package sequence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Reza on 10/31/2015.
 */
public class ItemSet implements Comparable<ItemSet>{
    private List<Item> items;

    public void addItem(Item item){
        items.add(item);
    }

    public List<Item> getItems(){
        return items;
    }

    public ItemSet(){
        items = new ArrayList<>();
    }

    public ItemSet(List<Item> items){
        this.items = items;
    }

    public boolean hasItem(Item item){
        for (Item i:items) {
            if (i.getItemId().equals(item.getItemId())) {
                return true;
            }
        }
        return false;
    }

    public Integer size(){
        return items.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Item i:items) {
            sb.append(i).append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public int compareTo(ItemSet o) {
        Iterator<Item> thisIter = items.iterator();
        Iterator<Item> anotherIter = o.items.iterator();

        while (thisIter.hasNext()) {
            if (!anotherIter.hasNext()) {
                return 1;
            }

            Item i1 = thisIter.next();
            Item i2 = anotherIter.next();

            int diff = i1.compareTo(i2);
            if (diff != 0) {
                return diff;
            }
        }

        if (anotherIter.hasNext()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemSet itemSet = (ItemSet) o;

        return items.equals(itemSet.items);

    }

    @Override
    public int hashCode() {
        return items.hashCode();
    }
}
