package sequence;

import datapreparation.StayPoint;
import hierarchical.*;
import utility.FunctionStore;
import utility.MapUtil;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created by Reza on 11/2/2015.
 */
public class Tester {
    public static void main(String[] args) throws NodeNotFoundException, IOException {
        List<String> validUsers = FunctionStore.getValidUsers("result\\stayPointCount.csv", 60);
//        Map<String, Set<String>> indices = new HashMap<>();
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(HierarchicalRunner.testIndicesWritePath, true)));
        for (String username:validUsers){
            Set<String> all = new HashSet<>();
            Set<String> intersection = new HashSet<>();
            List<String> grand = Function.readTestIndicesFromFile(username,System.getProperty("user.home")+"\\Desktop\\indices-rand-1.txt");
            List<String> gwrite = Function.readTestIndicesFromFile(username,System.getProperty("user.home")+"\\Desktop\\indices-write-1s.txt");
//            List<String> hrand = Function.readTestIndicesFromFile(username,System.getProperty("user.home")+"\\Desktop\\indices-rand-h.txt");
//            List<String> hwrite = Function.readTestIndicesFromFile(username,System.getProperty("user.home")+"\\Desktop\\indices-write-h.txt");
            all.addAll(grand);all.addAll(gwrite);//all.addAll(hrand);all.addAll(hwrite);
            for (String key:all){
                if ((grand.contains(key) || gwrite.contains(key))){
//                if ((grand.contains(key) || gwrite.contains(key)) && (hrand.contains(key) || hwrite.contains(key))){
                    intersection.add(key);
                }
            }
            writer.append("u:").append(username).append("\n");
            intersection.forEach(a -> writer.append(a).append("\n"));
            writer.flush();
        }
//        start();
    }
    public static void start() {
//        List<Integer> i1 = Arrays.asList(5,3,1,7,2,8,6,4);
        List<Integer> i1 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i2 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i3 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i4 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i5 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i6 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i7 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> i8 = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        Collections.shuffle(i2);
        Collections.shuffle(i3);
        Collections.shuffle(i4);
        Collections.shuffle(i5);
        Collections.shuffle(i6);
        Collections.shuffle(i7);
        Collections.shuffle(i8);
        System.out.println(mutation(i1,i5));
        List<List<Integer>> initPop = new ArrayList<>(Arrays.asList(i1, i2, i3, i4, i5, i6, i7, i8));
        int cnt = 0;
        List<Integer> costs = new ArrayList<>(8);
        while (next(initPop, costs) || cnt < 100) {
            initPop = newGeneration(initPop, costs);
            cnt++;
        }
    }

    private static List<List<Integer>> newGeneration(List<List<Integer>> initPop, List<Integer> initCosts) {
        List<List<Integer>> newGeneration = new ArrayList<>();
        List<Integer> initPopIdx = Arrays.asList(0,1,2,3,4,5,6,7);
        Collections.sort(initPopIdx, (a, b) -> initCosts.get(a).compareTo(initCosts.get(b)));
        newGeneration.add(initPop.get(initPopIdx.get(0)));
        newGeneration.add(initPop.get(initPopIdx.get(1)));
        Random r = new Random();
        List<Integer> used = new ArrayList<>();
        used.add(initPopIdx.get(0));used.add(initPopIdx.get(1));
        for (int i = 0; i < 3; i++) {
            int a = r.nextInt(6);
            while (used.contains(a)){
                a = r.nextInt(6);
            }
            used.add(a);
            int b = r.nextInt(6);
            while (used.contains(b)){
                b = r.nextInt(6);
            }
            used.add(b);
            newGeneration.addAll(mutation(initPop.get(a),initPop.get(b)));
        }
        return newGeneration;
    }

    private static List<List<Integer>> mutation(List<Integer> a, List<Integer> b) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> newA = new ArrayList<>(Arrays.asList(0,0,0,0,0,0,0,0));
        List<Integer> newB = new ArrayList<>(Arrays.asList(0,0,0,0,0,0,0,0));
        Random r = new Random();
        List<Integer> randList = new ArrayList<>();
        for (int i = 1; i < 9; i++) {
            int ra = r.nextInt(8)+1;
            while (randList.contains(ra)){
                ra = r.nextInt(8)+1;
            }
            randList.add(ra);
        }
        for (int i = 0; i < 4; i++) {
            newA.set(a.indexOf(randList.get(i)),randList.get(i));
            newB.set(b.indexOf(randList.get(i)), randList.get(i));
        }
        for (int i = 4; i < 8; i++) {
            newA.set(b.indexOf(randList.get(i)),randList.get(i));
            newB.set(a.indexOf(randList.get(i)),randList.get(i));
        }
        result.add(newA);
        result.add(newB);
        return result;
    }

    public static boolean next(List<List<Integer>> population, List<Integer> costs){
        int minCost = Integer.MAX_VALUE;
        for (int i = 0; i < population.size(); i++) {
            costs.set(i,cost(population.get(i)));
        }
        for (int i = 0; i < costs.size(); i++) {
            if (costs.get(i)<minCost){
                minCost = costs.get(i);
            }
        }
        if (minCost==0){
            System.out.println(population.get(costs.indexOf(0)));
            return false;
        }else{
            return true;
        }
    }

    private static Integer cost(List<Integer> integers) {
        int result = 0;
        for (int i = 0; i < integers.size(); i++) {
            for (int j = i+1; j < integers.size(); j++) {
                if ((j-i)==Math.abs(integers.get(j) - integers.get(i))){
                    result++;
                }
            }
        }
        return result;
    }
}