package utility;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by Reza on 11/1/2015.
 */
public class MapUtil {
    public static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue( Map<K, V> map )
    {
        Map<K,V> result = new LinkedHashMap<>();
        Stream<Map.Entry<K,V>> st = map.entrySet().stream();

        st.sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(e ->result.put(e.getKey(),e.getValue()));

        return result;
    }
    public static <K, V extends Comparable<? super V>> Map<K, V>
    sortByValue( Map<K, V> map , Comparator<V> comparator)
    {
        Map<K,V> result = new LinkedHashMap<>();
        Stream<Map.Entry<K,V>> st = map.entrySet().stream();

        st.sorted(Comparator.comparing(Map.Entry::getValue, comparator))
                .forEach(e ->result.put(e.getKey(),e.getValue()));

        return result;
    }

    public static <K extends Comparable<K>, V> Map<K, V>
    sortByKey( Map<K, V> map)
    {
        Map<K,V> result = new TreeMap<>();
        map.forEach(result::put);
        return result;
    }
}
