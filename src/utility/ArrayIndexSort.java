package utility;

import java.util.Comparator;

/**
 * Created by Harati on 8/3/2015.
 */
public class ArrayIndexSort implements Comparator<Integer> {
    private final Double[] array;
    private boolean ascending;

    public ArrayIndexSort(Double[] array, boolean ascending) {
        this.array = array;
        this.ascending = ascending;
    }

    public Integer[] createIndexArray() {
        Integer[] indexes = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            indexes[i] = i; // Autoboxing
        }
        return indexes;
    }

    @Override
    public int compare(Integer index1, Integer index2) {
        // Autounbox from Integer to int to use as array indexes
        if (ascending) {
            return array[index1].compareTo(array[index2]);
        } else {
            return array[index2].compareTo(array[index1]);
        }
    }
}

