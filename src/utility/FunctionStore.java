package utility;

import datapreparation.Location;
import datapreparation.LocationDetection;
import datapreparation.SPDate;
import datapreparation.StayPoint;
import hierarchical.Cluster;
import hierarchical.HLocation;
import hierarchical.HierarchicalRunner;
import prediction.aprioriall.*;
import prediction.location.ULocHolder;
import prediction.location.UserLocation;
import prediction.location.UserSequence;
import prediction.util.AprioriRunner;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class FunctionStore {
    public static double[] kNearestNeighbors(Map<Integer, Location> locationMap, int k) {
        double meanK[] = new double[locationMap.size()];
        List<Location> locations = new ArrayList<>();
        for (Integer i : locationMap.keySet()) {
            locations.add(locationMap.get(i));
        }
        for (int i = 0; i < locations.size(); i++) {
            double distances[] = new double[locations.size()];
            for (int j = 0; j < locations.size(); j++) {
                distances[j] = Location.centerDistance(locations.get(i), locations.get(j));
            }
            Arrays.sort(distances);
            meanK[i] = distances[k];
        }
        return meanK;
    }

    /**
     * Reads and returns sequences of stay points which have been written on file after detecting locations with corresponding cluster ids.
     *
     * @param map
     * @param countMap
     * @param url         address to root of files  @throws IOException
     * @param locationMap
     */
    public static void readSequences(Map<String, List<StayPoint>> map, Map<String, Integer> countMap, String url,
                                     Map<Integer, Location> locationMap, boolean hasClusterId) throws IOException{
        File root = new File(url);
        if (root.isDirectory()) {
            for (File child : root.listFiles((dir, name) -> name.matches("([0-9])+.*"))) {
                ArrayList<StayPoint> stayPoints = new ArrayList<>();
                BufferedReader reader = new BufferedReader(new FileReader(child));
                String line;
//                String username = child.getName().substring(child.getName().lastIndexOf(",") + 1, child.getName().lastIndexOf(".txt"));
                String username = child.getName().substring(0, child.getName().lastIndexOf(".txt"));
                while ((line = reader.readLine()) != null) {
                    stayPoints.add(new StayPoint(line, username, hasClusterId));
                }
                map.put(username, stayPoints);
                countMap.put(username, stayPoints.size());
            }
            if (hasClusterId) {
                File[] locationFiles = root.listFiles((dir, name) -> name.startsWith("loc"));
                File locations = null;
                if (locationFiles.length > 0) {
                    locations = locationFiles[0];
                }
                List<Location> locationList = new ArrayList<>();
                if (locations != null) {
                    BufferedReader reader = new BufferedReader(new FileReader(locations));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        Location l = new Location(line);
                        if (l.getId() > 0) {
                            locationList.add(l);
                        }
                    }
                    for (Location l : locationList) {
                        locationMap.put(l.getId(), l);
                    }
                }
            }
            Map<String, Double> noise_ratio = new HashMap<>();
            StringBuilder logger = new StringBuilder();
            map.keySet().forEach(w -> {
                logger.append(w).append(": stay point count:").append(map.get(w).size()).append("\tdistinct locations:").append(map.get(w).stream().map(q -> q.clusterID).distinct().collect(Collectors.toSet()).size());
                if (map.get(w).size() > 0) {
                    double ratio = map.get(w).stream().filter(x -> x.clusterID < 0).collect(Collectors.counting()) / (double) map.get(w).size();
                    noise_ratio.put(w, ratio);
                    logger.append("\tnoise ratio:").append(ratio).append("\n");
                } else {
                    logger.append("\n");
                }
            });
            logger.append("avrg:").append(noise_ratio.values().stream().collect(Collectors.averagingDouble(w -> w))).append("\tmin:").append(Collections.min(noise_ratio.values())).append("\tmax:").append(Collections.max(noise_ratio.values())).append("\n");
//            System.out.println(logger.toString());
        }
    }

    public static void createRules(List<StayPoint> mainSequence, String username, double train_ratio, double minConf,
                                   double minSup, PrintWriter file_PrWr, Map<Integer, Location> locationMap,
                                   Map<String, List<ApriorAllRule>> ruleBase, Map<String, List<UserSequence>> testBase,
                                   StringBuilder logger) {
//        StringBuilder temporary = new StringBuilder();temporary.append(username).append("\t");StringBuilder builder = new StringBuilder();builder.append(username + "\n");builder.append("loc_cnt:").append(distinct_loc.size()).append("\n");temporary.append(distinct_loc.size()).append("\t");
        mainSequence = mainSequence.stream().filter(a->a.clusterID>0).collect(Collectors.toList());
        List<UserSequence> dailySequences = makeDailySequences(mainSequence, username);
        if (dailySequences==null){
            return;
        }
        logger.append(username).append("\tinitial sequences count:").append(dailySequences.size()).append("\taverage length:")
                .append(dailySequences.stream().map(p -> p.size()).collect(Collectors.averagingInt(p -> p)));
        dailySequences = filterSequences(dailySequences, new HashMap<>());
        logger.append("\tfiltered count:").append(dailySequences.size()).append("\taverage length:").append(dailySequences.stream().map(p -> p.size()).collect(Collectors.averagingInt(p -> p))).append("\t");
        int train_limit = (int) Math.round(train_ratio * dailySequences.size());
        List<UserSequence> trainSequences = dailySequences.subList(0,train_limit);//temporary.append(dailySequences.size()).append("\t");
        List<UserSequence> testSequences = dailySequences.subList(train_limit, dailySequences.size());
        testBase.put(username, testSequences);
//        temporary.append(dailySequences.size()).append("\t");temporary.append(dailySequences.stream().map(p->p.size()).collect(Collectors.averagingInt(p -> p))).append("\t");dailySequences.stream().forEach(p -> temporary.append(p.toString()).append("\n"));
        List<ApriorAllModelData> data = convertData(trainSequences);
//        builder.append("train sequences").append("\n");dailySequences.subList(0,train_limit).forEach(q->builder.append(q.toString()).append("\n"));
        List<ApriorAllRule> rules = AprioriAll.findAllFreqAssoRule(data, new ApriorPrecision(minConf, minSup));
        logger.append("test count:").append(testSequences.size()).append("\trules count:").append(rules.size()).append("\n");
        ruleBase.put(username, rules);
//        temporary.append(rules.stream().map(p -> p.getConsequenceSeq().size() + p.getConditionSeq().size()).anyMatch(p -> p < 3)).append("\t");builder.append("# of rules:").append(rules.size()).append("\n");for (ApriorAllRule r : rules){builder.append(r.toString()).append("\n");}temporary.append(rules.size()).append("\t");getPredictionResult(rules, testSequences, builder, locationMap, temporary);file_PrWr.println(builder.toString());file_PrWr.flush();
    }

    // accurate
    public static List<UserSequence> makeDailySequences(List<StayPoint> mainSequence, String username) {
        if (mainSequence.isEmpty()){
            return null;
        }
        int sequenceCounter = 1;
        List<UserSequence> dailyUserSequences = new ArrayList<>();
        UserSequence sequence = new UserSequence(username, sequenceCounter++);
        UserLocation userLocation = new UserLocation(mainSequence.get(0), username);
        ItemSet itemSet = new ItemSet();
        itemSet.add(userLocation);
        SPDate last_time = new SPDate(mainSequence.get(0).timeString);
        for (int i = 1; i < mainSequence.size(); i++) {
            StayPoint spz = mainSequence.get(i);
            userLocation = new UserLocation(spz, username);
            SPDate current_time = new SPDate(spz.timeString);
            if (current_time.start.day == last_time.end.day) {
                itemSet = new ItemSet();
                itemSet.add(userLocation);
                sequence.add(itemSet);
                if (current_time.start.day != current_time.end.day) {
                    dailyUserSequences.add(sequence);
                    sequence = new UserSequence(username, sequenceCounter++);
                    sequence.add(itemSet);
                }
            } else {
                dailyUserSequences.add(sequence);
                sequence = new UserSequence(username, sequenceCounter++);
                itemSet = new ItemSet();
                itemSet.add(userLocation);
                sequence.add(itemSet);
                if (current_time.start.day != current_time.end.day) {
                    dailyUserSequences.add(sequence);
                    sequence = new UserSequence(username, sequenceCounter++);
                    sequence.add(itemSet);
                }
            }
            last_time = current_time;
        }
        return dailyUserSequences;
    }

    public static List<ApriorAllModelData> convertData(List<UserSequence> trainSequence) {
        List<ApriorAllModelData> data = new ArrayList<>();
        // making model from train data
        for (UserSequence us : trainSequence) {
            data.add(new ApriorAllModelData("", us));
        }
        return data;
    }

    public static List<ApriorAllModelData> convertHData(List<UserSequence> trainSequence) throws Exception {
        Map<String, List<UserSequence>> variations = new HashMap<>();
        for (UserSequence us : trainSequence) {
            List<List<Integer>> variationLists = new ArrayList<>();
            List<Integer> head = new ArrayList<>();
            generateSequenceVariations(variationLists, us, head);
            List<UserSequence> relabeled = reLabelUserSequence(variationLists, us);
            variations.put(us.id + "", relabeled);
        }
        List<ApriorAllModelData> data = new ArrayList<>();
        // making model from train data
        List<UserSequence> lus = new ArrayList<>();
        for (List<UserSequence> luss : variations.values()) {
            lus.addAll(luss);
        }
        for (UserSequence us : lus) {
            data.add(new ApriorAllModelData(us.id + "", us));
        }
        return data;
    }

    private static List<UserSequence> reLabelUserSequence(List<List<Integer>> variationLists, UserSequence seed) throws Exception {
        List<UserSequence> result = new ArrayList<>();
        for (List<Integer> l : variationLists) {
            UserSequence us = new UserSequence(seed.username, seed.id);
            if (l.size() != seed.size()) {
                throw new Exception("oh oh oh ...!");
            } else {
                for (int i = 0; i < l.size(); i++) {
                    UserLocation ul = new UserLocation((UserLocation) seed.get(i).first(), l.get(i));
                    ItemSet is = new ItemSet();
                    is.add(ul);
                    us.add(is);
                }
            }
            result.add(us);
        }
        return result;
    }

    private static void generateSequenceVariations(List<List<Integer>> variationLists, List<ItemSet> us, List<Integer> head) {
        if (us.size() > 0) {
            UserLocation ul = (UserLocation) us.get(0).first();
            for (Integer i : ul.levelIds) {
                List<Integer> niu = new ArrayList<>();
                niu.addAll(head);
                niu.add(i);
                if (us.size() > 1) {
                    generateSequenceVariations(variationLists, us.subList(1, us.size()), niu);
                } else {
                    variationLists.add(niu);
                }
            }

        }
    }

    public static List<UserSequence> filterSequences(List<UserSequence> dailySequences, Map<Integer, HLocation> locationMap) {
        double labeledNoises = 0;
        boolean h = locationMap.size() > 0;
        List<UserSequence> resultSequences = new ArrayList<>();
        for (UserSequence us : dailySequences) {
            if (h) {
                labeledNoises += labelNoises(us, locationMap);
            }
            UserSequence userSequence = new UserSequence(us.username, us.id);
            int last_id = -1;
            for (ItemSet is : us) {
                UserLocation location = (UserLocation) is.first();
                if (location.location_id == last_id) {
                    last_id = location.location_id;
                } else if (location.location_id > 0) {
                    ItemSet items = new ItemSet();
                    items.add(location);
                    userSequence.add(items);
                    last_id = location.location_id;
                }
            }
            if (userSequence.size() > 1 && userSequence.size() < AprioriRunner.len_thrsh) {
                resultSequences.add(userSequence);
            }
        }
//        System.out.println("noises relabeled:"+labeledNoises/dailySequences.size());
        return resultSequences;
    }

    private static double labelNoises(UserSequence us, Map<Integer, HLocation> locationMap) {
        int counter = 0;
        int all = 0;
        for (ItemSet is : us) {
            List<HLocation> candidates = new ArrayList<>();
            UserLocation ul = (UserLocation) is.first();
            if (ul.location_id == -1) {
                all++;
                for (HLocation hloc : locationMap.values()) {
                    if (hloc.isInRadius(ul)) {
                        candidates.add(hloc);
                    }
                }
                if (candidates.isEmpty()) {
                    continue;
                } else {
                    counter++;
                    Optional<HLocation> op = candidates.stream().min((a, b) -> HLocation.distance(a, ul).compareTo(HLocation.distance(b, ul)));
                    if (op.isPresent()) {
                        ul.location_id = op.get().id;
                        ul.setItemId(op.get().id + "");
                        ul.levelIds = Arrays.asList(ul.location_id);
                    }
                }
            }
        }
        return counter/(double)all;
    }


    private static String findNearestGlobalLocation(String curr, List<String> allItems, Map<Integer, Location> locationMap) {
        String result = "";
        double min = Double.MAX_VALUE;
        for (String s : allItems) {
            double temp = 0.0;
            if ((temp = Location.centerDistance(locationMap.get(Integer.parseInt(curr)), locationMap.get(Integer.parseInt(s)))) < min) {
                min = temp;
                result = s;
            }
        }
        return result;
    }

    private static String findNearestSelfLocation(String curr, Set<String> items, Map<Integer, Location> locationMap) {
        String result = "";
        double min = Double.MAX_VALUE;
        for (String s : items) {
            double temp = 0.0;
            if ((temp = Location.centerDistance(locationMap.get(Integer.parseInt(curr)), locationMap.get(Integer.parseInt(s)))) < min) {
                min = temp;
                result = s;
            }
        }
        return result;
    }

    public static List<ApriorAllRule> addSimilarRules(List<String> heads, List<ApriorAllRule> ruleBase, Map<Integer, Location> locationMap, List<ApriorAllRule> candidates) {
        for (ApriorAllRule rule : ruleBase) {
            if (Location.centerDistance(locationMap.get(Integer.parseInt(heads.get(heads.size() - 1))),
                    locationMap.get(Integer.parseInt(rule.getConditionSeq().get(rule.getConditionSeq().size() - 1).iterator().next().getItemId()))
            ) < AprioriRunner.similarity_threshold) {
                candidates.add(rule);
            }
            if (rule.getConditionSeq().size() > 1 && heads.size() > 1) {
                if (Location.centerDistance(locationMap.get(Integer.parseInt(heads.get(heads.size() - 2))),
                        locationMap.get(Integer.parseInt(rule.getConditionSeq().get(rule.getConditionSeq().size() - 2).iterator().next().getItemId())))
                        > AprioriRunner.similarity_threshold) {
                    candidates.remove(rule);
                }
            }
        }
        return candidates;
    }

    public static boolean hasNew(List<String> heads, List<ApriorAllRule> rules, Set<String> items) {
        String last = heads.get(heads.size() - 1);
        if (!items.contains(last)) {
            return true;
        }
        return false;
    }

    public static double LCSuffix(List<String> head, List<ItemSet> conditionSeq) {
        int m = head.size() - 1;
        int n = conditionSeq.size() - 1;
        double suffix = 0;
        while (m >= 0 && n >= 0) {
            if (head.get(m).equalsIgnoreCase(conditionSeq.get(n).iterator().next().getItemId())) {
                suffix++;
                m--;
                n--;
            } else {
                return suffix;
            }
        }
        return suffix;
    }

    private static double WCSuffix(List<ItemSet> head, List<ItemSet> conditionSeq) {
        int[][] c = new int[head.size() + 1][conditionSeq.size() + 1];

        for (int i = 0; i < head.size(); i++) {
            c[i][0] = 0;
        }
        for (int j = 0; j < conditionSeq.size(); j++) {
            c[0][j] = 0;
        }
        for (int i = 0; i < head.size(); i++) {
            for (int j = 0; j < conditionSeq.size(); j++) {
                if (head.get(i).iterator().next().getItemId().equalsIgnoreCase(conditionSeq.get(j).iterator().next().getItemId())) {
                    c[i + 1][j + 1] = c[i][j] + 1;
                } else if (c[i + 1][j] >= c[i][j + 1]) {
                    c[i + 1][j + 1] = c[i + 1][j];
                } else {
                    c[i + 1][j + 1] = c[i][j + 1];
                }
            }
        }
        int[] common = new int[c[head.size()][conditionSeq.size()]];
        int cIndex = common.length - 1;
        int n = conditionSeq.size();
        for (int i = head.size(); i > 0; i--) {
            if (c[i][n] > c[i - 1][n]) {
                common[cIndex] = i - 1;
                cIndex--;
            }
        }

        double score = 0;
        for (int aCommon : common) {
            score += 1.0 / (aCommon + 1);
        }
        return score;
    }

    public static double LCSubSeq(List<ItemSet> head, List<ItemSet> conditionSeq) {
        int[][] c = new int[head.size() + 1][conditionSeq.size() + 1];

        for (int i = 0; i < head.size(); i++) {
            c[i][0] = 0;
        }
        for (int j = 0; j < conditionSeq.size(); j++) {
            c[0][j] = 0;
        }
        for (int i = 0; i < head.size(); i++) {
            for (int j = 0; j < conditionSeq.size(); j++) {
                if (head.get(i).iterator().next().getItemId().equalsIgnoreCase(conditionSeq.get(j).iterator().next().getItemId())) {
                    c[i + 1][j + 1] = c[i][j] + 1;
                } else if (c[i + 1][j] >= c[i][j + 1]) {
                    c[i + 1][j + 1] = c[i + 1][j];
                } else {
                    c[i + 1][j + 1] = c[i][j + 1];
                }
            }
        }
        return c[head.size()][conditionSeq.size()];
    }

    public static double commonElement(List<ItemSet> head, List<ItemSet> conditionSeq) {
        Set<String> common = new HashSet<>();
        for (ItemSet is : head) {
            for (ItemSet ic : conditionSeq) {
                String temp = "";
                if ((temp = is.iterator().next().getItemId()).equalsIgnoreCase(ic.iterator().next().getItemId())) {
                    common.add(temp);
                }
            }
        }
        return common.size();
    }

    public static boolean lastItem(List<String> head, List<ItemSet> conditionSeq) {
        return conditionSeq.get(conditionSeq.size() - 1).iterator().next().getItemId().equalsIgnoreCase(head.get(head.size() - 1));
    }

    public static void generatePoint(int radius, int minPts) {
        try {
            LocationDetection ld = new LocationDetection();
            ld.makePoints("");
            for (String key : ld.map.keySet()) {
                List<StayPoint> stayPoints = new ArrayList<>();
                stayPoints.addAll(ld.map.get(key));
                ld.DBSCAN(stayPoints, radius, minPts, key);
                ld.writeSPsWithLocations(ld.map.get(key), radius + "," + minPts + "," + key, "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double findCandidates(List<String> heads, List<ApriorAllRule> rules, List<ApriorAllRule> candidates) {
        double maxScore = 0;
        for (ApriorAllRule rule : rules) {
            double lcSuffix = FunctionStore.LCSuffix(heads, rule.getConditionSeq());
            if (lcSuffix > maxScore) {
                maxScore = lcSuffix;
                candidates.clear();
                candidates.add(rule);
            } else if (lcSuffix == maxScore
                    && lcSuffix > 0.0
                    ) {
                candidates.add(rule);
            }
        }
        return maxScore;
    }
    public static void findCandidates(List<String> heads, List<ApriorAllRule> rules, Map<String, Double> scores, String matchMethod) {
        for (ApriorAllRule rule : rules) {
            double score=0;
            if (matchMethod.equals(HierarchicalRunner.LONGEST_COMMON_SUFFIX)) {
                score = FunctionStore.LCSuffix(heads, rule.getConditionSeq());
            }else if (matchMethod.equals(HierarchicalRunner.LAST_ITEM)){
                score = FunctionStore.lastItem(heads, rule.getConditionSeq())?heads.size():0;
            }else if (matchMethod.equals(HierarchicalRunner.ALL_MATCH)){
                score = FunctionStore.wholeMatch(heads, rule.getConditionSeq())?heads.size():0;
            }

            if (score > 0) {
                String target = rule.getConsequenceSeq().get(0).iterator().next().getItemId();
                if (scores.containsKey(target)) {
                    scores.put(target, scores.get(target) + rule.getApriorPrecision().getMinConf()*(score/heads.size()));
                } else {
                    scores.put(target, rule.getApriorPrecision().getMinConf()*(score/heads.size()));
                }
            }
        }
    }

    private static boolean wholeMatch(List<String> head, Sequence conditionSeq) {
        int m = head.size() - 1;
        int n = conditionSeq.size() - 1;
        while (m >= 0 && n >= 0) {
            if (head.get(m).equalsIgnoreCase(conditionSeq.get(n).iterator().next().getItemId())) {
                m--;
                n--;
            } else {
                return false;
            }
        }
        return true;
    }

    public static List<String> getValidUsers(String path, Integer limit) {
        List<String> validUsers = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            for (int i = 0; i < limit; i++) {
                validUsers.add(new StringTokenizer(reader.readLine(), ",").nextToken().substring(1, 4));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return validUsers;
    }

    public static void makeInfrastructure(Map<String, List<StayPoint>> map, List<String> validUsers, double train_ratio, double minSup, double minConf,
                                          PrintWriter file_PrWr, Map<Integer, Location> locationMap, Map<String, List<ApriorAllRule>> ruleBase,
                                          Map<String, List<UserSequence>> testBase, List<ApriorAllRule> allRules, List<String> allItems) {
        StringBuilder logger = new StringBuilder();
        for (String key : map.keySet()) {
            if (validUsers.contains(key)) {
                createRules(map.get(key), key, train_ratio, minSup, minConf, file_PrWr, locationMap, ruleBase, testBase, logger);
            } else {
                file_PrWr.println(key + "'s sequences where below threshold\n");
            }
        }
        System.out.println(logger.toString());
        for (String s : ruleBase.keySet()) {
            allRules.addAll(ruleBase.get(s));
        }
        for (ApriorAllRule r : allRules) {
            for (ItemSet is : r.getConditionSeq()) {
                allItems.add(is.iterator().next().getItemId());
            }
        }
    }

    public static void aprioriPrediction(PrintWriter file_PrWr, Map<Integer, Location> specLocationMap, Map<Integer, Location> genLocationMap, List<String> validUsers) {
        StringBuilder builder = new StringBuilder();
        StringBuilder logger = new StringBuilder();
        //username, {0#alltests, 1#totalCorrect, 2#ruledtests, 3#ruledcorrect, 4#randTest, 5#randCorrect, 6#genTest, 7#genCorrect , 8#errorCounts, 9#errorValueSum, 10#test size}
        Map<String, int[]> stats = new HashMap<>();
        StringBuilder checker = new StringBuilder();
        for (String username : validUsers) {
//            predict(username, AprioriRunner.ruleBaseSpec.get(username), AprioriRunner.ruleBaseGen.get(username), AprioriRunner.testBaseSpec.get(username), AprioriRunner.testBaseGen.get(username), specLocationMap, builder, false, stats);
            predict(username, AprioriRunner.ruleBaseSpec.get(username), AprioriRunner.ruleBaseGen.get(username), AprioriRunner.testBaseSpec.get(username), AprioriRunner.testBaseGen.get(username), specLocationMap, genLocationMap, builder, false, stats, checker);
        }
        stats.keySet().forEach(k -> logger.append(k).append("\ttest size:").append(stats.get(k)[10]).append("\tall samples:").append(stats.get(k)[0])
                .append("\tacc all %").append((double)stats.get(k)[1]/stats.get(k)[0]).append("\truled:").append((double)stats.get(k)[3]/stats.get(k)[2])
                .append("\trand rule:").append((double)stats.get(k)[5]/stats.get(k)[4]).append("\tgen rule:").append((double)stats.get(k)[7]/stats.get(k)[6])
                .append("\terror avg:").append((double)stats.get(k)[9]/stats.get(k)[8]).append("\n"));
        Optional<int[]> res = stats.keySet().stream().filter(q->stats.get(q)!=null).map(q -> stats.get(q))
                .reduce((a1, a2) -> {
                    for (int i = 0; i < a1.length; i++) {
                        a1[i] = a1[i] + a2[i];
                    }
                    return a1;
                });
//        System.out.println(checker.toString());
        logger.append("total results:").append((double)res.get()[1]/res.get()[0]).append("\tself rule:").append((double)res.get()[3]/res.get()[2])
                .append("\trand rule:").append((double)res.get()[5]/res.get()[4]).append("\tgen rule:").append((double)res.get()[7]/res.get()[6])
                .append("\terror avg:").append((double)res.get()[9]/res.get()[8]).append("\n").append(Arrays.toString(res.get()));

        System.out.println(logger.toString());
//        System.out.println(AprioriRunner.gen_size_mismatch);
//        System.out.println(AprioriRunner.has_gen_match);
//        System.out.println(AprioriRunner.rule_fail);
//        file_PrWr.print(builder.toString());
    }

    public static void predict(String username, List<ApriorAllRule> specRules, List<ApriorAllRule> genRules, List<UserSequence> specTestSeq, List<UserSequence> genTestSeq,
                               Map<Integer, Location> specLocationMap, Map<Integer, Location> genLocationMap, StringBuilder shrt, boolean lastItem,
                               Map<String, int[]> stats, StringBuilder checker) {
        checker.append(username).append(":\n");
        if (specRules == null || specRules.size() == 0) {
            shrt.append("No Rules!\n");
            return;
        }
        Set<String> items = new HashSet<>();
        for (ApriorAllRule rule : specRules) {
            for (ItemSet is : rule.getConditionSeq()) {
                items.add(is.iterator().next().getItemId());
            }
            for (ItemSet is : rule.getConsequenceSeq()) {
                items.add(is.iterator().next().getItemId());
            }
        }
        //username, {0#alltests, 1#totalCorrect, 2#ruledtests, 3#ruledcorrect, 4#randTest, 5#randCorrect, 6#genTest, 7#genCorrect , 8#errorCounts, 9#errorValueSum, 10#testSequenceSize}
        int[] stat = new int[11];
        if (!lastItem) {
            stat[10] = (int)specTestSeq.stream().filter(a->a.size()>1).count();
            for (UserSequence us : specTestSeq) {
//                Optional<UserSequence> gen = AprioriRunner.testBaseGen.get(us.username).stream().filter(p -> p.id == us.id).findAny();
                if (us.size() > 1) {
                    for (int i = 1; i < us.size(); i++) {
                        List<ItemSet> condition = us.subList(0, i);
                        shrt.append("\n\nactive sequence:\t").append(us.toString()).append("position:").append(i).append("\n");
                        ULocHolder holder = new ULocHolder();
                        AtomicBoolean useSelfRule = new AtomicBoolean(false);
                        AtomicBoolean useRandRule = new AtomicBoolean(false);
                        AtomicBoolean useGenRule = new AtomicBoolean(false);
                        String next = whatNext(us, condition, holder, specRules, genRules, genTestSeq, shrt, items, specLocationMap, useSelfRule, useRandRule, useGenRule, checker);
                        UserLocation nextLoc;
                        if (useGenRule.get()) {
                            nextLoc = holder.location;
                            stat[6] = stat[6] + 1;
                        } else {
                            nextLoc = (UserLocation) us.get(i).first();
                        }
                        String realNext = nextLoc.getItemId();
                        stat[0] = stat[0] + 1;
                        if (useSelfRule.get()) {
                            stat[2] = stat[2] + 1;
                        }
                        if (useRandRule.get()) {
                            stat[4] = stat[4] + 1;
                        }
                        AprioriRunner.testCount++;
                        if (next.length() > 0) {
                            stat[8] = stat[8] + 1;
                            if (next.equalsIgnoreCase(realNext)) {
                                AprioriRunner.correctCount++;
//                                AprioriRunner.SPCorrectError.add(HLocation.stayPointDistance(specLocationMap.get(Integer.parseInt(next)), nextLoc));
                                stat[1] = stat[1] + 1;
                                if (useSelfRule.get()) {
                                    stat[3] = stat[3] + 1;
                                }
                                if (useRandRule.get()) {
                                    stat[5] = stat[5] + 1;
                                }
                                if (useGenRule.get()) {
                                    stat[7] = stat[7] + 1;
                                }
                            }
//                            double error = HLocation.centerDistance(specLocationMap.get(Integer.parseInt(next)), specLocationMap.get(Integer.parseInt(realNext)));
//                            double error =0.0;// HLocation.centerDistance(specLocationMap.get(Integer.parseInt(next)), specLocationMap.get(Integer.parseInt(realNext)));
                            if (useGenRule.get()) {
                                stat[9] = stat[9] + Location.stayPointDistance(genLocationMap.get(Integer.parseInt(next)), nextLoc).intValue();
                            } else {
                                stat[9] = stat[9] + Location.stayPointDistance(specLocationMap.get(Integer.parseInt(next)), nextLoc).intValue();
                            }
//                            AprioriRunner.errors.add(error);
//                            if (useSelfRule.get()) {
//                                AprioriRunner.fromRandomError.add(error);
//                            }
                        }
                    }
                }
            }
//            logger.append(AprioriRunner.correctCount).append("\t").append(AprioriRunner.testCount).append("\t");
//            logger.append((double) AprioriRunner.correctCount / AprioriRunner.testCount);
            if (AprioriRunner.testCount < 1) {
                shrt.append("no result\n");
            } else {
                AprioriRunner.precision.add((double) AprioriRunner.correctCount / AprioriRunner.testCount);
                AprioriRunner.precisionCount.add(AprioriRunner.testCount);
                shrt.append("\nresult:").append((double) AprioriRunner.correctCount / AprioriRunner.testCount).append("\n");
            }
            stats.put(username, stat);
        } else {
            /**
             * When Predicting Only The Last Item.
             */
            /*for (UserSequence us : testSeq) {
                if (us.size() > 1) {
                    testCount++;
                    shrt.append("\n\nactive sequence:\t").append(us.toString()).append("\n");
                    String next = whatNext(us.subList(0, us.size() - 1), rules, shrt, items);
                    if (next.equalsIgnoreCase(us.get(us.size() - 1).iterator().next().getItemId())) {
                        correctCount++;
                    }
                }
            }*/
        }
    }

    public static void hPredict(String username, List<ApriorAllRule> rules, List<UserSequence> testSeq, Map<Integer, HLocation> locationMap,
                                StringBuilder builder, Map<String, int[]> stats, StringBuilder checker) {
        if (rules.size() == 0) {
            builder.append("No Rules!\n");
            return;
        }
        //username, {0#alltests, 1#totalCorrect, 2#ruledtests, 3#ruledcorrect, 4#randTest, 5#randCorrect, 6#errorCounts, 7#errorValueSum}
        int[] stat = new int[8];
        checker.append(username).append(":\n");
        for (UserSequence us : testSeq) {
            if (us.size() > 1) {
                for (int i = 1; i < us.size(); i++) {
                    List<ItemSet> condition = us.subList(0, i);
                    AtomicBoolean useSelfRule = new AtomicBoolean(false);
                    AtomicBoolean useRandRule = new AtomicBoolean(false);
                    AtomicBoolean useGenRule = new AtomicBoolean(false);
                    String next = "";
                    next = whatNextH(condition, rules, builder, useSelfRule, useRandRule, checker);
                    UserLocation nextLoc;
                    nextLoc = (UserLocation) us.get(i).first();
//                    String realNext = nextLoc.getItemId();
                    List<String> nextItems = nextLoc.levelIds.stream().map(p -> p.toString()).collect(Collectors.toList());
                    stat[0] = stat[0] + 1;
                    if (useSelfRule.get()) {
                        stat[2] = stat[2] + 1;
                    }
                    if (useRandRule.get()) {
                        stat[4] = stat[4] + 1;
                    }
                    if (next.length() > 0) {
                        if (nextItems.contains(next)) {
                            stat[1] = stat[1] + 1;
                            if (useSelfRule.get()) {
                                stat[3] = stat[3] + 1;
                            }
                            if (useRandRule.get()) {
                                stat[5] = stat[5] + 1;
                            }
                        }

                        stat[6] = stat[6] + 1;
                        stat[7] = stat[7] + HLocation.distance(locationMap.get(Integer.parseInt(next)), nextLoc).intValue();
                    }
                }
            }
        }
        stats.put(username, stat);
    }

    public static String whatNext(UserSequence seq, List<ItemSet> head, ULocHolder nextLoc, List<ApriorAllRule> specRules, List<ApriorAllRule> genRules,
                                  List<UserSequence> genSequences, StringBuilder shrt, Set<String> items, Map<Integer, Location> locationMap, AtomicBoolean useSelfRule, AtomicBoolean userRandRule, AtomicBoolean userGenRule, StringBuilder checker) {
        List<String> heads = new ArrayList<>();
        head.stream().forEach(p -> heads.add(p.iterator().next().getItemId()));
        List<ApriorAllRule> candidates = new ArrayList<>();

        double similarity = findCandidates(heads, specRules, candidates);

        if (!candidates.isEmpty()) {
            useSelfRule.set(true);
            checker.append(heads.toString()).append("\n");
        }
        if (candidates.isEmpty()) {
            // Note: add rules according to new locations
//            if (FunctionStore.hasNew(heads, specRules, items)) {
//                addSimilarRules(heads, allRulesSpec , locationMap, candidates);
//                String nearestLocation = findNearestSelfLocation(heads.get(heads.size() - 1), items, locationMap);
//                String nearestLocation = findNearestGlobalLocation(heads.get(heads.size() - 1), allItemsSpec, locationMap);
//                heads.set(heads.size()-1,nearestLocation);
//                findCandidates(heads, rules, candidates);
//            }
/**/
            // Note: use same general sequence
            /*if (candidates.isEmpty()){
                AprioriRunner.rule_fail++;
                Optional<UserSequence> matchSeq = genSequences.stream().filter(a->a.id == seq.id).findFirst();
                if (matchSeq.isPresent()){
                    AprioriRunner.has_gen_match++;
                    if (matchSeq.get().size() == seq.size()){
                        head = matchSeq.get().subList(0,head.size());
                        nextLoc.location = (UserLocation) matchSeq.get().get(head.size()).first();
                        similarity = findCandidates(head.stream().map(p -> p.iterator().next().getItemId()).collect(Collectors.toList()), genRules, candidates);
                    }else if (matchSeq.get().size() > seq.size()){
                        head = matchSeq.get().subList(0,head.size());
                        nextLoc.location = (UserLocation) matchSeq.get().get(head.size()).first();
                        similarity = findCandidates(head.stream().map(p -> p.iterator().next().getItemId()).collect(Collectors.toList()), genRules, candidates);
                    }else{
                        AprioriRunner.gen_size_mismatch++;
                    }
                    if (!candidates.isEmpty()){
                        userGenRule.set(true);
                    }
                }
            }*/
            // Note: adding random rules
            if (candidates.isEmpty()) {
                candidates.addAll(specRules);
                userRandRule.set(true);
            }
        }
        String result = "";
        if (candidates.isEmpty()) {
            return result;
        }
        shrt.append("candidate rules:\t");
        candidates.forEach(p -> shrt.append(p.toString()).append("\n"));

        Map<String, Double> scores = new HashMap<>();
        for (ApriorAllRule rule : candidates) {
            String target = rule.getConsequenceSeq().get(0).iterator().next().getItemId();
            if (scores.containsKey(target)) {
                scores.put(target, scores.get(target) + rule.getApriorPrecision().getMinSup() * rule.getApriorPrecision().getMinConf());
//                scores.put(target,scores.get(target) + rule.getApriorPrecision().getMinSup()*rule.getApriorPrecision().getMinConf());
            } else {
                scores.put(target, rule.getApriorPrecision().getMinSup() * rule.getApriorPrecision().getMinConf());
//                scores.put(target,rule.getApriorPrecision().getMinSup()*rule.getApriorPrecision().getMinConf());
            }
        }
        double max = 0.0;
        String best = "";
        for (String s : scores.keySet()) {
            if (scores.get(s) > max) {
                best = s;
                max = scores.get(s);
            }
        }
        result = best;

/*        double maxConfSup = 0.0;
        for (ApriorAllRule r:candidates){
//            if (r.getApriorPrecision().getMinConf()+r.getApriorPrecision().getMinSup()>=maxConfSup){
            if (r.getApriorPrecision().getMinConf()>=maxConfSup){
//                maxConfSup = r.getApriorPrecision().getMinConf()+r.getApriorPrecision().getMinSup();
                maxConfSup = r.getApriorPrecision().getMinConf();
            }
        }
        final double d1 = maxConfSup;
//        candidates = candidates.stream().filter(p->p.getApriorPrecision().getMinConf()+p.getApriorPrecision().getMinSup()>=d1).collect(Collectors.toList());
        candidates = candidates.stream().filter(p->p.getApriorPrecision().getMinConf()>=d1).collect(Collectors.toList());

        shrt.append("selected rules:\t");
        candidates.forEach(p -> shrt.append(p.toString()));
        List<String> candidateList = candidates.stream().map(p->p.getConsequenceSeq().get(0).iterator().next().getItemId()).collect(Collectors.toList());
        Map<String, Long> countMap;
        countMap = candidateList.stream().collect(Collectors.groupingBy(p -> p, Collectors.counting()));
        Long best = 0L;
        for (String s:countMap.keySet()){
            if (countMap.get(s)>=best){
                best = countMap.get(s);
                result = s;
            }
        }
        shrt.append(countMap.toString()).append(result);*/
        return result;
    }

    public static String whatNextH(List<ItemSet> head, List<ApriorAllRule> rules, StringBuilder builder,
                                   AtomicBoolean useSelfRule, AtomicBoolean userRandRule, StringBuilder checker) {
        List<ApriorAllRule> candidates = new ArrayList<>();
        List<UserLocation> headLocations = new ArrayList<>();
        head.stream().forEach(p -> headLocations.add((UserLocation) p.iterator().next()));
        List<List<Integer>> variations = new ArrayList<>();
        generateSequenceVariations(variations, head, new ArrayList<>());
        List<List<String>> orderedVariations = generateOrderedVariations(variations);
        double similarity = 0;
        for (List<String> varList : orderedVariations) {
            similarity = findCandidates(varList, rules, candidates);
            if (!candidates.isEmpty()) {
                checker.append(varList).append("\n");
                break;
            }
        }
        if (!candidates.isEmpty()) {
            useSelfRule.set(true);
        }
        if (candidates.isEmpty()) {
            //adding random rules
            if (candidates.isEmpty()) {
                candidates.addAll(rules);
                userRandRule.set(true);
            }
        }
        String result = "";
        if (candidates.isEmpty()) {
            return result;
        }
        List<ApriorAllRule> highCandidates = candidates.stream().filter((rule) -> isFirstLayer(rule)).collect(Collectors.toList());
        candidates = highCandidates.size() > 0 ? highCandidates : candidates;
        builder.append("candidate rules:\t");
        candidates.forEach(p -> builder.append(p.toString()).append("\n"));

        Map<String, Double> scores = new HashMap<>();
        for (ApriorAllRule rule : candidates) {
            String target = rule.getConsequenceSeq().get(0).iterator().next().getItemId();
            if (scores.containsKey(target)) {
                scores.put(target, scores.get(target) + rule.getApriorPrecision().getMinSup() * rule.getApriorPrecision().getMinConf());
//                scores.put(target, scores.get(target) + rule.getApriorPrecision().getMinConf()*similarity);
            } else {
                scores.put(target, rule.getApriorPrecision().getMinSup() * rule.getApriorPrecision().getMinConf());
//                scores.put(target, rule.getApriorPrecision().getMinConf()*similarity);
            }
        }
        double max = 0.0;
        String best = "";
        for (String s : scores.keySet()) {
            if (scores.get(s) > max) {
                best = s;
                max = scores.get(s);
            }
        }
        result = best;
        return result;
    }

    private static boolean isFirstLayer(ApriorAllRule rule) {
        for (ItemSet is : rule.getConditionSeq()) {
            if (Integer.parseInt(is.first().getItemId()) >= 1000) {
                return false;
            }
        }
        for (ItemSet is : rule.getConsequenceSeq()) {
            if (Integer.parseInt(is.first().getItemId()) >= 1000) {
                return false;
            }
        }
        return true;
    }

    private static List<List<String>> generateOrderedVariations(List<List<Integer>> headLocations) {
        Double[] scores = new Double[headLocations.size()];
        for (int i = 0; i < headLocations.size(); i++) {
            double score = 0.0;
            for (int j = 0; j < headLocations.get(i).size(); j++) {
                if (headLocations.get(i).get(j) < 1000) {
                    score += 1;
                } else {
                    score += (j + 1) * (1.0 / headLocations.get(i).get(j));
                }
            }
            scores[i] = score;
        }
        ArrayIndexSort comparator = new ArrayIndexSort(scores, false);
        Integer[] indexes = comparator.createIndexArray();
        Arrays.sort(indexes, comparator);
        List<List<String>> finall = new ArrayList<>(headLocations.size());
        for (int i = 0; i < indexes.length; i++) {
            finall.add(i, headLocations.get(indexes[i]).stream().map(q -> q + "").collect(Collectors.toList()));
        }
        return finall;
    }

    public static List<UserLocation> getUserLocations(List<ItemSet> seq) {
        List<TransformedItem> tis = new ArrayList<>();
        for (int i = 0; i < seq.size(); i++) {
            tis.add((TransformedItem) seq.get(i).first());
        }
        List<UserLocation> userLocations = new ArrayList<>();
        for (TransformedItem ti : tis) {
            userLocations.add((UserLocation) ti.first());
        }
        return userLocations;
    }

    public static void checkValidity(Map<String, List<UserSequence>> testBaseSpec, Map<String, List<UserSequence>> testBaseGen, List<String> validUsers, Map<String, List<StayPoint>> genUsrSpMap, Map<String, List<StayPoint>> specUsrSpMap) {
        for (String s : validUsers) {
            if (!testBaseGen.containsKey(s) || !testBaseSpec.containsKey(s)) {
                System.err.println("error 1:" + s);
                break;
            } else if (testBaseGen.get(s).size() != testBaseSpec.get(s).size()) {
                System.err.println("error 2:" + s + " " + testBaseGen.get(s).size() + "," + testBaseSpec.get(s).size());
                break;
            } else if (specUsrSpMap.get(s).size() != genUsrSpMap.get(s).size()) {
                System.err.println("error 3:" + s + ";" + specUsrSpMap.get(s).size() + s + ";" + genUsrSpMap.get(s).size());
                break;
            }

        }
    }

    public static void labelStayPoints(List<StayPoint> stayPoints, Map<Integer, List<Integer>> clustered) {
        for (StayPoint sp : stayPoints) {
            if (clustered.containsKey(sp.clusterID)) {
                sp.levelIds = editList(clustered.get(sp.clusterID));
            }
        }
    }

    /**
     * reverses the order of locations
     *
     * @param integers
     * @return
     */
    public static List<Integer> editList(List<Integer> integers) {
        List<Integer> reverse = new ArrayList<>();
        for (int i = 0; i < integers.size(); i++) {
            reverse.add(integers.get(integers.size() - i - 1));
        }
        return reverse;
    }

    public static List<UserSequence> makeAndFilterSequences(List<StayPoint> mainSequence, String username, Map<Integer, HLocation> locationMap, StringBuilder logger) {
        List<UserSequence> dailySequences = makeDailySequences(mainSequence, username);
        logger.append(username).append("\tinitial sequences count:").append(dailySequences.size()).append("\taverage length:")
                .append(dailySequences.stream().map(p -> p.size()).collect(Collectors.averagingInt(p -> p)));
        dailySequences = filterSequences(dailySequences, locationMap);
        logger.append("\tfiltered count:").append(dailySequences.size()).append("\taverage length:").append(dailySequences.stream().map(p -> p.size()).collect(Collectors.averagingInt(p -> p))).append("\t");
        return dailySequences;
    }

    public static List<ApriorAllRule> createHRules(List<UserSequence> dailySequences, String username, double train_ratio, double minConf, double minSup,
                                                   StringBuilder logger) {
        List<ApriorAllModelData> data;
        try {
            data = convertHData(dailySequences);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        //Rules
        return AprioriAll.findAllFreqAssoRule(data, new ApriorPrecision(minConf, minSup));
    }

    public static Map<Integer, Cluster> getPointedClusters(Map<Integer, List<Cluster>> clustered) {
        Map<Integer, Cluster> res = new HashMap<>();
        for (List<Cluster> cL:clustered.values()){
            for (Cluster c:cL){
                if (!res.containsKey(c.getIndex())){
                    res.put(c.getIndex(),c);
                }
            }
        }
        System.out.println("allClusters:" + res.size());
        return res;
    }

    public static Integer getNearestLocation(Map<Integer, Cluster> firstLevelClusterMap, Cluster current) {
        double minDistance = Double.MAX_VALUE;
        List<Cluster> minCluster = new ArrayList<>();
        minCluster.add(current);
        for (Integer i : firstLevelClusterMap.keySet()){
            double dist = 0.0;
            if ((dist = Cluster.getDistance(current,firstLevelClusterMap.get(i))) < minDistance){
                if (dist!=0.0){
                    minDistance = dist;
                    minCluster.clear();
                    minCluster.add(firstLevelClusterMap.get(i));
                }
            }else if((dist = Cluster.getDistance(current,firstLevelClusterMap.get(i))) == minDistance){
                if (dist!=0.0) {
                    minCluster.add(firstLevelClusterMap.get(i));
                }
            }
        }
        return minCluster.size()>0?minCluster.get(new Random().nextInt(minCluster.size())).getIndex():current.getIndex();
    }
    public static Integer getNearestLocation(Map<Integer, Cluster> firstLevelClusterMap, StayPoint current) {
        double minDistance = Double.MAX_VALUE;
        List<Cluster> minCluster = new ArrayList<>();
        for (Integer i : firstLevelClusterMap.keySet()){
            double dist = 0.0;
            if ((dist = StayPoint.distance(current, firstLevelClusterMap.get(i).getEuclideanAverage())) < minDistance){
                if (dist!=0.0){
                    minDistance = dist;
                    minCluster.clear();
                    minCluster.add(firstLevelClusterMap.get(i));
                }
            }else if(StayPoint.distance(current, firstLevelClusterMap.get(i).getEuclideanAverage()) == minDistance){
                minCluster.add(firstLevelClusterMap.get(i));
            }
        }
        return minCluster.size()>0?minCluster.get(new Random().nextInt(minCluster.size())).getIndex():firstLevelClusterMap.get(0).getIndex();
    }
}
