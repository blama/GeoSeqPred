package utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Constants {
    public static final String StayPointsPath = "";

    public static void main(String[] args) {
        List<String> s = new ArrayList<>();
        s.addAll(Arrays.asList(new String[]{"dsad", "ds", "ds", "d", "ad", "dsadas"}));
        System.out.println(s);
        s.removeIf(p -> p.length() > 4);
        System.out.println(s);
        double d = s.stream().map(p -> p.length()).collect(Collectors.averagingInt(p -> p));
        System.out.println(d);

    }
}
