
package prediction.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class GlobalRunner {
    public static void main(String[] args) throws Exception {
        File file = new File("F:\\reza\\courses\\ECML2015 Discovery Challenge\\GeoLink\\Porto_taxi_data_training.csv");
        int count = 0;
        long chars = 0;
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(file));
        for (int i = 0; i < 1710671; i++) {
            line = reader.readLine();
            count++;
            chars += line.length();
        }
        /*while ((line = reader.readLine())!=null){
        }*/
        System.out.println(count);
        System.out.println(chars);
    }
}
/*    public static int len_thrsh = 20;1710671
    public static Map<Integer,Double> precision = new HashMap<>();
    public static void main(String[] args) {
        Map<String,List<StayPoint>> map = new TreeMap<>();
        Map<String,Integer> countMap = new TreeMap<>();
        double train_ratio = 0.8;
        double minSup = 0.1;
        double minConf = 0.1;
        int seq_len_thrd = 20;
        String url = "stayPoints+loc";
        try {
            PrintWriter sh_wr = new PrintWriter(new FileWriter("result/short-res.txt"),true);
            PrintWriter lg_wr = new PrintWriter(new FileWriter("result/long-res.txt"),true);
            readSequences(map,countMap,url);
            AprioriRunner.aprioriPrediction(map, train_ratio, minSup, minConf, seq_len_thrd, sh_wr, lg_wr);
            */
/*int sumCount=0;
            Double sumVal=0.0;
            for (Integer i:precision.keySet()){
                sumCount+=i;
                sumVal+=precision.get(i)*i;
            }*//*

            String s = "avg precision:" //+ sumVal/sumCount;
                    + precision.values().stream().collect(Collectors.averagingDouble(p -> p));
            sh_wr.println(s);
            lg_wr.println(s);
            System.out.println(s);
        } catch (IOException e) {e.printStackTrace();}

//        generateAllocatedPoint(30,3);
    }


    */
/**
 * Sequences of stay points have been written on file after detecting locations with corresponding cluster ids.
 *
 * @param map
 * @param countMap
 * @param url address to root of files  @throws IOException
 *//*

    public static void readSequences(Map<String, List<StayPoint>> map, Map<String, Integer> countMap, String url) throws IOException {
        File root = new File(url);
        if (root.isDirectory()) {
            for (File child : root.listFiles()) {
                ArrayList<StayPoint> stayPoints = new ArrayList<>();
                BufferedReader reader = new BufferedReader(new FileReader(child));
                String line;
                String username = child.getName().substring(child.getName().lastIndexOf(",")+1,child.getName().lastIndexOf(".txt"));
//                String username = child.getName().substring(0,child.getName().lastIndexOf(".txt"));
                while ((line=reader.readLine()) != null) {
                    stayPoints.add(new StayPoint(line,username,true));
                }
                map.put(username,stayPoints);
                countMap.put(username, stayPoints.size());
            }
        }
    }

    public static void createRules(List<StayPoint> sequence, String username, double length_threshold, double minConf, double minSup, PrintWriter sh_wr, PrintWriter lg_wr){
        StringBuilder shrt = new StringBuilder();
        StringBuilder lng = new StringBuilder();
        shrt.append(username + "\n");
        lng.append(username + "\n");
        Set<Integer> distinct_loc = new HashSet<>();
        for (StayPoint st:sequence){
            distinct_loc.add(st.clusterID);
        }
        shrt.append("loc_cnt:" + distinct_loc.size()+"\n");
        lng.append("loc_cnt:" + distinct_loc.size() + "\n Locations: " + distinct_loc.toString()+"\n");
        List<UserSequence> allSequences = new ArrayList<>();
        UserSequence us = new UserSequence(username);
        double last_time = sequence.get(0).enterTime;
        int lastId = -1;
        for (StayPoint spz: sequence){
            ItemSet uls = new ItemSet();
            if (spz.clusterID > 0){
                UserLocation ul = new UserLocation(spz.clusterID,spz.enterTime,spz.leaveTime);
                if (spz.enterTime-last_time<1){
                    if (spz.clusterID != lastId){
                        uls.add(ul);
                        us.add(uls);
                        lastId = spz.clusterID;
                    }
                    else{
                        last_time = spz.enterTime;
                    }
                }
                else{
                    allSequences.add(us);
                    us = new UserSequence(username);
                    uls.add(ul);
                    us.add(uls);
                    last_time = spz.enterTime;
                    lastId = spz.clusterID;
                }
            }
        }
        List<ApriorAllModelData> data = new ArrayList<>();
        int singlesCount=0;
        int longleCount=0;
        // making model from train data
        int limit = (int)Math.round(length_threshold*allSequences.size());
        for (int i = 0; i < limit; i++) {
            if (allSequences.get(i).size()<2){singlesCount++;}
            else if (allSequences.get(i).size()>len_thrsh){longleCount++;}
            else{data.add(new ApriorAllModelData("", allSequences.get(i)));}
        }
        shrt.append("sequence count:"+allSequences.size() + "\tsingles:" + singlesCount+"\tlongles:"+longleCount+"\n");
        lng.append("sequence count:"+allSequences.size() + "\tsingles:" + singlesCount+"\tlongles:"+longleCount+"\n");

        List<ApriorAllRule> rules = AprioriAll.findAllFreqAssoRule(data, new ApriorPrecision(minConf,minSup));

        if (rules.size() == 0) {
            shrt.append("No Rules!\n");
            lng.append("No Rules!\n");
        } else {
            shrt.append("rules count: " + rules.size() + "\n");
            lng.append("rules count: " + rules.size() + "\n");
            lng.append("Sup\tConf\tRule\n");
            for (ApriorAllRule r : rules) {
                StringBuilder sb = new StringBuilder();
                sb.append(r.getConditionSeq());
                sb.append(" => ");
                sb.append(r.getConsequenceSeq());

                NumberFormat nf = NumberFormat.getPercentInstance();

                lng.append(nf.format(r.getApriorPrecision()
                        .getMinSup())
                        + "\t"
                        + nf.format(r.getApriorPrecision().getMinConf())
                        + "\t" + sb +"\n");
            }
        }
        String result = predict(rules,allSequences.subList(limit,allSequences.size()));
        shrt.append(result+"\n");
        lng.append(result+"\n");
        sh_wr.println(shrt.toString());
        sh_wr.flush();
        lg_wr.println(lng.toString());
        lg_wr.flush();
    }

    public static String predict(List<ApriorAllRule> rules, List<UserSequence> testSeq){
        int testCount = 0;
        int correctCount = 0;
        for (UserSequence s:testSeq){
            testCount++;
            if (s.size()==1){testCount--;}
            else if (s.size()>1){
                String result = whatNext(s.subList(0,s.size()-1),rules);
                ItemSet iset =null;
                Iterator<ItemSet> it = s.iterator();
                while (it.hasNext()){
                    iset = it.next();
                }
                if (iset.iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }

            }
            */
/*else if (s.size()==2){
                String result = whatNext(s.subList(0,1),rules);
                Iterator<ItemSet> it = s.iterator();
                it.next();
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
            }
            else if (s.size()==3){
                String result = whatNext(s.subList(0,1),rules);
                Iterator<ItemSet> it = s.iterator();
                it.next();
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
                result = whatNext(s.subList(0,2),rules);
                testCount++;
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
            }
            else if (s.size()==4){
                String result = whatNext(s.subList(0,1),rules);
                Iterator<ItemSet> it = s.iterator();
                it.next();
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
                result = whatNext(s.subList(0,2),rules);
                testCount++;
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
                result = whatNext(s.subList(0,3),rules);
                testCount++;
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
            }
            else if (s.size()==5){
                String result = whatNext(s.subList(0,1),rules);
                Iterator<ItemSet> it = s.iterator();
                it.next();
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
                result = whatNext(s.subList(0,2),rules);
                testCount++;
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
                result = whatNext(s.subList(0,3),rules);
                testCount++;
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
                result = whatNext(s.subList(0,4),rules);
                testCount++;
                if (it.next().iterator().next().getItemId().equalsIgnoreCase(result)){
                    correctCount++;
                }
            }*//*

        }
        if (testCount<1){
            return "no result";
        }
        precision.put(testCount,(double)correctCount/testCount);
        return "result:" + (double)correctCount/testCount;
    }

    public static String whatNext(List<ItemSet> head, List<ApriorAllRule> rules){
        List<ApriorAllRule> candidates = new ArrayList<>();
//        if (head.size()==1){
            for (ApriorAllRule r:rules){
                if (r.getConditionSeq().size()>0  && r.getConditionSeq().get(r.getConditionSeq().size()-1).iterator().next().getItemId().equalsIgnoreCase(head.get(head.size()-1).iterator().next().getItemId())){
                    candidates.add(r);
                }
            }
            int best=0;
            for (int i = 0; i <candidates.size(); i++) {
                if (candidates.get(i).getApriorPrecision().getMinConf()+candidates.get(i).getApriorPrecision().getMinSup()>=best){
                    best=i;
                }
            }
            if (candidates.size()>0){
                return candidates.get(best).getConsequenceSeq().get(0).iterator().next().getItemId();
            }else{
                return "";
            }
//        }
    }

    public static int containsCondition(){
        return 0;
    }
    public static void generateAllocatedPoint(int radius,int minPts){
        try {
            LocationDetection ld = new LocationDetection();
            ld.makePoints("");
            for (String key:ld.map.keySet()){
                List<StayPoint> stayPoints = new ArrayList<>();
                stayPoints.addAll(ld.map.get(key));
                ld.DBSCAN(stayPoints,radius,minPts,key);
                ld.writeSPsWithLocations(ld.map.get(key),radius+","+minPts+","+key);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}*/
