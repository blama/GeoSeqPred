package prediction.util;

import datapreparation.Location;
import datapreparation.StayPoint;
import prediction.aprioriall.ApriorAllRule;
import prediction.location.UserSequence;
import utility.FunctionStore;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class AprioriRunner {

//    public static String staypointsSpecURL = "stayPoints+loc(timeString)";
    public static String staypointsSpecURL = "stayPoints(30,200)+loc(DBSCAN,30,3)";
    public static String staypointsGenURL = "stayPoints+loc+all(timeString)";
    public static String genResultURI = "result/gen-result.txt";
    public static String specResultURI = "result/spec-result.txt";

    public static int len_thrsh = 20;
    public static double train_ratio = 0.8;
    public static double similarity_threshold = 500;
    public static double minSup = 0.0;
    public static double minSupGen = 0.0;
    public static double minConf = 0.0;
    public static double minConfGen = 0.0;

    public static int no_rule_count = 0;
    public static int has_gen_match = 0;
    public static int gen_size_mismatch = 0;
    public static int rule_fail = 0;
    public static int testCount = 0;
    public static int correctCount = 0;
    public static int fromRandomCounter = 0;
    public static int fromRandomCorrect = 0;

    public static List<Double> errors = new ArrayList<>();
    public static List<Double> fromRandomError = new ArrayList<>();
    public static List<Double> SPCorrectError = new ArrayList<>();
    public static List<Double> SPWrongError = new ArrayList<>();
    public static List<Double> precision = new ArrayList<>();
    public static List<Integer> precisionCount = new ArrayList<>();
    public static Map<String, List<ApriorAllRule>> ruleBaseSpec = new HashMap<>();
    public static Map<String, List<ApriorAllRule>> ruleBaseGen = new HashMap<>();
    public static Map<String, List<UserSequence>> testBaseGen = new HashMap<>();
    public static Map<String, List<UserSequence>> testBaseSpec = new HashMap<>();
    public static List<ApriorAllRule> allRulesSpec = new ArrayList<>();
    public static List<ApriorAllRule> allRulesGen = new ArrayList<>();

    public static List<String> allItemsSpec = new ArrayList<>();
    public static List<String> allItemsGen = new ArrayList<>();


    public static void main(String[] args) {

        Map<String, List<StayPoint>> specUsrSpMap = new TreeMap<>();
        Map<String, List<StayPoint>> genUsrSpMap = new TreeMap<>();
        Map<String, Integer> specCountMap = new TreeMap<>();
        Map<String, Integer> genCountMap = new TreeMap<>();
        Map<Integer, Location> specLocationMap = new HashMap<>();
        Map<Integer, Location> genLocationMap = new HashMap<>();
        List<String> validUsers = FunctionStore.getValidUsers("result\\stayPointCount.csv", 61);
        validUsers.remove("035");
//        validUsers = Arrays.asList("144");

        try {
            PrintWriter file_PrWr = new PrintWriter(new FileWriter(specResultURI), true);

            //general locations
/**            FunctionStore.readSequences(genUsrSpMap, genCountMap, staypointsGenURL, genLocationMap, true);
             FunctionStore.makeInfrastructure(genUsrSpMap, validUsers, train_ratio, minSupGen, minConfGen, file_PrWr, genLocationMap, ruleBaseGen, testBaseGen, allRulesGen, allItemsGen);
 */
            //specific locations
            FunctionStore.readSequences(specUsrSpMap, specCountMap, staypointsSpecURL, specLocationMap, true);
            FunctionStore.makeInfrastructure(specUsrSpMap, validUsers, train_ratio, minSup, minConf, file_PrWr, specLocationMap, ruleBaseSpec, testBaseSpec, allRulesSpec, allItemsSpec);
//            List<Integer> lists = specUsrSpMap.keySet().stream().filter(p->validUsers.contains(p)).flatMap(q->specUsrSpMap.get(q).stream()).map(r->r.clusterID).distinct().collect(Collectors.toList());
//            System.out.println(">>>>"+(double)lists.size()/validUsers.size());
//            checkValidity(testBaseSpec, testBaseGen, validUsers, genUsrSpMap, specUsrSpMap);
            FunctionStore.aprioriPrediction(file_PrWr, specLocationMap, genLocationMap, validUsers);
//            exportResult(file_PrWr);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void exportResult(PrintWriter file_PrWr) {
        if (precision.size() != precisionCount.size()) {
            System.err.println("Error on Size");
        } else {
            double averageSum = 0.0;
            for (int i = 0; i < precision.size(); i++) {
                averageSum += precision.get(i) * precisionCount.get(i);
            }
            String s = "";
//                        "avg precision:" //+ sumVal/sumCount;
//                        + precision.stream().collect(Collectors.averagingDouble(p -> p))
//                        + "\t#entries: " + precision.size();
//                s += "\nweighted average precision: "+ averageSum/precisionCount.stream().mapToInt(Integer::intValue).sum()+
//                        "\t# entry tests:"+precisionCount.stream().mapToInt(Integer::intValue).sum()+" \n;; no_rule_prediction: "+ no_rule_count + " ;; new_item_prediction: "+ new_chronometer +
//                        " no_rule_counter: "+ no_rule_counter;
            s += "\naverage distance error: " + errors.stream().collect(Collectors.averagingDouble(p -> p)) + "\tcount:" + errors.size();
            s += "\ntests:" + testCount + " correct:" + correctCount + " fraction:" + (double) correctCount / testCount;
            s += "\nno rule cases: " + no_rule_count;
            s += "\nall faults: " + (testCount - correctCount) + " rule_fault: " + ((testCount - correctCount) - (fromRandomCounter - fromRandomCorrect)) + "random_fault: " + (fromRandomCounter - fromRandomCorrect);
            s += "\nrandom prediction count:" + fromRandomCounter + " random correct count:" + fromRandomCorrect + " fraction:" + (double) fromRandomCorrect / fromRandomCounter;
            s += "\nrandom distance error: " + fromRandomError.stream().collect(Collectors.averagingDouble(p -> p))
                    + " not from random error: " + ((errors.stream().collect(Collectors.summingDouble(p -> p)) - fromRandomError.stream().collect(Collectors.summingDouble(p -> p))) / (errors.size() - fromRandomError.size()));
            s += "\naverage stay point error for correct predictions: " + SPCorrectError.stream().collect(Collectors.averagingDouble(p -> p)) + " count:" + SPCorrectError.size();
            s += "\naverage stay point error for wrong predictions: " + SPWrongError.stream().collect(Collectors.averagingDouble(p -> p)) + " count:" + SPWrongError.size();
            file_PrWr.println(s);
            //            s+= "\n"+ errors.stream().collect(Collectors.averagingDouble(p->p));
            System.out.println(s);
        }
    }
}
