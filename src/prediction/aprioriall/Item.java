package prediction.aprioriall;

public interface Item extends Comparable<Item> {
    String getItemId();

    void setItemId(String itemId);
}
