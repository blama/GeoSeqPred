package prediction.aprioriall;

public class ApriorAllModelData {
    private String id;

    private Sequence sequence;

    public ApriorAllModelData() {
        super();
    }

    public ApriorAllModelData(String id, Sequence sequence) {
        super();
        this.id = id;
        this.sequence = sequence;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApriorAllModelData other = (ApriorAllModelData) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (sequence == null) {
            if (other.sequence != null)
                return false;
        } else if (!sequence.equals(other.sequence))
            return false;
        return true;
    }

    public String getId() {
        return id;
    }

    public Sequence getSequence() {
        return sequence;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result
                + ((sequence == null) ? 0 : sequence.hashCode());
        return result;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSequence(Sequence sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        return "ModelData [id=" + id + ", sequence=" + sequence + "]";
    }
}
