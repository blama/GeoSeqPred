package prediction.aprioriall;

public class ApriorPrecision {
    private double minConf = 0.01; // minimum confident
    private double minSup = 0.01; // minimum support

    public ApriorPrecision(double minConf, double minSup) {
        this.minConf = minConf;
        this.minSup = minSup;
    }

    public ApriorPrecision() {
    }

    public double getMinConf() {
        return minConf;
    }

    public double getMinSup() {
        return minSup;
    }

    public void setMinConf(double minConf) {
        this.minConf = minConf;
    }

    public void setMinSup(double minSup) {
        this.minSup = minSup;
    }
}