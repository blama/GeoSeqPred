package prediction.aprioriall;

import prediction.aprioriall.weblog.Attribute;

import java.util.*;

/**
 * Main Apriori algorithm processing class
 *
 * @author Kelvin
 */
public class Apriori {

    public static int countSup(List<ApriorModelData> data, ItemSet itemSet) {
        int count = 0;

        for (ApriorModelData m : data) {
            if (m.getItems().containsAll(itemSet)) {
                count++;
            }
        }

        return count;
    }

    public static SortedSet<ItemSet> findAllFreqSets(
            List<ApriorModelData> data, double minSup) {

        Set<ItemSet> candidates = generateC1(data);
        SortedSet<ItemSet> lastRoundFreqItemSet = generateLn(data, candidates,
                minSup);
        SortedSet<ItemSet> freqItemSets = lastRoundFreqItemSet;

        int maxNumFreqItems = maxNumFreqItems(data);

        for (int i = 2; i <= maxNumFreqItems; i++) {
            candidates = generateCn(i, lastRoundFreqItemSet);

            lastRoundFreqItemSet = generateLn(data, candidates, minSup);

            if (lastRoundFreqItemSet.size() == 0) {
                break;
            }

            freqItemSets.addAll(lastRoundFreqItemSet);
        }
        return freqItemSets;
    }

    public static SortedSet<ItemSet> findAllUniqueFreqSets(
            List<ApriorModelData> data, double minSup) {

        Set<ItemSet> candidates = generateC1(data);
        SortedSet<ItemSet> lastRoundFreqItemSet = generateLn(data, candidates,
                minSup);
        SortedSet<ItemSet> freqItemSets = lastRoundFreqItemSet;

        int maxNumFreqItems = maxNumFreqItems(data);

        for (int i = 2; i <= maxNumFreqItems; i++) {
            candidates = generateCn(i, lastRoundFreqItemSet);

            lastRoundFreqItemSet = generateLn(data, candidates, minSup);

            if (lastRoundFreqItemSet.size() == 0) {
                break;
            }

            SortedSet<ItemSet> previousFreqItemSets = freqItemSets;
            freqItemSets = new TreeSet<ItemSet>();

            label:
            for (ItemSet is1 : previousFreqItemSets) {
                for (ItemSet is2 : lastRoundFreqItemSet) {
                    if (is2.containsAll(is1)) {
                        continue label;
                    }
                }
                freqItemSets.add(is1);
            }
            freqItemSets.addAll(lastRoundFreqItemSet);
        }
        return freqItemSets;
    }

    private static Set<ItemSet> generateC1(List<ApriorModelData> data) {

        Set<ItemSet> ret = new HashSet<ItemSet>();

        for (ApriorModelData md : data) {
            for (Item item : md.getItems()) {
                ItemSet is = new ItemSet();
                is.add(item);
                ret.add(is);
            }
        }

        return ret;
    }

    private static Set<ItemSet> generateCn(int n,
                                           SortedSet<ItemSet> lastFreqSeqs) {

        Set<ItemSet> ret = new HashSet<ItemSet>();

        label:
        for (ItemSet is : lastFreqSeqs) {
            boolean on = false;
            for (ItemSet is2 : lastFreqSeqs) {
                if (on) {
                    Iterator<Item> seqIter = is.iterator();
                    Iterator<Item> seqIter2 = is2.iterator();

                    for (int j = 0; j < n - 2; j++) {
                        if (!seqIter.next().equals(seqIter2.next())) {
                            continue label;
                        }
                    }
                    ItemSet itemSet = new ItemSet();
                    itemSet.addAll(is);
                    itemSet.add(seqIter2.next());
                    ret.add(itemSet);

                } else if (is.equals(is2)) {
                    on = true;
                }
            }
        }

        return ret;
    }

    private static SortedSet<ItemSet> generateLn(List<ApriorModelData> data,
                                                 Set<ItemSet> candidates, double minSup) {

        SortedSet<ItemSet> ret = new TreeSet<ItemSet>();
        double minSupCount = data.size() * minSup;

        for (ItemSet itemSet : candidates) {
            if (isSignificant(data, itemSet, minSupCount)) {
                ret.add(itemSet);
            }
        }

        return ret;
    }

    public static boolean isSignificant(List<ApriorModelData> data,
                                        ItemSet itemSet, double sig) {
        int count = 0;

        for (ApriorModelData m : data) {
            if (m.getItems().containsAll(itemSet)) {
                count++;
                if (count >= sig) {
                    return true;
                }
            }
        }

        return false;
    }

    private static int maxNumFreqItems(List<ApriorModelData> data) {
        int maxNumFreqItems = 0;

        for (ApriorModelData d : data) {
            int size = d.getItems().size();
            if (size > maxNumFreqItems) {
                maxNumFreqItems = size;
            }
        }

        return maxNumFreqItems;
    }

    public static List<ApriorModelData> toModel(String[][] d) {
        List<ApriorModelData> data = new ArrayList<ApriorModelData>();

        for (String[] arr : d) {
            SortedSet<Item> items = new TreeSet<Item>();

            for (String str : arr) {
                Item item = new Attribute(str, null, null);
                items.add(item);
            }

            data.add(new ApriorModelData(items));
        }

        return data;
    }

//	@Test
//	public void zfaq1() {
//		String[][] d = { { "K", "A", "D", "B" }, { "D", "A", "C", "E", "B" },
//				{ "C", "A", "B", "E" }, { "B", "A", "D" } };
//
//		List<ApriorModelData> data = toModel(d);
//		SortedSet<ItemSet> freqItemSets = Apriori.findAllFreqSets(data, .6);
//		Assert.assertEquals(7, freqItemSets.size());
//
//		SortedSet<ItemSet> freqItemSets2 = Apriori.findAllUniqueFreqSets(data,
//				.6);
//		Assert.assertEquals(1, freqItemSets2.size());
//	}
//
//	@Test
//	public void zlectureNotes1() {
//		String[][] d = { { "1", "3", "4" }, { "2", "3", "5" },
//				{ "1", "2", "3", "5" }, { "2", "5" } };
//
//		List<ApriorModelData> data = toModel(d);
//		SortedSet<ItemSet> freqItemSets = Apriori.findAllFreqSets(data, .4);
//		Assert.assertEquals(9, freqItemSets.size());
//
//		SortedSet<ItemSet> freqItemSets2 = Apriori.findAllUniqueFreqSets(data,
//				.4);
//		Assert.assertEquals(2, freqItemSets2.size());
//	}
//
//	@Test
//	public void ztestForFaq1() {
//		String[][] d = { { "K", "A", "D", "B" }, { "D", "A", "C", "E", "B" },
//				{ "C", "A", "B", "E" }, { "B", "A", "D" } };
//
//		List<ApriorModelData> data = toModel(d);
//
//		Set<ItemSet> candidates = Apriori.generateC1(data);
//		Assert.assertEquals(6, candidates.size());
//		SortedSet<ItemSet> freqSeqs = Apriori.generateLn(data, candidates, 0.6);
//		Assert.assertEquals(3, freqSeqs.size());
//
//		Set<ItemSet> candidates2 = Apriori.generateCn(2, freqSeqs);
//		Assert.assertEquals(3, candidates2.size());
//		SortedSet<ItemSet> freqSeqs2 = Apriori.generateLn(data, candidates2,
//				0.6);
//		Assert.assertEquals(3, freqSeqs2.size());
//
//		Set<ItemSet> candidates3 = Apriori.generateCn(3, freqSeqs2);
//		Assert.assertEquals(1, candidates3.size());
//		SortedSet<ItemSet> freqSeqs3 = Apriori.generateLn(data, candidates3,
//				0.6);
//		Assert.assertEquals(1, freqSeqs3.size());
//	}
//
//	@Test
//	public void ztestForLectureNotes1() {
//		String[][] d = { { "1", "3", "4" }, { "2", "3", "5" },
//				{ "1", "2", "3", "5" }, { "2", "5" } };
//
//		List<ApriorModelData> data = toModel(d);
//
//		Set<ItemSet> candidates = Apriori.generateC1(data);
//		Assert.assertEquals(5, candidates.size());
//		SortedSet<ItemSet> freqSeqs = Apriori.generateLn(data, candidates, 0.4);
//		Assert.assertEquals(4, freqSeqs.size());
//
//		Set<ItemSet> candidates2 = Apriori.generateCn(2, freqSeqs);
//		Assert.assertEquals(6, candidates2.size());
//		SortedSet<ItemSet> freqSeqs2 = Apriori.generateLn(data, candidates2,
//				0.4);
//		Assert.assertEquals(4, freqSeqs2.size());
//
//		Set<ItemSet> candidates3 = Apriori.generateCn(3, freqSeqs2);
//		Assert.assertEquals(1, candidates3.size());
//		SortedSet<ItemSet> freqSeqs3 = Apriori.generateLn(data, candidates3,
//				0.4);
//		Assert.assertEquals(1, freqSeqs3.size());
//	}
}