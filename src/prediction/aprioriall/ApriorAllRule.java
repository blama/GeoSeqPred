package prediction.aprioriall;

import java.text.NumberFormat;

public class ApriorAllRule {
    private ApriorPrecision apriorPrecision;
    private Sequence conditionSeq;
    private Sequence consequenceSeq;

    public ApriorPrecision getApriorPrecision() {
        return apriorPrecision;
    }

    public Sequence getConditionSeq() {
        return conditionSeq;
    }

    public Sequence getConsequenceSeq() {
        return consequenceSeq;
    }

    public void setApriorPrecision(ApriorPrecision apriorPrecision) {
        this.apriorPrecision = apriorPrecision;
    }

    public void setConditionSeq(Sequence conditionSeq) {
        this.conditionSeq = conditionSeq;
    }

    public void setConsequenceSeq(Sequence consequenceSeq) {
        this.consequenceSeq = consequenceSeq;
    }

    public Integer getMapKey() {
        return conditionSeq.size() + consequenceSeq.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
//        builder.append("Sup\tConf\tRule\n");
        sb.append(getConditionSeq());
        sb.append(" => ");
        sb.append(getConsequenceSeq());
        NumberFormat nf = NumberFormat.getPercentInstance();

        sb.append("\t" + nf.format(getApriorPrecision()
                .getMinSup())
                + "\t"
                + nf.format(getApriorPrecision().getMinConf())
                + "\t");
        return sb.toString();
    }
}