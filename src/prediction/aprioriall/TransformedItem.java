package prediction.aprioriall;

import java.util.TreeSet;

public class TransformedItem extends TreeSet<Item> implements Item {

    private static final long serialVersionUID = 2089369446057609284L;

    private int id;
    private String location_id;

    public TransformedItem(int id, ItemSet itemSet, String location_id) {
        super();
        this.id = id;
        addAll(itemSet);
        this.location_id = location_id;
    }

    @Override
    public int compareTo(Item o) {
        TransformedItem other = (TransformedItem) o;
        return id - other.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TransformedItem other = (TransformedItem) obj;
        return id == other.id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getItemId() {
        return location_id;
    }

    @Override
    public void setItemId(String itemId) {
        this.location_id = itemId;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("(");
        boolean appendSpace = false;
        for (Item i : this) {
            if (appendSpace) {
                sb.append(" ");
            }
            sb.append(i);
            appendSpace = true;
        }
        sb.append(")");
        return sb.toString();
    }
}
