package prediction.aprioriall;

import java.util.SortedSet;
import java.util.TreeSet;

public class ApriorModelData {
    private String id;

    private SortedSet<Item> items = new TreeSet<Item>();

    public ApriorModelData() {
        super();
    }

    public ApriorModelData(SortedSet<Item> items) {
        super();
        this.items = items;
    }

    public ApriorModelData(String id, SortedSet<Item> items) {
        super();
        this.id = id;
        this.items = items;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApriorModelData other = (ApriorModelData) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (items == null) {
            if (other.items != null)
                return false;
        } else if (!items.equals(other.items))
            return false;
        return true;
    }

    public String getId() {
        return id;
    }

    public SortedSet<Item> getItems() {
        return items;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((items == null) ? 0 : items.hashCode());
        return result;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setItems(SortedSet<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "ApriorModelData [id=" + id + ", items=" + items + "]";
    }
}
