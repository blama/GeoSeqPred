package prediction.aprioriall;

public abstract class AbstractItem implements Item {
    private String itemId;

    public AbstractItem(String itemId) {
        super();
        this.itemId = itemId;
    }

    @Override
    public int compareTo(Item o) {
        AbstractItem other = (AbstractItem) o;
        return itemId.compareTo(other.itemId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractItem other = (AbstractItem) obj;
        return itemId.equals(other.itemId);
    }

    @Override
    public String getItemId() {
        return itemId;
    }

    @Override
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    @Override
    public int hashCode() {
        return ((itemId == null) ? 0 : itemId.hashCode());
    }

    @Override
    public String toString() {
        return itemId;
    }
}
