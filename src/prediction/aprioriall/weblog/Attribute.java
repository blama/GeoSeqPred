package prediction.aprioriall.weblog;

import prediction.aprioriall.AbstractItem;

public class Attribute extends AbstractItem {

    private String title;
    private String url;

    public Attribute(String attributeId, String title, String url) {
        super(attributeId);
        this.title = title;
        this.url = url;
    }

    public String getAttributeId() {
        return getItemId();
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}