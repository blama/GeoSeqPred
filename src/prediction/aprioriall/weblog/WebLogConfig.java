package prediction.aprioriall.weblog;

import prediction.aprioriall.ApriorPrecision;

import java.io.File;


public class WebLogConfig {
    private ApriorPrecision config;
    private File dataSource;

    public ApriorPrecision getConfig() {
        return config;
    }

    public File getDataSource() {
        return dataSource;
    }

    public void setConfig(ApriorPrecision config) {
        this.config = config;
    }

    public void setDataSource(File dataSource) {
        this.dataSource = dataSource;
    }
}