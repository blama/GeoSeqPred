//package prediction.aprioriall.weblog;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.text.NumberFormat;
//import java.util.List;
//import java.util.SortedSet;
//
//import dm.core.ApriorAllModelData;
//import dm.core.ApriorAllRule;
//import dm.core.ApriorPrecision;
//import dm.core.AprioriAll;
//import dm.core.Sequence;
//
//public class WebLogMiner {
//
//	public static void main(String[] args) throws IOException {
//		while (true) {
//			System.out.println("**Welcome to WebLog Miner***");
//			BufferedReader in = new BufferedReader(new InputStreamReader(
//					System.in));
//
//			WebLogConfig config = new WebLogConfig();
//			config.setConfig(new ApriorPrecision());
//
//			while (true) {
//				System.out.println("DataSource ? (1 DATA/2 test): ");
//				String s = in.readLine();
//				String input = s.trim();
//				if (input.isEmpty() || "1".equals(input)) {
//					config.setDataSource(DataLoader.DATA);
//					break;
//				} else if ("2".equals(input)) {
//					config.setDataSource(DataLoader.TEST);
//					break;
//				}
//			}
//
//			while (true) {
//				System.out.println("Minimum support? (0.1): ");
//				String s = in.readLine();
//				String input = s.trim();
//
//				if (input.isEmpty()) {
//					config.getConfig().setMinSup(0.1d);
//					break;
//				}
//
//				try {
//					double minSup = Double.parseDouble(input);
//					if (minSup > 0 && minSup <= 1) {
//						config.getConfig().setMinSup(minSup);
//						break;
//					}
//				} catch (NumberFormatException e) {
//				}
//			}
//
//			while (true) {
//				System.out.println("Minimum confident? (0.3): ");
//				String s = in.readLine();
//				String input = s.trim();
//
//				if (input.isEmpty()) {
//					config.getConfig().setMinSup(0.3d);
//					break;
//				}
//				try {
//					double minConf = Double.parseDouble(input);
//					if (minConf > 0 && minConf <= 1) {
//						config.getConfig().setMinConf(minConf);
//						break;
//					}
//				} catch (NumberFormatException e) {
//				}
//			}
//			System.out.println("Mining...");
//			System.out.println("==================");
//
//			List<ApriorAllModelData> data = DataLoader.load(config
//					.getDataSource());
//			SortedSet<Sequence> freqSeqs = AprioriAll.findAllFreqSequences(
//					data, config.getConfig().getMinSup());
//
//			System.out.println("Frequent Sequences:");
//			for (Sequence s : freqSeqs) {
//				System.out.println(s);
//			}
//
//			List<ApriorAllRule> rules = AprioriAll.findAllFreqAssoRule(data,
//					config.getConfig(), freqSeqs);
//
//			System.out.println("==================");
//
//			System.out.println("Assoication Rules:");
//			if (rules.size() == 0) {
//				System.out.println("No Rules!");
//			} else {
//				System.out.println("Sup\tConf\tRule");
//				for (ApriorAllRule r : rules) {
//					StringBuilder sb = new StringBuilder();
//					sb.append(r.getConditionSeq());
//					sb.append(" => ");
//					sb.append(r.getConsequenceSeq());
//
//					NumberFormat nf = NumberFormat.getPercentInstance();
//
//					System.out.println(nf.format(r.getApriorPrecision()
//							.getMinSup())
//							+ "\t"
//							+ nf.format(r.getApriorPrecision().getMinConf())
//							+ "\t" + sb);
//				}
//			}
//
//			System.out.println("==================");
//			System.out.println();
//		}
//	}
//}
