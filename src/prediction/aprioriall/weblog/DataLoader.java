//package prediction.aprioriall.weblog;
//
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import prediction.aprioriall.*;
//
//
//import org.junit.Test;
//
//public class DataLoader {
//
//	public final static File DATA = new File("data/anonymous-msweb.data");
//	public final static File TEST = new File("data/anonymous-msweb.test");
//
//	public static List<ApriorAllModelData> load(File file) throws IOException {
//		Map<String, Attribute> attributeMap = new HashMap<String, Attribute>();
//
//		List<ApriorAllModelData> ret = new ArrayList<ApriorAllModelData>();
//
//		CSVReader reader = new CSVReader(new FileReader(file));
//		String[] nextLine;
//		ApriorAllModelData currentModel = null;
//		while ((nextLine = reader.readNext()) != null) {
//			if (nextLine.length > 0) {
//				if (nextLine[0].equals("A")) {
//					if (nextLine.length >= 5) {
//						Attribute attribute = new Attribute(nextLine[1],
//								nextLine[3], nextLine[4]);
//						attributeMap.put(attribute.getAttributeId(), attribute);
//					}
//				} else if (nextLine[0].equals("C")) {
//					if (nextLine.length >= 3) {
//						if (currentModel != null) {
//							ret.add(currentModel);
//						}
//						currentModel = new ApriorAllModelData(nextLine[1],
//								new Sequence());
//					}
//				} else if (nextLine[0].equals("V")) {
//					Attribute a = attributeMap.get(nextLine[1]);
//					if (a != null) {
//						ItemSet itemSet = new ItemSet();
//						itemSet.add(a);
//						currentModel.getSequence().add(itemSet);
//					}
//				}
//			}
//		}
//
//		if (currentModel != null) {
//			ret.add(currentModel);
//		}
//
//		return ret;
//	}
//
//	@Test
//	public void data() throws IOException {
//		List<ApriorAllModelData> data = DataLoader.load(DATA);
//	}
//
//	@Test
//	public void test() throws IOException {
//		List<ApriorAllModelData> data = DataLoader.load(TEST);
//	}
//}