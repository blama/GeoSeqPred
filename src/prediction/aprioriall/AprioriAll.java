package prediction.aprioriall;

import prediction.aprioriall.weblog.Attribute;

import java.util.*;

/**
 * Main AprioriAll algorithm processing class
 *
 * @author Kelvin
 */
public class AprioriAll {

    public static int countSup(List<Sequence> data, Sequence seq) {
        int count = 0;

        for (Sequence sequence : data) {
            if (sequence.containsSequence(seq)) {
                count++;
            }
        }

        return count;
    }

    public static SortedSet<Sequence> filterNonConfident(int dataSize, SortedSet<Sequence> sequences, double minConf) {
        SortedSet<Sequence> ret = new TreeSet<Sequence>();
        for (Sequence seq : sequences) {

        }
        return ret;
    }

    public static List<ApriorAllRule> findAllFreqAssoRule(List<ApriorAllModelData> data, ApriorPrecision precision) {

        SortedSet<Sequence> allFreqSet = findAllFreqSequences(data, precision.getMinSup());

        return findAllFreqAssoRule(data, precision, allFreqSet);
    }

    public static List<ApriorAllRule> findAllFreqAssoRule(List<ApriorAllModelData> data, ApriorPrecision precision, SortedSet<Sequence> allFreqSeq) {
        List<ApriorAllRule> ret = new ArrayList<ApriorAllRule>();

        Map<Sequence, Integer> supportMap = new HashMap<Sequence, Integer>();

        SortedSet<ItemSet> freqSets = findAllFreqSets(data, precision.getMinSup());
        List<Sequence> sequences = transform(data, freqSets);

        for (Sequence freqSeq : allFreqSeq) {
            //TODO: REZA << mistake for sub seq creation, no length two rule >>
            if (freqSeq.size() > 1) {
/*                for (int i = 1; i < freqSeq.size() - 1; i++) {
                    Sequence conditionSeq = new Sequence();
                    Sequence consequenceSeq = new Sequence();

                    for (int j = 0; j < i; j++) {
                        conditionSeq.add(freqSeq.get(j));
                    }
                    for (int j = i; j < freqSeq.size(); j++) {
                        consequenceSeq.add(freqSeq.get(j));
                    }*/
                for (int i = 1; i < freqSeq.size(); i++) {
                    Sequence conditionSeq = new Sequence();
                    Sequence consequenceSeq = new Sequence();
                    conditionSeq.addAll(freqSeq.subList(0, i));
                    consequenceSeq.addAll(freqSeq.subList(i, freqSeq.size()));

                    Integer conditionSup = supportMap.get(conditionSeq);
                    if (conditionSup == null) {
                        conditionSup = countSup(sequences, conditionSeq);
                        supportMap.put(conditionSeq, conditionSup);
                    }

                    Integer freqSeqSup = supportMap.get(freqSeq);
                    if (freqSeqSup == null) {
                        freqSeqSup = countSup(sequences, freqSeq);
                        supportMap.put(freqSeq, freqSeqSup);
                        //					supportMap.put(freqSeq, conditionSup);
                    }

                    if ((double) freqSeqSup.intValue() / conditionSup.intValue() >= precision
                            .getMinConf()) {
                        ApriorAllRule rule = new ApriorAllRule();
                        rule.setConditionSeq(conditionSeq);
                        rule.setConsequenceSeq(consequenceSeq);

                        ApriorPrecision p = new ApriorPrecision();

                        p.setMinSup((double) conditionSup / data.size());
                        p.setMinConf((double) freqSeqSup / conditionSup);
                        rule.setApriorPrecision(p);

                        ret.add(rule);
                    }
                }
            }
        }

        return ret;
    }

    public static SortedSet<Sequence> findAllFreqSequences(
            List<ApriorAllModelData> data, double minSup) {

        SortedSet<ItemSet> freqSets = findAllFreqSets(data, minSup);

        List<Sequence> sequences = transform(data, freqSets);

        Set<Sequence> candidates = generateC1(sequences);
        SortedSet<Sequence> lastRoundFreqItemSet = generateLn(data.size(),
                sequences, candidates, minSup);
        SortedSet<Sequence> freqItemSets = lastRoundFreqItemSet;

        int maxLengthFreqSeq = maxLengthFreqSeq(data);

        for (int i = 2; i <= maxLengthFreqSeq; i++) {
            candidates = generateCn(i, lastRoundFreqItemSet);

            lastRoundFreqItemSet = generateLn(data.size(), sequences,
                    candidates, minSup);

            if (lastRoundFreqItemSet.size() == 0) {
                break;
            }

            freqItemSets.addAll(lastRoundFreqItemSet);
        }

        return freqItemSets;
    }

    public static SortedSet<ItemSet> findAllFreqSets(
            List<ApriorAllModelData> data, double minSup) {
        List<ApriorModelData> apriorModelDataList = new ArrayList<ApriorModelData>();
        for (ApriorAllModelData d : data) {
            for (ItemSet is : d.getSequence()) {
                ApriorModelData apriorModelData = new ApriorModelData();
                apriorModelData.setItems(is);
                apriorModelDataList.add(apriorModelData);
            }
        }

        SortedSet<ItemSet> ret = Apriori.findAllFreqSets(apriorModelDataList,
                minSup * data.size() / apriorModelDataList.size());

        return ret;
    }

    private static Set<Sequence> generateC1(List<Sequence> data) {

        Set<Sequence> ret = new HashSet<Sequence>();

        for (Sequence seq : data) {
            for (ItemSet itemset : seq) {
                for (Item item : itemset) {
                    Sequence s = new Sequence();
                    ItemSet is = new ItemSet();
                    is.add(item);
                    s.add(is);
                    ret.add(s);
                }
            }
        }

        return ret;
    }

    private static Set<Sequence> generateCn(int n,
                                            SortedSet<Sequence> lastFreqSeqs) {

        Set<Sequence> ret = new HashSet<Sequence>();

        for (Sequence seq : lastFreqSeqs) {
            label:
            for (Sequence seq2 : lastFreqSeqs) {
                Iterator<ItemSet> seqIter = seq.iterator();
                Iterator<ItemSet> seqIter2 = seq2.iterator();
                for (int j = 0; j < n - 2; j++) {
                    if (!seqIter.next().equals(seqIter2.next())) {
                        continue label;
                    }
                }

                Sequence newSeq = new Sequence();
                newSeq.addAll(seq);
                newSeq.add(seqIter2.next());
                ret.add(newSeq);
            }
        }

        return ret;
    }

    private static SortedSet<Sequence> generateLn(int dataSize,
                                                  List<Sequence> data, Set<Sequence> candidates, double minSup) {

        SortedSet<Sequence> ret = new TreeSet<Sequence>();
        double minSupCount = dataSize * minSup;

        for (Sequence seq : candidates) {
            if (isSignificant(data, seq, minSupCount)) {
                ret.add(seq);
            }
        }

        return ret;
    }

    public static boolean isSignificant(List<Sequence> data, Sequence seq,
                                        double sig) {
        int count = 0;

        for (Sequence sequence : data) {
            if (sequence.containsSequence(seq)) {
                count++;
                if (count >= sig) {
                    return true;
                }
            }
        }

        return false;
    }

    private static int maxLengthFreqSeq(List<ApriorAllModelData> data) {
        int maxLengthFreqSeq = 0;

        for (ApriorAllModelData d : data) {
            int size = d.getSequence().size();
            if (size > maxLengthFreqSeq) {
                maxLengthFreqSeq = size;
            }
        }

        return maxLengthFreqSeq;
    }

    private static Sequence transform(ApriorAllModelData data,
                                      Map<ItemSet, TransformedItem> transformedItemMap) {

        Sequence ret = new Sequence();
        for (ItemSet is : data.getSequence()) {
            ItemSet toBeAdded = new ItemSet();
            for (ItemSet freqItemSet : transformedItemMap.keySet()) {
                if (is.containsAll(freqItemSet)) {
                    toBeAdded.add(transformedItemMap.get(freqItemSet));
                }
            }
            if (!toBeAdded.isEmpty()) {
                ret.add(toBeAdded);
            }
        }

        if (ret.isEmpty()) {
            return null;
        } else {
            return ret;
        }
    }

    public static List<Sequence> transform(List<ApriorAllModelData> data,
                                           SortedSet<ItemSet> freqItemSets) {

        Map<ItemSet, TransformedItem> transformedItemMap = new HashMap<ItemSet, TransformedItem>();

        int i = 0;
        for (ItemSet itemSet : freqItemSets) {
            TransformedItem ti = new TransformedItem(i++, itemSet, itemSet.first().getItemId());
            transformedItemMap.put(itemSet, ti);
        }

        List<Sequence> ret = new ArrayList<Sequence>();

        for (ApriorAllModelData md : data) {
            Sequence transformed = transform(md, transformedItemMap);
            if (transformed != null) {
                ret.add(transformed);
            }
        }

        return ret;
    }

    public List<ApriorAllModelData> toModel(String[][][] d) {
        List<ApriorAllModelData> data = new ArrayList<ApriorAllModelData>();

        for (String[][] _2dArr : d) {
            Sequence seq = new Sequence();

            for (String[] arr : _2dArr) {
                ItemSet itemSet = new ItemSet();
                for (String str : arr) {
                    Item item = new Attribute(str, null, null);
                    itemSet.add(item);
                }
                seq.add(itemSet);
            }

            data.add(new ApriorAllModelData("", seq));
        }

        return data;
    }

    public void zfindAllFreqSets() {
        String[][][] d = { { { "30" }, { "90" } },
                { { "10", "20" }, { "30" }, { "40", "60", "70" } },
                { { "30", "50", "70" } },
                { { "30" }, { "40", "70" }, { "90" } }, { { "90" } } };
        String[][][] dp = {{{"30","300","3000"}, {"90"}},
                {{"10", "20"}, {"30","300","3000"}, {"40","400","4000", "60", "70"}},
                {{"30","300","3000", "50", "70"}},
                {{"30","300","3000"}, {"40","400","4000", "70"}, {"90"}}, {{"90"}}};
        List<ApriorAllModelData> data = toModel(dp);
        SortedSet<ItemSet> freqSets = findAllFreqSets(data, 0.25);
        System.out.println("frequent sets..., sup:" + 0.25);
        freqSets.forEach(a-> System.out.println(a));
        List<Sequence> sequences = transform(data, freqSets);
        System.out.println("seq");
        sequences.forEach(a -> System.out.println(a));
        SortedSet<Sequence> freqSeq = findAllFreqSequences(data, 0.25);
        System.out.println("freq");
        freqSeq.forEach(a -> System.out.println(a));
        List<ApriorAllRule> rules = findAllFreqAssoRule(data, new ApriorPrecision(0.1 , 0.25));
        System.out.println("rules:/");
        rules.forEach(a-> System.out.println(a));
        /*String[][][] dp = {{{"30","300","3000"}, {"90"}},
                {{"10", "20"}, {"30","300","3000"}, {"40","400","4000", "60", "70"}},
                {{"30","300","3000", "50", "70"}},
                {{"30","300","3000"}, {"40","400","4000", "70"}, {"90"}}, {{"90"}}};
        *//*String[][][] d = {{{"30"}, {"90"}},
                {{"10", "20"}, {"30"}, {"40", "60", "70"}},
                {{"30", "50", "70"}},
                {{"30"}, {"40", "70"}, {"90"}}, {{"90"}}};*//*

        List<ApriorAllModelData> data = toModel(dp);
        double minSup = 0.25;
        double minConf = 0.4;
        SortedSet<ItemSet> freqSets = findAllFreqSets(data, minSup);
        System.out.println("frequent sets..., sup:" + minSup);
        freqSets.forEach(a-> System.out.println(a));
//		Assert.assertEquals(5, freqSets.size());

        List<Sequence> sequences = transform(data, freqSets);
        SortedSet<Sequence> freqSeq = findAllFreqSequences(data, minSup);
        System.out.println("sequences..." + minSup);
        sequences.forEach(a -> System.out.println(a));
        System.out.println("++++++++++++++");
        System.out.println("frequent sequences..., sup:" + minSup);
        freqSeq.forEach(a -> System.out.println(a));
        List<ApriorAllRule> rules = findAllFreqAssoRule(data, new ApriorPrecision(minConf , minSup));
        System.out.println("rules:/");
        rules.forEach(a-> System.out.println(a));*/

    }

    public static void main(String[] args) {
        AprioriAll all = new AprioriAll();
        all.zfindAllFreqSets();
    }
}