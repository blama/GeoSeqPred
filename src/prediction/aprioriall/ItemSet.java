package prediction.aprioriall;

import java.util.Iterator;
import java.util.TreeSet;

public class ItemSet extends TreeSet<Item> implements Comparable<ItemSet> {
    private static final long serialVersionUID = -2578635808960086063L;

    @Override
    public int compareTo(ItemSet o) {
        Iterator<Item> thisIter = iterator();
        Iterator<Item> anotherIter = o.iterator();

        while (thisIter.hasNext()) {
            if (!anotherIter.hasNext()) {
                return 1;
            }

            Item i1 = thisIter.next();
            Item i2 = anotherIter.next();

            int diff = i1.compareTo(i2);
            if (diff != 0) {
                return diff;
            }
        }

        if (anotherIter.hasNext()) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        boolean appendSpace = false;
        for (Item i : this) {
            if (appendSpace) {
                sb.append(",");
            }
            sb.append(i);
            appendSpace = true;
        }
        sb.append("]");
        return sb.toString();
    }
}
