package prediction.aprioriall;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Sequence extends ArrayList<ItemSet> implements Comparable<Sequence> {

    @Override
    public int compareTo(Sequence o) {
        Iterator<ItemSet> thisIter = iterator();
        Iterator<ItemSet> anotherIter = o.iterator();

        while (thisIter.hasNext()) {
            if (!anotherIter.hasNext()) {
                return 1;
            }

            ItemSet i1 = thisIter.next();
            ItemSet i2 = anotherIter.next();

            int diff = i1.compareTo(i2);
            if (diff != 0) {
                return diff;
            }
        }

        if (anotherIter.hasNext()) {
            return -1;
        } else {
            return 0;
        }
    }

    public boolean containsSequence(Sequence subSeq) {
        assert subSeq != null;
        if (this.size() < subSeq.size()) {
            return false;
        }
        int n = this.size();
        int m = subSeq.size();
        for (int i = 0; i < n - m + 1; i++) {
            if (isSameSequence(this.subList(i, i + m), subSeq)) {
                return true;
            }
        }
        return false;
    }

    private boolean isSameSequence(List<ItemSet> itemSets, Sequence subSeq) {
        Iterator<ItemSet> it1 = itemSets.iterator();
        Iterator<ItemSet> it2 = subSeq.iterator();
        while (it1.hasNext()) {
            if (!it1.next().iterator().next().equals(it2.next().iterator().next())) {
                return false;
            }
        }
        return true;
    }


	/*public boolean containsSequence(Sequence subSeq) {
        assert subSeq != null;

		Iterator<ItemSet> itemSetIter = subSeq.iterator();
		Item targetItem = itemSetIter.next().iterator().next();

		for (ItemSet itemSet : this) {
			for (Item item : itemSet) {
				if (item.equals(targetItem)) {
					if (itemSetIter.hasNext()) {
						targetItem = itemSetIter.next().iterator().next();
						break;
					} else {
						return true;
					}
				} else if (item.compareTo(targetItem) > 0) {
					break;
				}
			}
		}

		return false;

	}*/

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("<");
        boolean appendSpace = false;
        for (ItemSet is : this) {
            if (appendSpace) {
                sb.append(" ");
            }
            sb.append(is);
            appendSpace = true;
        }
        sb.append(">");
        return sb.toString();
    }
}
