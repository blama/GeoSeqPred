package prediction.simpleapriori;

/**
 * Created by sce on 8/23/2015.
 */
public interface Item {
    String getItemId();
}
