package prediction.simpleapriori;

import java.util.List;

public class UserLocation implements Item {

    public double east;
    public double north;
    public double diameter;
    public int location_id;
    public String username;
    public double enter_time;
    public String enter_time_string;
    public double leave_time;
    public String leave_time_string;
    public List<Integer> levelIds;

    public UserLocation(double east, double north, double diameter, int location_id, String username, double enter_time,
                        String enter_time_string, double leave_time, String leave_time_string, List<Integer> levelIds) {
        this.east = east;
        this.north = north;
        this.diameter = diameter;
        this.location_id = location_id;
        this.username = username;
        this.enter_time = enter_time;
        this.enter_time_string = enter_time_string;
        this.leave_time = leave_time;
        this.leave_time_string = leave_time_string;
        this.levelIds = levelIds;
    }

    @Override
    public String getItemId() {
        return location_id + "";
    }
}