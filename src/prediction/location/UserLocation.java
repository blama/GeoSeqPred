package prediction.location;

import datapreparation.StayPoint;
import prediction.aprioriall.AbstractItem;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Description of an item in the item set and sequences
 */
public class UserLocation extends AbstractItem {

    public double east;
    public double north;
    public double diameter;
    public int location_id;
    public String username;
    public double enter_time;
    public String enter_time_string;
    public double leave_time;
    public String leave_time_string;
    public List<Integer> levelIds;

    public UserLocation(int location_id, double east, double north, double diameter, String username, double enter_time, double leave_time) {
        super(location_id + "");
        this.east = east;
        this.north = north;
        this.diameter = diameter;
        this.location_id = location_id;
        this.username = username;
        this.enter_time = enter_time;
        this.leave_time = leave_time;
    }

    public UserLocation(int location_id, double enter_time, double leave_time) {
        super(location_id + "");
        this.location_id = location_id;
        this.enter_time = enter_time;
        this.leave_time = leave_time;
    }

    public UserLocation(StayPoint sp, String username) {
        super(sp.clusterID + "");
        this.east = sp.east;
        this.north = sp.north;
        this.diameter = sp.diameter;
        this.location_id = sp.clusterID;
        this.enter_time = sp.enterTime;
        this.leave_time = sp.leaveTime;
        this.levelIds = sp.levelIds;
        StringTokenizer st = new StringTokenizer(sp.timeString, ",");
        this.enter_time_string = st.nextToken() + "," + st.nextToken();
        this.leave_time_string = st.nextToken() + "," + st.nextToken();
        this.username = username;
    }

    public UserLocation(UserLocation ul, Integer location_id) {
        super(location_id + "");
        this.location_id = location_id;
        this.east = ul.east;
        this.north = ul.north;
        this.diameter = ul.diameter;
        this.enter_time = ul.enter_time;
        this.leave_time = ul.leave_time;
        this.levelIds = ul.levelIds;
        this.enter_time_string = ul.enter_time_string;
        this.leave_time_string = ul.leave_time_string;
        this.username = ul.username;
    }

    @Override
    public String getItemId() {
        return location_id + "";
    }

    @Override
    public void setItemId(String itemId) {
        super.setItemId(itemId);
    }

    @Override
    public String toString() {
        return "" + location_id;
    }// + ", enter_tm:"+enter_time+ ", leave_tm:"+leave_time

    public static double distance(UserLocation u1, UserLocation u2) {
        return Math.sqrt(Math.pow(u1.east - u2.east, 2) + Math.pow(u1.north - u2.north, 2));
    }
}
