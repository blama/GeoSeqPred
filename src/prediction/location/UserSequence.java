package prediction.location;

import prediction.aprioriall.Sequence;

public class UserSequence extends Sequence {
    public String username;
    public int id;

    public UserSequence(String username, int id) {
        this.username = username;
        this.id = id;
    }

    public UserSequence() {

    }

    @Override
    public String toString() {
        return /*username + "," + size() +":\n" +*/ super.toString();
    }
}
