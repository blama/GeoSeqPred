package clustering;

public interface Euclidean<E> {
    public E add(E t1, E t2);

    public E addSqr(E sumSqr, E entry);

    public double getDistance(E e1, E e2);

    public E divide(E sum, int size);
}
