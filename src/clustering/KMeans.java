package clustering;

import datapreparation.Location;
import datapreparation.StayPoint;

import java.util.*;

public class KMeans {
    public static List<Location> initiateClusters(List<StayPoint> dataPoints, int k) throws Exception {
        if (k > dataPoints.size()) {
            throw new Exception("k must be smaller than points");
        }
        //making a random sample of size k==count
        Random r = new Random();
        Set<Integer> sampledIndices = new HashSet<Integer>();
        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            int random_index = r.nextInt(dataPoints.size());
            while (true) {
                if (sampledIndices.contains(random_index)) {
                    random_index = r.nextInt(dataPoints.size());
                } else {
                    sampledIndices.add(random_index);
                    break;
                }
            }
        }
        for (Integer i : sampledIndices) {
            List<StayPoint> spz = new ArrayList<>();
            spz.add(dataPoints.get(i));
            locations.add(new Location(spz));
        }
        for (Integer i : sampledIndices) {
            dataPoints.remove(i);
        }
        for (StayPoint p : dataPoints) {
            double min = Double.MAX_VALUE;
            Location minLoc = new Location();
            for (Location l : locations) {
                if (!l.isEmpty() && distance(l, p) < min) {
                    min = distance(l, p);
                    minLoc = l;
                }
            }
            minLoc.pts.add(p);
        }
        for (Location l : locations) {
            l.evaluateParameters();
        }
        return locations;
    }

    public static void updateClusters(List<Location> locations, int iterations) {
        for (int i = 0; i < iterations; i++) {
            for (Location lout : locations) {
                for (StayPoint p : lout.pts) {
                    double min = Double.MAX_VALUE;
                    Location minLoc = new Location();
                    for (Location lin : locations) {
                        if (!lin.isEmpty() && distance(lin, p) < min) {
                            min = distance(lin, p);
                            minLoc = lin;
                        }
                    }
                    minLoc.pts.add(p);
                    lout.pts.remove(p);
                }
            }
            for (Location l : locations) {
                l.evaluateParameters();
            }
        }
    }

    private static double distance(Location l, StayPoint p) {
        return Math.sqrt(Math.pow((l.easting - p.east), 2) + Math.pow((l.northing - p.north), 2));
    }
}
