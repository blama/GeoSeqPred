package clustering;

import datapreparation.StayPoint;
import utility.ArrayIndexSort;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public void runBFR(BFR bfr) throws Exception {

        String s = "<html>\n" +
                "<body>\n" +
                "\n" +
                "<h1 align=\"right\">?????</h1>\n" +
                "<h3 align=\"right\">\n" +
                "<li><a href=\"/wiki/%D9%BE%D8%B1%D9%88%DB%8C%D8%B2_%D9%85%D8%B4%DA%A9%D8%A7%D8%AA%DB%8C%D8%A7%D9%86\" title=\"????? ????????\">????? ????????</a>: <a href=\"/wiki/%D8%B3%D9%86%D8%AA%D9%88%D8%B1\" title=\"?????\">?????</a></li>\n" +
                "<li><a href=\"/wiki/%D9%85%D8%AD%D9%85%D8%AF%D8%B1%D8%B6%D8%A7_%D9%84%D8%B7%D9%81%DB%8C\" title=\"??????? ????\">??????? ????</a>: <a href=\"/wiki/%D8%AA%D8%A7%D8%B1\" title=\"???\">???</a></li>\n" +
                "<li><a href=\"/wiki/%D9%86%D8%A7%D8%B5%D8%B1_%D9%81%D8%B1%D9%87%D9%86%DA%AF_%D9%81%D8%B1\" title=\"???? ????? ??\" class=\"mw-redirect\">???? ????? ??</a>: <a href=\"/wiki/%D8%AA%D9%85%D8%A8%DA%A9\" title=\"????\" class=\"mw-redirect\">????</a></li>\n" +
                "<li><a href=\"/wiki/%D8%A7%D8%B1%D8%B3%D9%84%D8%A7%D9%86_%DA%A9%D8%A7%D9%85%DA%A9%D8%A7%D8%B1\" title=\"?????? ??????\">?????? ??????</a>: <a href=\"/wiki/%D8%A8%D8%B1%D8%A8%D8%B7\" title=\"????\">????</a></li>\n" +
                "<li><a href=\"/wiki/%D8%A7%D8%B1%D8%AF%D8%B4%DB%8C%D8%B1_%DA%A9%D8%A7%D9%85%DA%A9%D8%A7%D8%B1\" title=\"?????? ??????\">?????? ??????</a>: <a href=\"/wiki/%DA%A9%D9%85%D8%A7%D9%86%DA%86%D9%87\" title=\"??????\">??????</a></li>\n" +
                "<li><a href=\"/wiki/%D8%AC%D9%85%D8%B4%DB%8C%D8%AF_%D8%B9%D9%86%D8%AF%D9%84%DB%8C%D8%A8%DB%8C\" title=\"????? ???????\">????? ???????</a>: <a href=\"/wiki/%D9%86%DB%8C_(%D8%A7%D8%A8%D8%B2%D8%A7%D8%B1_%D9%85%D9%88%D8%B3%DB%8C%D9%82%DB%8C)\" title=\"?? (????? ??????)\" class=\"mw-redirect\">??</a></li>\n" +
                "<li><a href=\"/wiki/%D8%B2%DB%8C%D8%AF%D8%A7%D9%84%D9%84%D9%87_%D8%B7%D9%84%D9%88%D8%B9%DB%8C\" title=\"??????? ?????\">??????? ?????</a>: <a href=\"/wiki/%D8%AA%D8%A7%D8%B1\" title=\"???\">???</a></li>\n" +
                "<li><a href=\"/wiki/%D9%81%D8%B1%D8%AE_%D9%85%D8%B8%D9%87%D8%B1%DB%8C\" title=\"??? ?????\">??? ?????</a>: <a href=\"/wiki/%D8%A8%D9%85%E2%80%8C%D8%AA%D8%A7%D8%B1\" title=\"??\u200C???\">??\u200C???</a></li>\n" +
                "<li><i>?????? ????: ????? ????????</i></li>\n" +
                "</h3>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        double time = System.currentTimeMillis();
        System.out.println("taking sample");
        bfr.sampleGen(0.1);
        System.out.println("clustering sample");
//        bfr.entries = allStayPoints;
        bfr.clusterSamples(BFR.K_MEANS, 300);
        System.out.println("adding points");
        for (StayPoint sp : (List<StayPoint>) bfr.entries) {
            bfr.addEntry(sp);
        }
        System.out.println("time: " + (System.currentTimeMillis() - time));
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            if (i==5){
                i++;
            }

        }
//        File f = new File("F:\\reza\\IdeaProjects\\GeoLife\\stayPoints+loc(timeString)");
//
//        if (f.exists() && f.isDirectory()){
//            for (File file:f.listFiles()){
//                if (file.getName().contains(",")){
//                    file.renameTo(new File("F:\\reza\\IdeaProjects\\GeoLife\\stayPoints+loc(timeString)\\"+file.getName().substring(file.getName().lastIndexOf(",")+1)));
//                }
//
//            }
//        }
//        List<Integer> integers = Arrays.asList(new Integer[]{0,2,3,4,5,6,1,1});
//        System.out.println(integers);
//        List<String> l = integers.stream().map(p->"'"+p.toString()+"'").collect(Collectors.toList());
//        Map<String, Long> countedSeqs = l.stream()
//                .collect(Collectors.groupingBy(p -> p, Collectors.counting()));
//
//        System.out.println(l);
//        System.out.println(countedSeqs);
//        System.out.println(new Long(0l));
//        System.out.println(1.0/2);
//        double wSum = 0.0;
//
//        for (int i = 1; i < 10; i++) {
//            for (int j = 0; j < i; j++) {
//                wSum += 1.0/(j+1);
//
//            }
//            System.out.println(wSum);
//            wSum=0;
//        }
//        List<String> s = Arrays.asList("a","b","v");
//        Map<String, String> map = new HashMap<>();
//        map.put("a","1");
//        map.put("b","2");
//        map.put("3","3");
//
//        System.out.println(map.toString());

//        List<Point> list = Arrays.asList(new Point(1.2,2.1,4.4,"sds"),new Point(11.2,22.1,4.4,"dd"),new Point(14.2,52.1,4.4,"seeds"));
//        List<Point> newLKist = new ArrayList<>();
//        list.forEach(p->newLKist.add(p));
//        System.out.println(list);
//        System.out.println(newLKist);
//        Point q = list.get(0);
//        q.mlat = 10.0;
//        System.out.println(list);
//        System.out.println(newLKist);

    }

}
