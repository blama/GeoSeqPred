package clustering;

import java.util.*;

/**
 * This is an implementation of BFR(Bradley-Fayyad-Reina) clustering. The type E, defined as a
 * generic, should have characteristics of a point in an euclidean space. So It must extend class Euclidean
 * of type E to be forced to implement required functions.
 *
 * @param <E> A generic type of Entries
 */
public class BFR<E extends Euclidean<E>> {
    public static final String K_MEANS = "K_MEANS";
    public static final String HIERARCHICAL = "HIERARCHICAL";
    public double distance_threshold = 669;
    public double eps = 669;
    public int hierarchical_threshold = 200;
    public static int iteration_count = 100;
    public List<E> entries;
    private List<E> sample;

    public List<Cluster> clusters;
    public List<ClusterRep> discardSet = new ArrayList<ClusterRep>();
    private Set<ClusterRep> compressedSet;
    private Set<E> retainedSet;

    public BFR(List<E> entries) {
        this.entries = entries;
        this.clusters = new ArrayList<Cluster>();
        retainedSet = new HashSet<E>();
    }

    /**
     * generates a sample of entries using java {@link java.util.Random}
     *
     * @param samplePercentage the fraction of inputs required for sampling
     */
    public void sampleGen(double samplePercentage) {
        Random r = new Random();
        List<E> tempries = new ArrayList<E>();
        tempries.addAll(entries);
        int sampleSize = (int) Math.round(samplePercentage * entries.size());
        List<E> sample = new ArrayList<E>();
        for (int i = 0; i < sampleSize; i++) {
            int index = r.nextInt(tempries.size());
            sample.add(tempries.get(index));
            tempries.remove(index);
        }
        this.sample = sample;
        entries.removeAll(sample);
    }

    public List<ClusterRep> clusterSamples(String algorithm, int k) throws Exception {
        if (algorithm.equalsIgnoreCase(K_MEANS)) {
            clusterSamplesKMeans(k);
        } else if (algorithm.equalsIgnoreCase(HIERARCHICAL)) {
            clusterSamplesHierarchical(k);
        }
        setDiscardSet();
        return discardSet;
    }

    private void clusterSamplesHierarchical(int count) {
    }

    private void clusterSamplesKMeans(int count) throws Exception {
        if (count > entries.size()) {
            throw new Exception("k must be smaller than points");
        }
        //making a random sample of size k==count
        Random r = new Random();
        Set<Integer> sampledIndices = new HashSet<Integer>();
        List<Cluster> clusterSet = new ArrayList<Cluster>();
        for (int i = 0; i < count; i++) {
            int random_index = r.nextInt(entries.size());
            while (true) {
                if (sampledIndices.contains(random_index)) {
                    random_index = r.nextInt(entries.size());
                } else {
                    sampledIndices.add(random_index);
                    break;
                }

            }
        }
        for (Integer i : sampledIndices) {
            List<E> ens = new ArrayList<E>();
            ens.add(entries.get(i));
            clusterSet.add(new Cluster(ens));
        }
        for (Integer i : sampledIndices) {
            entries.remove(i);
        }
        System.out.println("first clusters");
        for (E e : entries) {
            double min = Double.POSITIVE_INFINITY;
            Cluster minC = null;
            for (Cluster c : clusterSet) {
                if (e.getDistance(e, c.getCentroid()) < min) {
                    min = e.getDistance(e, c.getCentroid());
                    minC = c;
                }
            }
//            if (e.getDistance(e,minC.getCentroid())<distance_threshold){
            minC.pEntries.add(e);
//            }
        }
        for (Cluster c : clusterSet) {
            c.updateCentroid();
        }
        System.out.println("updating clusters");
        updateClusters(clusterSet, iteration_count, eps, true);
        clusters = clusterSet;
    }

    private void updateClusters(List<Cluster> clusterSet, int iterations, double eps, boolean isIterate) {
        if (isIterate) {
            for (int i = 0; i < iterations; i++) {
                System.out.print("iteration " + i + ", ");
                List<E> allPoints = retainPoints(clusterSet);
                for (E e : allPoints) {
                    findClosest(clusterSet, e).pEntries.add(e);
                }
                allPoints.clear();
                for (Cluster c : clusterSet) {
                    c.updateCentroid();
                }
            }
        }
    }

    private List<E> retainPoints(List<Cluster> clusterSet) {
        List<E> allPoints = new ArrayList<E>();
        for (Cluster c : clusterSet) {
            allPoints.addAll(c.pEntries);
            c.pEntries.clear();
        }
        return allPoints;
    }

    private Cluster findClosest(List<Cluster> clusterSet, E e) {
        double min = Double.POSITIVE_INFINITY;
        Cluster minC = null;
        for (Cluster c : clusterSet) {
            if (e.getDistance(e, c.centroid) < min) {
                min = e.getDistance(e, c.centroid);
                minC = c;
            }
        }
        return minC;
    }

    public void setDiscardSet() {
        for (Cluster c : clusters) {
            if (c.pEntries.size() > 0)
                discardSet.add(new ClusterRep(c));
        }
    }

    /**
     * Adds a new entry to the closest cluster representation from discardedSet, if the distance is below
     * distance threshold, add to retainedSet otherwise
     *
     * @param e new entry to be added
     */
    public void addEntry(E e) {
        double[] dist = new double[discardSet.size()];
        for (int i = 0; i < dist.length; i++) {
            dist[i] = e.getDistance(e, discardSet.get(i).getCentroid());
        }
        int min = 0;
        for (int i = 0; i < dist.length; i++) {
            if (dist[i] < dist[min]) {
                min = i;
            }
        }
        if (dist[min] < distance_threshold) {
            discardSet.get(min).addPoint(e);
        } else {
            retainedSet.add(e);
        }
    }

    public Cluster newCluster(List<E> entries) {
        return new Cluster(entries);
    }

    public List<E> getSample() {
        return this.sample;
    }

    public String printClusters() {
        StringBuilder b = new StringBuilder();
        System.out.println("\nnumber of clusters" + clusters.size());
        double[] radius = new double[clusters.size()];
        for (int i = 0; i < radius.length; i++) {
            radius[i] = clusters.get(i).getRadius();
        }
        Arrays.sort(radius);
        b.append("min:" + radius[0]);
        b.append("max:" + radius[radius.length - 1]);
        b.append("median:" + radius[Math.round(radius.length / 2)]);
        b.append("\nretained" + retainedSet.size());
//        return b.toString();
        return b.toString();
    }

    /**
     * a set of entries used when clustering samples
     */
    class Cluster {
        List<E> pEntries;
        E centroid;

        Cluster(List<E> entries) {
            this.pEntries = entries;
            centroid = getCentroid();
        }

        E getCentroid() {
            E result = pEntries.get(0);
            for (int i = 1; i < pEntries.size(); i++) {
                result = result.add(result, pEntries.get(i));
            }
            return result.divide(result, pEntries.size());
        }

        void updateCentroid() {
            this.centroid = getCentroid();
        }

        public double getRadius() {
            double max = 0;
            for (E e : pEntries) {
                if (e.getDistance(e, centroid) > max) {
                    max = e.getDistance(e, centroid);
                }
            }
            return max;
        }
    }

    /**
     * A representation of clusters described in BFR algorithm (an element of Discarded Set)
     */
    class ClusterRep {
        public int numberOfPoints;
        private E sum;
        private E sumSqr;

        ClusterRep(Cluster c) {
            setCount(c);
            setSum(c);
            setSumSqr(c);
        }

        public void setCount(Cluster c) {
            this.numberOfPoints = c.pEntries.size();
        }

        public void setSum(Cluster c) {
            E e1 = c.pEntries.get(0);
            for (int i = 1; i < c.pEntries.size(); i++) {
                e1 = e1.add(e1, c.pEntries.get(i));
            }
            this.sum = e1;
        }

        public void setSumSqr(Cluster c) {
            E e1 = c.pEntries.get(0);
            for (int i = 1; i < c.pEntries.size(); i++) {
                e1 = e1.addSqr(e1, c.pEntries.get(i));
            }
            this.sumSqr = e1;
        }

        public E getCentroid() {
            return sum.divide(sum, numberOfPoints);
        }

        public double distance(E e1, E e2) {
            return e1.getDistance(e1, e2);
        }

        public void addPoint(E e) {
            this.numberOfPoints++;
            this.sum = e.add(e, this.sum);
            this.sumSqr = e.addSqr(this.sumSqr, e);
        }
    }
}
